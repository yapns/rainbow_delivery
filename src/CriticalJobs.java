/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File; 
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException; 
import java.text.ParseException;
import java.text.SimpleDateFormat; 
import java.util.Date; 
import java.util.ResourceBundle;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle; 
import org.apache.poi.ss.usermodel.Font; 
import org.apache.poi.ss.usermodel.Row; 
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook; 
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CriticalJobs {

   public static String str_logfolder = "D:\\DailyCatchUp\\20170707\\";
   // public static String str_logfolder = "D:\\UAT Timing\\20161129\\";
    public static int str_job_maxlength = 50;
    public static int str_time_maxlength = 7;

    public static String str_YYYYMMDD = "20161018";

      private static final ResourceBundle BANK_JOBS = ResourceBundle.getBundle("bank_jobs1");
    public static int total_bank_jobs = 18;

    private static final ResourceBundle CC_JOBS = ResourceBundle.getBundle("cc_jobs1");
    public static int total_cc_jobs = 18;

    private static final ResourceBundle DELTA_JOBS = ResourceBundle.getBundle("delta_jobs1");
    public static int total_delta_jobs = 19;
    
//    private static final ResourceBundle CO_JOBS = ResourceBundle.getBundle("wdco_jobs1");
//    public static int total_co_jobs = 19;
    
    public static void main(String[] args) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {

       // CreateExcelFile();

        WDBKD("WDBKD", str_logfolder + "WDBKD.log");

        WDCCD("WDCCD", str_logfolder + "WDCCD.log");

        WDDLD("WDDLD", str_logfolder + "WDDLD.log");

       //WDCOD_UAT("WDCOD", str_logfolder + "WDCOD.log");
    }

    public static void WDBKD(String str_CC_D_B, String str_logfile) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
        System.out.println("********* BANK ************");

        for (int i = 1; i <= total_bank_jobs; i++) {
            if (i < 10) {
                readLog(i, str_CC_D_B, str_logfile, BANK_JOBS.getString("JOB_0" + i), BANK_JOBS.getString("S_JOB_0" + i), BANK_JOBS.getString("E_JOB_0" + i));
            } else {
                readLog(i, str_CC_D_B, str_logfile, BANK_JOBS.getString("JOB_" + i), BANK_JOBS.getString("S_JOB_" + i), BANK_JOBS.getString("E_JOB_" + i));
            }
        }
    }

    public static void WDCCD(String str_CC_D_B, String str_logfile) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
        System.out.println("\n********* CC ************");
        for (int i = 1; i <= total_cc_jobs; i++) {
            if (i < 10) {
                readLog(i, str_CC_D_B, str_logfile, CC_JOBS.getString("JOB_0" + i), CC_JOBS.getString("S_JOB_0" + i), CC_JOBS.getString("E_JOB_0" + i));
            } else {
                readLog(i, str_CC_D_B, str_logfile, CC_JOBS.getString("JOB_" + i), CC_JOBS.getString("S_JOB_" + i), CC_JOBS.getString("E_JOB_" + i));
            }
        }
    }

    public static void WDDLD(String str_CC_D_B, String str_logfile) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
        System.out.println("\n********* DELTA ************");
        for (int i = 1; i <= total_delta_jobs; i++) {
            if (i < 10) {
                readLog(i, str_CC_D_B, str_logfile, DELTA_JOBS.getString("JOB_0" + i), DELTA_JOBS.getString("S_JOB_0" + i), DELTA_JOBS.getString("E_JOB_0" + i));
            } else {
                readLog(i, str_CC_D_B, str_logfile, DELTA_JOBS.getString("JOB_" + i), DELTA_JOBS.getString("S_JOB_" + i), DELTA_JOBS.getString("E_JOB_" + i));
            }
        }
    }

//    public static void WDCOD_UAT(String str_CC_D_B, String str_logfile) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
//        System.out.println("\n********* WDCOD ************"); 
//        for (int i = 1; i <= total_co_jobs; i++) {
//            if (i < 10) {
//                readLog(i, str_CC_D_B, str_logfile, CO_JOBS.getString("JOB_0" + i), CO_JOBS.getString("S_JOB_0" + i), CO_JOBS.getString("E_JOB_0" + i));
//            } else {
//                readLog(i, str_CC_D_B, str_logfile, CO_JOBS.getString("JOB_" + i), CO_JOBS.getString("S_JOB_" + i), CO_JOBS.getString("E_JOB_" + i));
//            }
//        }
//    }

    public static String calculateDiff(String str_starttime, String str_endtime) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        String str_duration = "";

        try {
            d1 = format.parse(str_starttime);
            d2 = format.parse(str_endtime);
//            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long lg_seconds = (diffHours * 3600) + (diffMinutes * 60) + diffSeconds;

            if (diffHours != 0 && diffMinutes != 0 && diffMinutes < 10 && diffSeconds != 0) {
                str_duration = diffHours + "h  " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes < 10 && diffSeconds == 0) {
                str_duration = diffHours + "h  " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes >= 10 && diffSeconds != 0) {
                str_duration = diffHours + "h " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes >= 10 && diffSeconds == 0) {
                str_duration = diffHours + "h " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes == 0 && diffSeconds != 0) {
                str_duration = diffHours + "h ";
            } else if ((diffHours == 0 && diffMinutes != 0 && diffSeconds != 0 && diffMinutes < 10) || (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0 && diffMinutes < 10)) {
                str_duration = "    " + diffMinutes + "m";
            } else if ((diffHours == 0 && diffMinutes != 0 && diffSeconds != 0 && diffMinutes >= 10) || (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0 && diffMinutes >= 10)) {
                str_duration = "   " + diffMinutes + "m";
            } else if ((diffHours == 0 && diffMinutes == 0 && diffSeconds != 0 && diffSeconds >= 10)) {
                str_duration = "   " + diffSeconds + "s";
            }else{
                 str_duration = "    " + diffSeconds + "s";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_duration;
    }

    public static String readLog(int row_num, String str_CC_D_B, String str_logfile, String str_job, String str_startcode, String str_endcode) throws FileNotFoundException, IOException, ParseException, InvalidFormatException {
        int diff = 0;

        if (str_job.length() < str_job_maxlength) {
            diff = str_job_maxlength - str_job.length();

            for (int z = 0; z < diff; z++) {
                str_job += " ";
            }
        }

        String line = null;

        int count = 0;

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_log = "";
        //String str_log1 = "";
        String str_duration = "";
        String str_starttime = "";
        String str_endtime = "";

        int sheetnum = 1;
        while ((line1 = bufferedReader1.readLine()) != null) {
             
            if (line1.contains(str_startcode)) {
                if(str_startcode != null){
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 6, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");
                str_starttime = str_starttime.replaceAll(" ", "");
                str_starttime = str_starttime.replaceAll(",", " ");
                }else{
                    str_starttime = "";
                }
            }
            if (line1.contains(str_endcode)) {
                i = line1.indexOf("END:");
                str_endtime = line1.substring(i + 4, line1.length());
                str_endtime = str_endtime.replaceAll(" ", "");
                str_endtime = str_endtime.replaceAll(",", " ");
            }
        }

        String str_start_HH_mm_ss = "";
        String str_end_HH_mm_ss = "";
        if (!"".equals(str_endtime) && !"".equals(str_starttime)) {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

            Date d1 = null;
            Date d2 = null;
            Date d3 = null;
            d1 = format.parse(str_starttime);

            String str_yyyy_dd_MM = new SimpleDateFormat("yyyy-dd-MM").format(d1);

            String str_time = str_yyyy_dd_MM + " 0:00:00";
            d2 = format.parse(str_time);
            d3 = format.parse(str_endtime);

            long s_diff = d1.getTime() - d2.getTime();
            long s_diffHours = s_diff / (60 * 60 * 1000) % 24;
            long s_diffMinutes = s_diff / (60 * 1000) % 60;

            long e_diff = d3.getTime() - d2.getTime();
            long e_diffHours = e_diff / (60 * 60 * 1000) % 24;
            long e_diffMinutes = e_diff / (60 * 1000) % 60;

            d1 = format.parse(str_starttime);
            str_start_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d1);

            d3 = format.parse(str_endtime);
            str_end_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d3);

            str_duration = calculateDiff(str_starttime, str_endtime);
            int diff1 = 0;

            //str_log1 = str_job + "  " + str_start_HH_mm_ss + " - " + str_end_HH_mm_ss + "  " + str_duration;
            //str_log1 = str_end_HH_mm_ss ;

            if ("WDBKD".equals(str_CC_D_B)) {
                sheetnum = 1;
            } else if ("WDCCD".equals(str_CC_D_B)) {
                sheetnum = 2;
            } else if ("WDDLD".equals(str_CC_D_B)) {
                sheetnum = 3;
            } else if ("WDCOD".equals(str_CC_D_B)) {
                sheetnum = 4;
            }

            row_num = row_num + 2;

            AppendExcelFile(row_num, str_job, str_start_HH_mm_ss, str_end_HH_mm_ss, str_duration, sheetnum);

            //System.out.println(str_CC_D_B + " " + str_log1);
        } else if ((!"".equals(str_starttime)) && ("".equals(str_endtime))) {
           // System.out.println(str_job + " (" + str_startcode + " " + str_endcode + ")" + str_starttime.substring(str_starttime.indexOf(" "), str_starttime.length()) + ", NOT FOUND");
        } else if ("".equals(str_starttime)) {
           // System.out.println(str_job + " (" + str_startcode + " " + str_endcode + ") NOT FOUND, NOT FOUND");
        }

        return str_log;
    }

    public static void AppendExcelFile(int row_num, String str_job, String str_starttime, String str_endtime, String str_duration, int sheetnum) throws FileNotFoundException, IOException, InvalidFormatException {

        if (!"                                                  ".equals(str_job)){
        //System.out.println(str_job + " " + str_starttime + " " + str_endtime + " " + str_duration);
        System.out.println(str_endtime);
        }else{
            System.out.println("");
        }
//        FileInputStream file = new FileInputStream(new File("batch_summary.xlsx"));
//
//        Workbook wb = WorkbookFactory.create(file);
//
//        Sheet sheet = wb.getSheet("BANK");;
//
//        if (sheetnum == 1) {
//            sheet = wb.getSheet("BANK");
//        } else if (sheetnum == 2) {
//            sheet = wb.getSheet("CC");
//        } else if (sheetnum == 3) {
//            sheet = wb.getSheet("DELTA");
//        } else if (sheetnum == 4) {
//            sheet = wb.getSheet("WDCO");
//        }
//
//        sheet.setMargin(Sheet.TopMargin, 0.75);
//        sheet.setColumnWidth(0, 1500);  //set column 0 width - No.
//        sheet.setColumnWidth(1, 10000); //set column 1 width - jobs
//       
//        
//        Font font = wb.createFont();
//        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
//
//        Row row = null;
//        Cell cell = null;
//        Cell cell1 = null;
//        int column_num = 0;
//
//        row = sheet.getRow(row_num);
//
//        CellStyle style = wb.createCellStyle();
//
//        for (int j = 2; j < 100; j++) {
//            cell = row.getCell((short) j);
//            if (cell == null) {
//                column_num = j;
//                sheet.setColumnWidth(j, 4300); //set column width - START to END
//                cell = row.createCell((short) column_num);
//                cell.setCellValue(str_starttime + " - " + str_endtime);
//                cell.setCellStyle(style);
//
//                sheet.setColumnWidth(j+1, 2000); //set column width - Duration
//                style.setAlignment(CellStyle.ALIGN_RIGHT);
//                style.setBorderLeft(CellStyle.BORDER_THIN);
//                cell1 = row.createCell((short) column_num + 1);
//                cell1.setCellValue(str_duration);
//                cell1.setCellStyle(style);
//                j = 99;
//            }
//        }
//
//        try {
////            FileOutputStream out = new FileOutputStream(new File("batch_summary.xlsx"));
////            wb.write(out);
////            out.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public static void CreateExcelFile() throws FileNotFoundException, IOException, InvalidFormatException {

        File file = new File("batch_summary.xlsx");

        String str_sheet = null;
        int num = 0;
        if (!file.exists()) {
            file.createNewFile();

            FileOutputStream fos = new FileOutputStream(file, false);

            Workbook wb = new XSSFWorkbook();

            for (int i = 1; i <= 4; i++) {
                if (i == 1) {
                    str_sheet = "BANK";
                    num = total_bank_jobs;
                } else if (i == 2) {
                    str_sheet = "CC";
                    num = total_cc_jobs;
                }else if (i == 3) {
                    str_sheet = "DELTA";
                    num = total_delta_jobs;
                }else if (i == 4) {
                    str_sheet = "WDCO";
//                    num = total_co_jobs;
                }
                Sheet sheet = wb.createSheet(str_sheet);
                sheet.setMargin(Sheet.TopMargin, 0.75);
                sheet.setColumnWidth(0, 500);

                Sheet sheet1 = wb.getSheet(str_sheet);

                sheet1.setMargin(Sheet.TopMargin, 0.75);
                sheet1.setColumnWidth(0, 500);

                Font font = wb.createFont();
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);

                CellStyle style = wb.createCellStyle();
                
                Row row = null;
                Cell cell0 = null;
                Cell cell1 = null;

                for (int j = 1; j <= num; j++) {
                    row = sheet1.getRow(j + 2);
                    if (row == null) {
                        row = sheet1.createRow((short) j + 2);
                    }

                    cell0 = row.getCell((short) 0);
                    if (cell0 == null) {
                        cell0 = row.createCell((short) 0);
                    }

                    style.setAlignment(CellStyle.ALIGN_CENTER);
                    cell0.setCellValue(j);
                    cell0.setCellStyle(style);
                    

                    cell1 = row.getCell((short) 1);
                    if (cell1 == null) {
                        cell1 = row.createCell((short) 1);
                    }

                    if (j < 10 && i == 1) {
                        cell1.setCellValue(BANK_JOBS.getString("JOB_0" + j));
                    } else if (j >= 10 && i == 1) {
                        cell1.setCellValue(BANK_JOBS.getString("JOB_" + j));
                    } else if (j < 10 && i == 2) {
                        cell1.setCellValue(CC_JOBS.getString("JOB_0" + j));
                    } else if (j >= 10 && i == 2) {
                        cell1.setCellValue(CC_JOBS.getString("JOB_" + j));
                    } else if (j < 10 && i == 3) {
                        cell1.setCellValue(DELTA_JOBS.getString("JOB_0" + j));
                    } else if (j >= 10 && i == 3) {
                        cell1.setCellValue(DELTA_JOBS.getString("JOB_" + j));
                    } else if (j < 10 && i == 4) {
                     //   cell1.setCellValue(CO_JOBS.getString("JOB_0" + j));
                    } else if (j >= 10 && i == 4) {
                      //  cell1.setCellValue(CO_JOBS.getString("JOB_" + j));
                    }
                }
            }

            try {
                FileOutputStream out = new FileOutputStream(new File("batch_summary.xlsx"));
                wb.write(out);
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
