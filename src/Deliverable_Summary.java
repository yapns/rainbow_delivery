/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.System.exit;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.ShapeTypes;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Deliverable_Summary {

    public static String str_logfolder = "D:\\DailyCatchUp\\20170707\\";
    public static String str_date="07-July";
    //public static String str_logfolder = "D:\\UAT Timing\\20160728\\";
    public static int str_job_maxlength = 50;
    public static int str_time_maxlength = 7;

    public static String str_YYYYMMDD = "20160916";

    private static final ResourceBundle BANK_JOBS = ResourceBundle.getBundle("bank_jobs");
    public static int total_bank_jobs = 37;

    private static final ResourceBundle CC_JOBS = ResourceBundle.getBundle("cc_jobs");
    public static int total_cc_jobs = 22;

    private static final ResourceBundle DELTA_JOBS = ResourceBundle.getBundle("delta_jobs");
    public static int total_delta_jobs = 20;
    
    private static final ResourceBundle CO_JOBS = ResourceBundle.getBundle("wdco_jobs");
    public static int total_co_jobs = 19;
    
    public static void main(String[] args) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {

       CreateExcelFile();

       WDBKD("WDBKD", str_logfolder + "WDBKD.log");

       WDCCD("WDCCD", str_logfolder + "WDCCD.log");

       WDDLD("WDDLD", str_logfolder + "WDDLD.log");

       WDCOD_UAT("WDCOD", str_logfolder + "WDCOD.log");
    }

    public static void WDBKD(String str_CC_D_B, String str_logfile) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
      
        System.out.println(""+str_date);
        readLog( 1, str_CC_D_B, str_logfile, "DCRMS BANK Pre-Batch",          "WDBKD0376B", "WDBKD0381B");
        readLog( 2, str_CC_D_B, str_logfile, "ALLTEL AUTO DIALER BATCH",      "WDBKD0269B", "WDBKD0273B"); //AutoDialer WDBKD0269B  WDBKD0273B
        readLog( 3, str_CC_D_B, str_logfile, "CRECC-CL PRE-BATCH REPORT",     "WDBKD0440B", "WDBKD0440B");  //ECC Inventory Report (Daily)
        
        System.out.println("");  //6
        System.out.println("");  //7
        System.out.println("");  //8
        System.out.println("");  //9
        readLog( 4, str_CC_D_B, str_logfile, "CRECC-CL PRE-BATCH REPORT",     "WDBKD0763B", "WDBKD0763B");//Productivity Report (Daily Accumulative)
       
        readLog( 5, str_CC_D_B, str_logfile, "CCRD-CL PRE-BATCH REPORT",      "WDBKD0492B", "WDBKD0492B");//Collector Performance Monitoring(Daily Accumulative)
        System.out.println("");  //12
        System.out.println("");  //13
        System.out.println("");  //14
        System.out.println("");  //15
        System.out.println("");  //16
        readLog( 6, str_CC_D_B, str_logfile, "CCRD-RC PRE-BATCH REPORT",      "WDBKD0567B", "WDBKD0567B"); //Productivity Report (Daily Accumulative)

        System.out.println("");  //18
        System.out.println("");  //19
        System.out.println("");  //20
        System.out.println("");  //21
        System.out.println("");  //22
        System.out.println("");  //23
        readLog( 7, str_CC_D_B, str_logfile, "CCRD-RC PRE-BATCH REPORT",      "WDBKD0576B", "WDBKD0576B"); //WO
        readLog( 8, str_CC_D_B, str_logfile, "CRSME-RC PRE-BATCH REPORT",     "WDBKD0463B", "WDBKD0466B"); //PLCC Management Report (Monthly)
        System.out.println("");  //26
        System.out.println("");   //27
        System.out.println("");   //28
        System.out.println("");   //29
        System.out.println("");   //30
        System.out.println("");  //31
        System.out.println("");  //32 
        readLog( 9, str_CC_D_B, str_logfile, "CRSME-RC PRE-BATCH REPORT",     "WDBKD0568B", "WDBKD0568B");//Productivity Report (Daily Accumulative)
        readLog( 10, str_CC_D_B, str_logfile, "CRSME-CGC PRE-BATCH REPORT",    "WDBKD0570B", "WDBKD0578B");  //Audit Trail Report (Daily)
        readLog( 11, str_CC_D_B, str_logfile, "CCPF-PF-CL PRE-BATCH REPORT",   "WDCCD0225B", "WDCCD0225B"); //ECA Performance Report (Daily)
        readLog( 12, str_CC_D_B, str_logfile, "CCPF-PF-CL PRE-BATCH REPORT",   "WDCCD0332B", "WDCCD0332B"); //Collector Performance Monitoring(Daily Accumulative
        readLog( 13, str_CC_D_B, str_logfile, "CCPF-PF-CL PRE-BATCH REPORT",   "WDCCD0303B", "WDCCD0303B"); //Productivity Report (Daily Accumulative)
        readLog(14, str_CC_D_B, str_logfile, "CRECC-CL POST-BATCH REPORT",    "WDBKD0475B", "WDBKD0478B");
        readLog(15, str_CC_D_B, str_logfile, "CCRD-CL POST-BATCH REPORT",     "WDBKD0471B", "WDBKD0474B");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        readLog(16, str_CC_D_B, str_logfile, "CCRD-RC POST-BATCH REPORT",     "WDBKD0457B", "WDBKD0461B");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        readLog(17, str_CC_D_B, str_logfile, "CRSME-RC POST-BATCH REPORT",    "WDBKD0479B", "WDBKD0482B");
        System.out.println("");
        System.out.println("");
        readLog(18, str_CC_D_B, str_logfile, "CCPF-PF-CL POST-BATCH REPORT ", "WDCCD0219B", "WDCCD0222B");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
    }

    public static void WDCCD(String str_CC_D_B, String str_logfile) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
        System.out.println("\n "+str_date);  //60
        readLog(1, str_CC_D_B, str_logfile, "CCPF-CC-CL PRE-BATCH REPORT", "WDCCD0247B", "WDCCD0247B"); //61 Collector Performance Monitoring(Daily Accumulative)
        System.out.println("");  //62
        System.out.println("");  //63
        System.out.println("");  //64
        readLog(2, str_CC_D_B, str_logfile, "CCPF-CC-RC PRE-BATCH REPORT", "WDCCD0269B", "WDCCD0269B"); //65 Security Report (Daily)
        System.out.println(""); //66
        System.out.println(""); //67
        System.out.println(""); //68
        readLog(2, str_CC_D_B, str_logfile, "CCPF-CC-RC PRE-BATCH REPORT", "WDCCD0302B", "WDCCD0302B"); //69Productivity Report (Daily Accumulative)
        readLog(3, str_CC_D_B, str_logfile, "CCPF-CC-CL POST-BATCH REPORT", "WDCCD0205B", "WDCCD0208B");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
       // readLog(4, str_CC_D_B, str_logfile, "CCPF Write Off", "WDCCD0328B", "WDCCD0330B");
        readLog(4, str_CC_D_B, str_logfile, "CCPF Write Off", "WDCCD0373B", "WDCCD0376B");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        readLog(5, str_CC_D_B, str_logfile, "CCPF Generate Legal CSV", "WDBKD0562B", "WDBKD0564B");
        System.out.println("");
        readLog(6, str_CC_D_B, str_logfile, "CCPF-CC-RC POST-BATCH REPORT", "WDCCD0232B", "WDCCD0236B");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
    }

    public static void WDDLD(String str_CC_D_B, String str_logfile) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
        System.out.println("\n "+str_date);  //92
         readLog(1,str_CC_D_B, str_logfile, "AFABF-CL PRE-BATCH REPORT", "WDDLD0268B", "WDDLD0268B"); //Field Turnaround Time Report (Daily)
         readLog(2,str_CC_D_B, str_logfile, "AFABF-CL PRE-BATCH REPORT", "WDDLD0317B", "WDDLD0317B"); //Arrear Collection Report (Daily)
          readLog(3,str_CC_D_B, str_logfile, "AFABF-CL PRE-BATCH REPORT", "WDDLD0322B", "WDDLD0278B");// Productivity Report (Daily Accumulative)
         readLog(4, str_CC_D_B, str_logfile, "AFABF AUTO DIALER BATCH", "WDDLD0145B", "WDDLD0145B");  //HP_GUARANTOR_YYMMDD.txt
         readLog(5, str_CC_D_B, str_logfile, "AFABF AUTO DIALER BATCH", "WDDLD0144B", "WDDLD0144B");  //HP_HIRER_YYMMDD.txt
         readLog(6, str_CC_D_B, str_logfile, "AFABF AUTO DIALER BATCH", "WDDLD0146B", "WDDLD0146B");  //HP_POSTCODE_YYMMDD.txt
         readLog(7, str_CC_D_B, str_logfile, "AFABF-CL POST-BATCH REPORT", "WDDLD0319B", "WDDLD0319B"); //Cheque Return Report (Daily)
         readLog(8,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0273B", "WDDLD0273B"); //100 Assignment of Accounts to ECA (Daily)
         readLog(9,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0318B", "WDDLD0318B"); //Ageing Summary (Daily)
       readLog(10,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0320B", "WDDLD0320B"); //ECA Inventory Report (Daily)
        readLog(11,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0321B", "WDDLD0321B"); //Legal Stages TAT Report (Fortnightly) 
       readLog(12,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0315B", "WDDLD0315B"); //New NPL Report 
       
       readLog(13,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0300B", "WDDLD0300B"); //List of External LOD Instruction (Daily)
       readLog(14,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0310B", "WDDLD0310B"); //ECA Daily Payment Tracking Report (Daily)
       readLog(15,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0305B", "WDDLD0305B"); //ECA Recovery Performance Report Summary (Monthly)
       readLog(16,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0402B", "WDDLD0402B"); //Legal Stages TAT Report (Fortnightly)
      readLog(17,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0065B", "WDDLD0065B"); //Solicitor Inventory Report
        readLog(18,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0244B", "WDDLD0245B"); //Repossessed Stock / Ageing Position (Daily)
         readLog(19,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0244B", "WDDLD0245B"); //ECA Performance Report (Daily)
        readLog(20,str_CC_D_B, str_logfile, "AFABF-RC POST-BATCH REPORT", "WDDLD0244B", "WDDLD0245B"); //List of Vehicle / Goods Repossessed / Redeemed / Sold (Daily) 
        
        System.out.println("");
        System.out.println("");
    }

    public static void WDCOD_UAT(String str_CC_D_B, String str_logfile) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
         System.out.println("\n "+str_date);
       readLog(1, str_CC_D_B, str_logfile, "CSV File Generation 1", "WDCOD0007B", "WDCOD0007B"); 
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
       readLog(2, str_CC_D_B, str_logfile, "CSV File Generation 2", "WDCOD0008B", "WDCOD0008B");
       readLog(3, str_CC_D_B, str_logfile, "CSV File Generation 3", "WDCOD0009B", "WDCOD0009B");
        System.out.println("");
        
        readLog(4, str_CC_D_B, str_logfile, "CSV File Generation 4", "WDCOD0010B", "WDCOD0010B");
        readLog(5, str_CC_D_B, str_logfile, "CSV File Generation 5", "WDCOD0011B", "WDCOD0011B");       
        readLog(6, str_CC_D_B, str_logfile, "CSV File Generation 6", "WDCOD0012B", "WDCOD0012B");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        
        
        readLog(7, str_CC_D_B, str_logfile, "CSV File Generation 7", "WDCOD0021B", "WDCOD0021B");
        readLog(8, str_CC_D_B, str_logfile, "CSV File Generation 8", "WDCOD0022B", "WDCOD0022B");
        readLog(9, str_CC_D_B, str_logfile, "CSV File Generation 9", "WDCOD0023B", "WDCOD0023B");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        readLog(10, str_CC_D_B, str_logfile, "CSV File Generation 10", "WDCOD0024B", "WDCOD0024B");
        readLog(11, str_CC_D_B, str_logfile, "CSV File Generation 11", "WDCOD0025B", "WDCOD0025B");
    }

    public static String calculateDiff(String str_starttime, String str_endtime) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        String str_duration = "";

        try {
            d1 = format.parse(str_starttime);
            d2 = format.parse(str_endtime);
//            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long lg_seconds = (diffHours * 3600) + (diffMinutes * 60) + diffSeconds;

            if (diffHours != 0 && diffMinutes != 0 && diffMinutes < 10 && diffSeconds != 0) {
                str_duration = diffHours + "h  " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes < 10 && diffSeconds == 0) {
                str_duration = diffHours + "h  " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes >= 10 && diffSeconds != 0) {
                str_duration = diffHours + "h " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes >= 10 && diffSeconds == 0) {
                str_duration = diffHours + "h " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes == 0 && diffSeconds != 0) {
                str_duration = diffHours + "h ";
            } else if ((diffHours == 0 && diffMinutes != 0 && diffSeconds != 0 && diffMinutes < 10) || (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0 && diffMinutes < 10)) {
                str_duration = "    " + diffMinutes + "m";
            } else if ((diffHours == 0 && diffMinutes != 0 && diffSeconds != 0 && diffMinutes >= 10) || (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0 && diffMinutes >= 10)) {
                str_duration = "   " + diffMinutes + "m";
            } else {
                str_duration = "   " + diffSeconds + "s";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_duration;
    }

    public static String readLog(int row_num, String str_CC_D_B, String str_logfile, String str_job, String str_startcode, String str_endcode) throws FileNotFoundException, IOException, ParseException, InvalidFormatException {
        int diff = 0;

        if (str_job.length() < str_job_maxlength) {
            diff = str_job_maxlength - str_job.length();

            for (int z = 0; z < diff; z++) {
                str_job += " ";
            }
        }

        String line = null;

        int count = 0;

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_log = "";
        String str_log1 = "";
        String str_duration = "";
        String str_starttime = "";
        String str_endtime = "";

        int sheetnum = 1;
        while ((line1 = bufferedReader1.readLine()) != null) {

            if (line1.contains(str_startcode)) {
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 6, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");
                str_starttime = str_starttime.replaceAll(" ", "");
                str_starttime = str_starttime.replaceAll(",", " ");
            }
            if (line1.contains(str_endcode)) {
                i = line1.indexOf("END:");
                str_endtime = line1.substring(i + 4, line1.length());
                str_endtime = str_endtime.replaceAll(" ", "");
                str_endtime = str_endtime.replaceAll(",", " ");
            }
        }

        String str_start_HH_mm_ss = "";
        String str_end_HH_mm_ss = "";
        if (!"".equals(str_endtime) && !"".equals(str_starttime)) {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

            Date d1 = null;
            Date d2 = null;
            Date d3 = null;
            d1 = format.parse(str_starttime);

            String str_yyyy_dd_MM = new SimpleDateFormat("yyyy-dd-MM").format(d1);

            String str_time = str_yyyy_dd_MM + " 0:00:00";
            d2 = format.parse(str_time);
            d3 = format.parse(str_endtime);

            long s_diff = d1.getTime() - d2.getTime();
            long s_diffHours = s_diff / (60 * 60 * 1000) % 24;
            long s_diffMinutes = s_diff / (60 * 1000) % 60;

            long e_diff = d3.getTime() - d2.getTime();
            long e_diffHours = e_diff / (60 * 60 * 1000) % 24;
            long e_diffMinutes = e_diff / (60 * 1000) % 60;

            d1 = format.parse(str_starttime);
            str_start_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d1);

            d3 = format.parse(str_endtime);
            str_end_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d3);

            str_duration = calculateDiff(str_starttime, str_endtime);
            int diff1 = 0;

            str_log1 = str_job + "  " + str_start_HH_mm_ss + " - " + str_end_HH_mm_ss + "  " + str_duration;

            if ("WDBKD".equals(str_CC_D_B)) {
                sheetnum = 1;
            } else if ("WDCCD".equals(str_CC_D_B)) {
                sheetnum = 2;
            } else if ("WDDLD".equals(str_CC_D_B)) {
                sheetnum = 3;
            } else if ("WDCOD".equals(str_CC_D_B)) {
                sheetnum = 4;
            }

            row_num = row_num + 2;

            String str_output= str_endtime.substring(str_endtime.indexOf(" "), str_endtime.length());
            str_output = str_output.replace(" ","");
            //AppendExcelFile(row_num, str_job, str_start_HH_mm_ss, str_end_HH_mm_ss, str_duration, sheetnum);
System.out.println(str_output);
           // System.out.println(str_CC_D_B + " " + str_log1);
        } else if ((!"".equals(str_starttime)) && ("".equals(str_endtime))) {
            System.out.println(str_job + " (" + str_startcode + " " + str_endcode + ")" + str_starttime.substring(str_starttime.indexOf(" "), str_starttime.length()) + ", NOT FOUND");
        } else if ("".equals(str_starttime)) {
            System.out.println(str_job + " (" + str_startcode + " " + str_endcode + ") NOT FOUND, NOT FOUND");
        }

        return str_log;
    }

    public static void AppendExcelFile(int row_num, String str_job, String str_starttime, String str_endtime, String str_duration, int sheetnum) throws FileNotFoundException, IOException, InvalidFormatException {

        System.out.println(str_endtime);
        FileInputStream file = new FileInputStream(new File("batch_summary.xlsx"));

        Workbook wb = WorkbookFactory.create(file);

        Sheet sheet = wb.getSheet("BANK");;

        if (sheetnum == 1) {
            sheet = wb.getSheet("BANK");
        } else if (sheetnum == 2) {
            sheet = wb.getSheet("CC");
        } else if (sheetnum == 3) {
            sheet = wb.getSheet("DELTA");
        } else if (sheetnum == 4) {
            sheet = wb.getSheet("WDCO");
        }

        sheet.setMargin(Sheet.TopMargin, 0.75);
        sheet.setColumnWidth(0, 1500);  //set column 0 width - No.
        sheet.setColumnWidth(1, 10000); //set column 1 width - jobs
       
        
        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);

        Row row = null;
        Cell cell = null;
        Cell cell1 = null;
        int column_num = 0;

        row = sheet.getRow(row_num);

        CellStyle style = wb.createCellStyle();

        for (int j = 2; j < 100; j++) {
            cell = row.getCell((short) j);
            if (cell == null) {
                column_num = j;
                sheet.setColumnWidth(j, 4300); //set column width - START to END
                cell = row.createCell((short) column_num);
                cell.setCellValue(str_starttime + " - " + str_endtime);
                cell.setCellStyle(style);

                sheet.setColumnWidth(j+1, 2000); //set column width - Duration
                style.setAlignment(CellStyle.ALIGN_RIGHT);
                style.setBorderLeft(CellStyle.BORDER_THIN);
                cell1 = row.createCell((short) column_num + 1);
                cell1.setCellValue(str_duration);
                cell1.setCellStyle(style);
                j = 99;
            }
        }

        try {
//            FileOutputStream out = new FileOutputStream(new File("batch_summary.xlsx"));
//            wb.write(out);
 //           out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void CreateExcelFile() throws FileNotFoundException, IOException, InvalidFormatException {

        File file = new File("batch_summary.xlsx");

        String str_sheet = null;
        int num = 0;
        if (!file.exists()) {
            file.createNewFile();

            FileOutputStream fos = new FileOutputStream(file, false);

            Workbook wb = new XSSFWorkbook();

            for (int i = 1; i <= 4; i++) {
                if (i == 1) {
                    str_sheet = "BANK";
                    num = total_bank_jobs;
                } else if (i == 2) {
                    str_sheet = "CC";
                    num = total_cc_jobs;
                }else if (i == 3) {
                    str_sheet = "DELTA";
                    num = total_delta_jobs;
                }else if (i == 4) {
                    str_sheet = "WDCO";
                    num = total_co_jobs;
                }
                Sheet sheet = wb.createSheet(str_sheet);
                sheet.setMargin(Sheet.TopMargin, 0.75);
                sheet.setColumnWidth(0, 500);

                Sheet sheet1 = wb.getSheet(str_sheet);

                sheet1.setMargin(Sheet.TopMargin, 0.75);
                sheet1.setColumnWidth(0, 500);

                Font font = wb.createFont();
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);

                CellStyle style = wb.createCellStyle();
                
                Row row = null;
                Cell cell0 = null;
                Cell cell1 = null;

                for (int j = 1; j <= num; j++) {
                    row = sheet1.getRow(j + 2);
                    if (row == null) {
                        row = sheet1.createRow((short) j + 2);
                    }

                    cell0 = row.getCell((short) 0);
                    if (cell0 == null) {
                        cell0 = row.createCell((short) 0);
                    }

                    style.setAlignment(CellStyle.ALIGN_CENTER);
                    cell0.setCellValue(j);
                    cell0.setCellStyle(style);
                    

                    cell1 = row.getCell((short) 1);
                    if (cell1 == null) {
                        cell1 = row.createCell((short) 1);
                    }

                    if (j < 10 && i == 1) {
                        cell1.setCellValue(BANK_JOBS.getString("JOB_0" + j));
                    } else if (j >= 10 && i == 1) {
                        cell1.setCellValue(BANK_JOBS.getString("JOB_" + j));
                    } else if (j < 10 && i == 2) {
                        cell1.setCellValue(CC_JOBS.getString("JOB_0" + j));
                    } else if (j >= 10 && i == 2) {
                        cell1.setCellValue(CC_JOBS.getString("JOB_" + j));
                    } else if (j < 10 && i == 3) {
                        cell1.setCellValue(DELTA_JOBS.getString("JOB_0" + j));
                    } else if (j >= 10 && i == 3) {
                        cell1.setCellValue(DELTA_JOBS.getString("JOB_" + j));
                    } else if (j < 10 && i == 4) {
                        cell1.setCellValue(CO_JOBS.getString("JOB_0" + j));
                    } else if (j >= 10 && i == 4) {
                        cell1.setCellValue(CO_JOBS.getString("JOB_" + j));
                    }
                }
            }

            try {
                FileOutputStream out = new FileOutputStream(new File("batch_summary.xlsx"));
                wb.write(out);
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
