
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jxl.write.WriteException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import static org.apache.poi.ss.usermodel.CellStyle.ALIGN_CENTER;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.ShapeTypes;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Rainbow_Summary_Template {

//    private static final String LOG_FOLDER = "D:\\UAT Timing\\20170816\\";
//    private static final String str_filename = "ProdTiming_";
//    private static final String smb_path = "smb://172.30.81.24/ebworx/Batch-Framework/log/";

    private static final String LOG_FOLDER = "D:\\DailyCatchUp\\20170919\\";   //Input folder  (date format yyyyMMdd)
    private static final String str_filename = "ProdTiming_";
    private static final String smb_path = "smb://10.188.0.41/ebworx/Batch-Framework/log/";
    
    private static final String str_current_date = LOG_FOLDER.substring(LOG_FOLDER.length() - 9, LOG_FOLDER.length() - 1);     //Create Graph Sheet
    public static long exceed_time_in_minutes = 5;
    private static final String KEYWORD_START = "START:";
    private static final String KEYWORD_END = "END:";
    private static final String KEYWORD_BATCH = " Batch ";
    private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String WRONG_DATE_FORMAT = "yyyy-dd-MM";
    private static final String CHANGE_DATE_FORMAT = "dd/MM/yyyy";
    private static final String CALCULATE_DATE_FORMAT = "yyyy-MM-dd";
    private static final String FILE_DATE_FORMAT = "ddMMyy";
    private static final String FOLDER_DATE_FORMAT = "yyyyMMdd";

    public static int str_job_maxlength = 32;
    public static int str_time_maxlength = 7;

    public static int bank_align = 27;
    public static int cc_align = 43;
    public static int delta_align = 53;

    private static final ResourceBundle CONFIG = ResourceBundle.getBundle("configs");

    public static void main(String[] args) throws FileNotFoundException, IOException, Exception {
        ResourceBundle.clearCache();

        //START Check input file and Output file
        String str_date = LOG_FOLDER.substring(LOG_FOLDER.length() - 9, LOG_FOLDER.length() - 1);

        String str_UAT = LOG_FOLDER.substring(LOG_FOLDER.indexOf("\\") + 1, LOG_FOLDER.indexOf("\\") + 4);

        DateFormat date_formatter = new SimpleDateFormat(FOLDER_DATE_FORMAT);
        Date todaydate = date_formatter.parse(str_date);
        Date dateBefore = new Date(todaydate.getTime() - 1 * 24 * 3600 * 1000);
        
        Date dateUAT = new Date(todaydate.getTime() - 6 * 24 * 3600 * 1000);
        String str_uat_date = new SimpleDateFormat(FOLDER_DATE_FORMAT).format(dateUAT);  //UAT date = Prod date - 6 days
    
        String str_in_date = new SimpleDateFormat(FILE_DATE_FORMAT).format(dateBefore);
        String str_out_date = new SimpleDateFormat(FILE_DATE_FORMAT).format(date_formatter.parse(str_date));

        File in_file = new File(LOG_FOLDER + str_filename + str_in_date + ".xlsx");     //input file
        File out_file = new File(LOG_FOLDER + str_filename + str_out_date + ".xlsx");     //Output file

        File folder = new File(LOG_FOLDER.substring(0, LOG_FOLDER.length() - 1));
        if (!folder.exists()) {
            if (folder.mkdirs()) {
                System.out.println("Folder created!");
            } else {
                System.out.println("No Folder Create!");
            }
        }

        FileInputStream inputfile = null;

        if (!in_file.exists()) { 
            copyInputFile(dateBefore, str_in_date);
            
            if (in_file.exists()) {
                inputfile = new FileInputStream(in_file);
                // cutInputFile(dateBefore, str_in_date);
            }

        } else {
            inputfile = new FileInputStream(in_file);
        }
        //END Check input file and Output file

        ////(0) Download LOG files
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String str_checktodaydate = dateFormat.format(date);

            if (   !new File(LOG_FOLDER + "WDBKD.log").exists()
                && !new File(LOG_FOLDER + "WDCCD.log").exists()
                && !new File(LOG_FOLDER + "WDDLD.log").exists()
                && !new File(LOG_FOLDER + "WDAPD.log").exists()
                && !new File(LOG_FOLDER + "WDCOD.log").exists()) {

            if (str_date.equals(str_checktodaydate) || (str_UAT.equals("UAT") && str_date.equals(str_checktodaydate))) {
                downloadfrom_smb(LOG_FOLDER.replace("\\", "/"), "WDBKD.log");
                downloadfrom_smb(LOG_FOLDER.replace("\\", "/"), "WDCCD.log");
                downloadfrom_smb(LOG_FOLDER.replace("\\", "/"), "WDCOD.log");
                downloadfrom_smb(LOG_FOLDER.replace("\\", "/"), "WDDLD.log");
                downloadfrom_smb(LOG_FOLDER.replace("\\", "/"), "WDAPD.log");
            } else {
                ResourceBundle.clearCache();

                if (str_UAT.equals("UAT")) {
                    str_date = str_uat_date; 
                }
                if (!new File(LOG_FOLDER + "log_" + str_date + ".zip").exists()) {
                    download_ZIP_from_smb(LOG_FOLDER.replace("\\", "/"), str_date);
                    if (new File(LOG_FOLDER + "log_" + str_date + ".zip").exists()){
                    unZipIt(str_date);
                    }else{
                        System.out.println(LOG_FOLDER + "log_" + str_date + ".zip not found");
                    }
                } else {
                    unZipIt(str_date);
                } 
            }
        }

        String str_proceed1 = checklog("WDBKD.log", "WDBKD0741B");
        String str_proceed2 = checklog("WDCCD.log", "WDCCD0366B");
        String str_proceed3 = checklog("WDDLD.log", "WDDLD0432B");
        String str_proceed4 = checklog("WDCOD.log", "WDCOD0017B");
        String str_proceed5 = checklog("WDAPD.log", "WDAPD0025B");

        if ("Y".equals(str_proceed1) && "Y".equals(str_proceed2) && "Y".equals(str_proceed3) && "Y".equals(str_proceed4) && "Y".equals(str_proceed5)) {
        
            if (inputfile != null) {
                ResourceBundle.clearCache();
                Workbook wb = new XSSFWorkbook(inputfile);

                ////==============Get all Sheets ==============
                int index_graph = 0;
                for (int i = 0; i < wb.getNumberOfSheets(); i++) {
                    if ("Summary".equals(wb.getSheetName(i))) {
                        index_graph = i;
                    }
                }
             
                ////(2) Write Summary show Start & End Time
              //  getSummaryStartEndTime(wb);

                ////(3) Write Template
              //  getTemplateWrite(wb, index_graph);
                
                ////(1) Draw Rainbow Chart
               getRainbowDraw(wb, index_graph);
                
                FileOutputStream out = null;
                  ResourceBundle.clearCache();
                try {
                    out = new FileOutputStream(out_file);
                    wb.write(out);
                   
                } catch (Exception e) {
                } finally {
                    out.close();
                    in_file.delete();
                }
                System.out.println("\nEND");
            }
        }

    }

    private static void copyInputFile(Date dateBefore, String str_in_date) throws FileNotFoundException, IOException {

        String strfolder = LOG_FOLDER.substring(0, LOG_FOLDER.length() - 9);

        InputStream inStream = null;
        OutputStream outStream = null;
        File afile = new File(strfolder
                + new SimpleDateFormat(FOLDER_DATE_FORMAT).format(dateBefore) + "\\"
                + str_filename + str_in_date + ".xlsx");

        File bfile = new File(LOG_FOLDER + str_filename + str_in_date + ".xlsx");

        if (!afile.exists() && !bfile.exists()) {
            System.out.println("No previous day file");
        } else {
            inStream = new FileInputStream(afile);
            outStream = new FileOutputStream(bfile);

            byte[] buffer = new byte[1024];

            int length;
            //copy the file content in bytes
            while ((length = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }

            System.out.println("File Copied !!!");

            inStream.close();
            outStream.close();

            ////delete the original file
            //afile.delete();
        }
    }

    private static void cutInputFile(Date dateBefore, String str_in_date) throws FileNotFoundException, IOException {

        File afile = new File("D:\\DailyCatchUp\\"
                + new SimpleDateFormat(FOLDER_DATE_FORMAT).format(dateBefore) + "\\"
                + str_filename + str_in_date + ".xlsx");

        if (afile.renameTo(new File(LOG_FOLDER + str_filename + str_in_date + ".xlsx"))) {
            System.out.println("File is moved successful!");
        } else {
            System.out.println("File is failed to move!");
        }
    }

    public static void downloadfrom_smb(String str_file_dir, String str_log) throws MalformedURLException, UnknownHostException, SmbException {
        String str_local_domain = CONFIG.getString("str_local_domain");
        String str_local_usr = CONFIG.getString("str_local_usr");
        String str_local_pwd = CONFIG.getString("str_local_pwd");

        //local
        String str_local_url = "smb://localhost/" + str_file_dir.replace(":", "$");
        NtlmPasswordAuthentication auth_local = new NtlmPasswordAuthentication(str_local_domain, str_local_usr, str_local_pwd);
        SmbFile sf_local = new SmbFile(str_local_url, auth_local);

        //smb
        //String url = CONFIG.getString("smb_path"); // "smb://10.188.0.41/ebworx/Batch-Framework/log/";
        String url = smb_path;
        String str_usr = CONFIG.getString("smb_usr");
        String str_pwd = CONFIG.getString("smb_pwd");
        NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, str_usr, str_pwd);
        SmbFile source = new SmbFile(url, auth);
        String str1 = "";

        for (SmbFile f : source.listFiles()) {
            str1 = f.getUncPath();
            str1 = str1.replace(str1.substring(0, f.getUncPath().indexOf("log") + 4), "");

            if (str_log.equals(str1)) {
                System.out.println("downloading... " + str1);
                f = new SmbFile(source, str1);
                sf_local = new SmbFile(sf_local, str1);
                f.copyTo(sf_local);
                 System.out.println("Download " + str_log + " Done");
            }else{
               //  System.out.println( str_log + " not found");
            }
        }
       
        ResourceBundle.clearCache();
    }

    public static void download_ZIP_from_smb(String str_file_dir, String str_date) throws MalformedURLException, UnknownHostException, SmbException {
        String str_local_domain = CONFIG.getString("str_local_domain");
        String str_local_usr = CONFIG.getString("str_local_usr");
        String str_local_pwd = CONFIG.getString("str_local_pwd");

        //local
        String str_local_url = "smb://localhost/" + str_file_dir.replace(":", "$");
        NtlmPasswordAuthentication auth_local = new NtlmPasswordAuthentication(str_local_domain, str_local_usr, str_local_pwd);
        SmbFile sf_local = new SmbFile(str_local_url, auth_local);

        //smb
        //String url = CONFIG.getString("smb_path"); // "smb://10.188.0.41/ebworx/Batch-Framework/log/";
        String url = smb_path;
        String str_usr = CONFIG.getString("smb_usr");
        String str_pwd = CONFIG.getString("smb_pwd");
        NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, str_usr, str_pwd);
        SmbFile source = new SmbFile(url, auth);
        String str1 = "";
        String str_date_ = "";
        for (SmbFile f : source.listFiles()) {
            str1 = f.getUncPath();
            str1 = str1.replace(str1.substring(0, f.getUncPath().indexOf("log") + 4), "");

            str_date_ = "log_" + str_date + ".zip";
            if (str_date_.equals(str1)) {
                System.out.println("Downloading... " + str1);
                f = new SmbFile(source, str1);
                sf_local = new SmbFile(sf_local, str1);
                f.copyTo(sf_local);
            }
        }
      
    }

    public static void unZipIt(String str_date) {
        
        byte[] buffer = new byte[1024];

        try {

            //create output directory is not exists
            File folder = new File(LOG_FOLDER.substring(0, LOG_FOLDER.length() - 1));
            if (!folder.exists()) {
                folder.mkdir();
            }

          //  System.out.println(LOG_FOLDER.substring(LOG_FOLDER.length() - 9, LOG_FOLDER.length() - 1));
            String str_zip = LOG_FOLDER + "log_" + str_date + ".zip";
            System.out.println("Unzip ......"+str_zip);
            //get the zip file content
            ZipInputStream zis = new ZipInputStream(new FileInputStream(str_zip));
            //get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {

                if ("log\\WDBKD.log".equals(ze.getName())
                        || "log\\WDCCD.log".equals(ze.getName())
                        || "log\\WDDLD.log".equals(ze.getName())
                        || "log\\WDCOD.log".equals(ze.getName())
                        || "log\\WDAPD.log".equals(ze.getName())) {
                    File newFile = new File(LOG_FOLDER.substring(0, LOG_FOLDER.length() - 1) + File.separator + ze.getName());

                    System.out.println("file unzip : " + newFile.getAbsoluteFile());

                    //create all non exists folders
                    //else you will hit FileNotFoundException for compressed folder
                    new File(newFile.getParent()).mkdirs();

                    FileOutputStream fos = new FileOutputStream(newFile);

                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }

                    fos.close();
                }
                ze = zis.getNextEntry();

            }

            zis.closeEntry();
            zis.close();

            File afile1 = new File(LOG_FOLDER + "log\\WDBKD.log");
            File afile2 = new File(LOG_FOLDER + "log\\WDCCD.log");
            File afile3 = new File(LOG_FOLDER + "log\\WDDLD.log");
            File afile4 = new File(LOG_FOLDER + "log\\WDCOD.log");
            File afile5 = new File(LOG_FOLDER + "log\\WDAPD.log");

            if (afile1.renameTo(new File(LOG_FOLDER + afile1.getName()))
                    && afile2.renameTo(new File(LOG_FOLDER + afile2.getName()))
                    && afile3.renameTo(new File(LOG_FOLDER + afile3.getName()))
                    && afile4.renameTo(new File(LOG_FOLDER + afile4.getName()))
                    && afile5.renameTo(new File(LOG_FOLDER + afile5.getName()))) {
                System.out.println("Unzip done!!! Files exist!");

            } else {
                System.out.println("Unzip done!!! Files failed to move!");
            }

            //delete extracted files
            File extractfile = new File(LOG_FOLDER + "log");
            extractfile.delete();

            //delete downloaded zip file
            File zipfile = new File(str_zip);
            zipfile.delete();

            System.out.println("Zip file deleted\n");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void CreateSummary(Workbook wb, int rownum, String str_date, String str_starttime, String str_endtime, String str_totaltime, String str_delay, String str_comment, int commentrownum) throws WriteException {

        //// =============== create summary ======================
        Sheet sheet_summary = wb.getSheet("Summary");
        Row row_s1 = null;
        Cell cell_s0 = null;
        row_s1 = sheet_summary.getRow(rownum);
        int createcell_num = 0;
        for (int j = 0; j < 100; j++) {
            cell_s0 = row_s1.getCell((short) j);
            if (cell_s0 != null) {
                if (cell_s0.toString() != null || !"".equals(cell_s0.toString())) {
                    // System.out.println(j + " " + cell_s0.toString());
                    createcell_num = j;
                }
            }
        }

//        System.out.println("CREATED SUMMARY FOR >> " + row_s1.getCell((short) 0).toString() + " >> DATE: >> " + row_s1.getCell((short) createcell_num).toString());
        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);

        CellStyle style1 = wb.createCellStyle();
        style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style1.setBorderRight(CellStyle.BORDER_THIN);
        style1.setBorderTop(CellStyle.BORDER_THIN);
        style1.setBorderBottom(CellStyle.BORDER_THIN);
        style1.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
        style1.setAlignment(ALIGN_CENTER);
        style1.setWrapText(true);
        style1.setFont(font);

        createcell_num = createcell_num + 1;

        row_s1 = sheet_summary.getRow(rownum);
        cell_s0 = row_s1.getCell((short) createcell_num);
        if (cell_s0 == null) {
            cell_s0 = row_s1.createCell((short) createcell_num);
        }

        sheet_summary.setColumnWidth(createcell_num, 5600);
        cell_s0.setCellValue(str_date);  //DATE
        cell_s0.setCellStyle(style1);

        rownum++;
        row_s1 = sheet_summary.getRow(rownum);
        cell_s0 = row_s1.getCell((short) createcell_num);
        if (cell_s0 == null) {
            cell_s0 = row_s1.createCell((short) createcell_num);
        }

        CellStyle style2 = wb.createCellStyle();
        style2.setBorderRight(CellStyle.BORDER_THIN);
        style2.setBorderTop(CellStyle.BORDER_THIN);
        style2.setBorderBottom(CellStyle.BORDER_THIN);

        cell_s0.setCellValue(str_starttime); //START
        cell_s0.setCellStyle(style2);
        rownum++;
        row_s1 = sheet_summary.getRow(rownum);
        cell_s0 = row_s1.getCell((short) createcell_num);
        if (cell_s0 == null) {
            cell_s0 = row_s1.createCell((short) createcell_num);
        }
        cell_s0.setCellValue(str_endtime); //END
        cell_s0.setCellStyle(style2);
        rownum++;

        row_s1 = sheet_summary.getRow(rownum);
        if (row_s1 == null) {
            row_s1 = sheet_summary.createRow(rownum);
        }
        cell_s0 = row_s1.getCell((short) createcell_num);
        if (cell_s0 == null) {
            cell_s0 = row_s1.createCell((short) createcell_num);
        }

        cell_s0.setCellValue(str_totaltime);  //TOTAL
        CellStyle style3 = wb.createCellStyle();
        style3.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style3.setBorderRight(CellStyle.BORDER_THIN);
        style3.setBorderTop(CellStyle.BORDER_THIN);
        style3.setBorderBottom(CellStyle.BORDER_THIN);
        style3.setFillForegroundColor(IndexedColors.RED.getIndex());
        style3.setFont(font);
        style3.setAlignment(ALIGN_CENTER);
        cell_s0.setCellStyle(style3);

        if (!"".equals(str_comment)) {
            rownum++;
            row_s1 = sheet_summary.getRow(rownum);
            cell_s0 = row_s1.getCell((short) createcell_num);
            if (cell_s0 == null) {
                cell_s0 = row_s1.createCell((short) createcell_num);
            }
            cell_s0.setCellValue(str_delay); //Delay

            CellStyle style4 = wb.createCellStyle();
            style4.setFont(font);
            cell_s0.setCellStyle(style4);

            CreationHelper richTextFactory = wb.getCreationHelper();
            Drawing drawing = sheet_summary.createDrawingPatriarch();
            ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, 0, 4, commentrownum); // col & row
            Comment comment1 = drawing.createCellComment(anchor);

            RichTextString rtf1 = richTextFactory.createRichTextString(str_comment);
            comment1.setString(rtf1);
            cell_s0.setCellComment(comment1);
            //=====================================================================
        }
    }

    public static String getlastline(String str_logfile, String str_lastcode) throws IOException {
        String last2Line = getlast2lines(LOG_FOLDER + str_logfile);
        String lastLine1 = last2Line.substring(0, last2Line.indexOf("|"));
        String lastLine2 = last2Line.substring(last2Line.indexOf("|") + 1, last2Line.length());
        String lastLine = "";

        if (str_lastcode.equals(lastLine2.substring(lastLine2.indexOf("WD"), lastLine2.indexOf("WD") + 10))) {
            lastLine = lastLine1.substring(lastLine1.indexOf("WD"), lastLine1.indexOf("WD") + 10);
        } else {
            lastLine = lastLine2.substring(lastLine2.indexOf("WD"), lastLine2.indexOf("WD") + 10);
        }
        return lastLine;
    }

    public static String getlast2lines(String str_logfile) throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(str_logfile));
        String[] lines = new String[2];
        String[] strlines = new String[2];
        int count = 0;
        String line = null;
        while ((line = br.readLine()) != null) {
            lines[count % lines.length] = line;
            count++;
        }

        int start = count - 2;
        if (start < 0) {
            start = 0;
        }
        int j = 0;
        for (int i = start; i < count; i++) {
            strlines[j] = lines[i % lines.length];
            j++;
        }

        String lastLine = strlines[0] + "|" + strlines[1];

        return lastLine;
    }

    public static String readlog(String str_CC_D_B, String str_logfile, String str_startcode, String str_endcode) throws FileNotFoundException, IOException, ParseException, InvalidFormatException {
        int diff = 0;

        String str_start_end = "";

        String line = null;

        int count = 0;

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_duration = "";
        String str_starttime = "";
        String str_endtime = "";

        while ((line1 = bufferedReader1.readLine()) != null) {

            if (line1.contains(str_startcode)) {
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 6, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");
                str_starttime = str_starttime.replaceAll(" 201", "201");
            }
            if (line1.contains(str_endcode)) {
                i = line1.indexOf("END:");
                str_endtime = line1.substring(i + 4, line1.length());
                str_endtime = str_endtime.replaceAll(" 201", "201");
            }
        }

        return str_start_end = str_starttime + "|" + str_endtime;
    }

    public static String calduration(String str_starttime, String str_endtime) {

        str_starttime = str_starttime.replace(",", " ");
        str_endtime = str_endtime.replace(",", " ");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        String str_duration = "";

        String str_diffHours = "";

        String str_diffMinutes = "";

        String str_diffSeconds = "";

        String str_output = "";
        try {
            d1 = format.parse(str_starttime);
            d2 = format.parse(str_endtime);

            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long lg_seconds = (diffHours * 3600) + (diffMinutes * 60) + diffSeconds;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            if (diffDays > 0) {
                diffHours = diffHours + (diffDays * 24);
            }
            if (diffHours < 10) {
                str_diffHours = "0" + diffHours;
            } else {
                str_diffHours = "" + diffHours;
            }

            if (diffMinutes < 10) {
                str_diffMinutes = "0" + diffMinutes;
            } else {
                str_diffMinutes = "" + diffMinutes;
            }

            if (diffSeconds < 10) {
                str_diffSeconds = "0" + diffSeconds;
            } else {
                str_diffSeconds = "" + diffSeconds;
            }
            str_output = str_diffHours + ":" + str_diffMinutes + ":" + str_diffSeconds;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_output;
    }

    public static String dateFormat(String str_date) throws ParseException {
        String result = "";
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-dd-MM");
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd MMMM yyyy");

        SimpleDateFormat formatter3 = new SimpleDateFormat("dd MMM yyyy");

        try {

            Date date = formatter1.parse(str_date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, -1);
            Date dateminusone = cal.getTime();

            result = formatter2.format(date) + "     Host File: " + formatter3.format(dateminusone) + "|" + formatter3.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String checklog(String str_logfile, String str_summary_job) throws IOException {
        if ("WDCOD.log".equals(str_logfile)) {
            modifiedLogFile(LOG_FOLDER + "WDCOD.log");
        }
        BufferedReader br = new BufferedReader(new FileReader(LOG_FOLDER + str_logfile));
        String[] lines = new String[2];
        String str_proceed = null;
        int count = 0;
        String line = null;
        String str_error_line = "";
        int summary_count = 0;

        while ((line = br.readLine()) != null) {
            lines[count % lines.length] = line;
            count++;
            if (line.contains(str_summary_job)) {
                if (line.contains("Error")) {
                    str_error_line += "\n ERROR: line " + count + " >" + line;
                }
                summary_count = count;
            } else if (line.contains("Error")) {
                str_error_line += "\n ERROR: line " + count + " >" + line;
            }
        }

        //No error & Got Summary
        if (str_error_line.equals("") && summary_count != 0) {
            str_error_line = "No Error";
            System.out.println(str_logfile + "> Total lines: " + count + ", Summary Log at " + summary_count + " (" + str_error_line + ")");
            str_proceed = "Y";
        } //Got error & No Summary
        else if (!str_error_line.equals("") && summary_count == 0) {
            System.out.println(str_logfile + "> Total lines: " + count + ", No Summary Log " + str_error_line);
            str_proceed = "N";
        } //No error & No Summary
        else if (str_error_line.equals("") && summary_count == 0) {
            str_error_line = "No Error";
            System.out.println(str_logfile + "> Total lines: " + count + ", No Summary Log" + " (" + str_error_line + ")");
            str_proceed = "N";
        } //Got error & Got Summary
        else if (!str_error_line.equals("") && summary_count != 0) {
            System.out.println(str_logfile + "> Total lines: " + count + ", Summary Log at " + summary_count + str_error_line);
            str_proceed = "N";
        }

        return str_proceed;
    }

    public static String cal_delay(String str_logfile, int num) throws FileNotFoundException, IOException, ParseException {

        String str_result = "";
        String str_ltime = "";
        String str_time = "";
        String str_stime = "";
        String lasttime = "";
        String starttime = "";
        String str_startjob = "";
        String str_endjob = "";
        String str_job = "";
        String str_result2 = "";
        String str_result1 = "";
        String sCurrentLine = "";
        BufferedReader br = new BufferedReader(new FileReader(str_logfile));

        ArrayList<String> list = new ArrayList<String>();
        ArrayList<String> result = new ArrayList<String>();
        int count = 0;
        while ((sCurrentLine = br.readLine()) != null) {

            if ((sCurrentLine.contains("R_WDCC_ETL") && !sCurrentLine.contains("WDCCD0333B")) && (sCurrentLine.contains("R_WDCC_ETL") && !sCurrentLine.contains("WDCCD0334B"))
                    || (sCurrentLine.contains("R_WDBK_ETL") && !sCurrentLine.contains("P_ETL_CIS_"))
                    || sCurrentLine.contains("R_WDDL_ETL")) {

                list.add(sCurrentLine);
            }
        }

        long total = 0;
        String str_ljob = "";
        String str_sjob = "";

        String str_f_output = "";
        for (int i = 0; i < list.size(); i++) {
            if (i != list.size() - 1) {

                lasttime = list.get(i);
                int lll = lasttime.indexOf("WD");
                str_ljob = lasttime.substring(lll, lll + 10);

                int ll = lasttime.indexOf("END:");
                lasttime = lasttime.substring(ll + 4, lasttime.length());

                int jj = lasttime.indexOf(",");
                lasttime = lasttime.substring(jj + 1, lasttime.length());
                lasttime = lasttime.replaceAll(" ", "");
                str_ltime = lasttime;

                starttime = list.get(i + 1);
                int sss = starttime.indexOf("WD");
                str_sjob = starttime.substring(sss, sss + 10);

                int ss = starttime.indexOf("START:");
                starttime = starttime.substring(ss + 5, starttime.length());

                int qq = starttime.indexOf(",");
                starttime = starttime.substring(qq + 1, starttime.indexOf("Batch"));
                starttime = starttime.replaceAll(" ", "");
                str_stime = starttime;

                if (!"".equals(cd_delay(str_ltime + " " + str_stime))) {
                    total = total + cd_total(str_ltime + " " + str_stime);
                }

                if (!"".equals(cd_delay(str_ltime + " " + str_stime))) {
                    if (!"WDBKD".equals(str_sjob.substring(0, 5))) {
                        str_result = "ETL " + cd_delay(str_ltime + " " + str_stime);
                    }

                    result.add(cd_delay(str_ltime + " " + str_stime)
                            + str_ljob + " " + list.get(i).substring(list.get(i).indexOf("START:"), list.get(i).length()) + "\n"
                            + str_sjob + " " + list.get(i + 1).substring(list.get(i + 1).indexOf("START:"), list.get(i + 1).length()) + "\n");

                }
            }
        }

        if (!result.isEmpty()) {
            if ("WDBKD".equals(result.get(0).substring(result.get(0).indexOf("WD"), result.get(0).indexOf("WD") + 5).replace("\n", ""))) {
                long diffSeconds = total / 1000 % 60;

                long diffMinutes = total / (60 * 1000) % 60;
                long diffHours = total / (60 * 60 * 1000) % 24;
                long lg_seconds = (diffHours * 3600) + (diffMinutes * 60) + diffSeconds;

                if (diffHours != 0 && diffMinutes != 0 && diffSeconds != 0) {
                    str_result = "ETL Delay " + diffHours + "h " + diffMinutes + "m";
                } else if (diffHours != 0 && diffMinutes == 0 && diffSeconds != 0) {
                    str_result = "ETL Delay " + diffHours + "h";
                } else if (diffHours != 0 && diffMinutes != 0 && diffSeconds == 0) {
                    str_result = "ETL Delay " + diffHours + "h " + diffMinutes + "m";
                } else if (diffHours == 0 && diffMinutes != 0 && diffSeconds != 0) {
                    str_result = "ETL Delay " + diffMinutes + "m";
                } else if (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0) {
                    str_result = "ETL Delay " + diffMinutes + "m";
                } else {
                    str_result = "ETL Delay " + diffSeconds + "s";
                }
                str_f_output += str_result + "\n";

            } else {

            }

            for (int i = 0; i < result.size(); i++) {
                str_f_output += result.get(i) + "\n";

            }
        }
        if (num == 0) {
            return str_result;
        } else {
            return str_f_output;
        }

    }

    public static String cd_delay(String str_time) throws ParseException {

        String dateStart = str_time.substring(0, str_time.indexOf(' '));
        String dateEnd = str_time.substring(str_time.indexOf(' ') + 1, str_time.length());

        String str_result = "";
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        long total = 0;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateEnd);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long exceedtime = exceed_time_in_minutes;
            //if time more than 5 mins, then show delay
            if (diff > (exceedtime * 60000)) {

                long diffSeconds = diff / 1000 % 60;
                total = diff;

                long diffMinutes = diff / (60 * 1000) % 60;
                long diffHours = diff / (60 * 60 * 1000) % 24;
                long lg_seconds = (diffHours * 3600) + (diffMinutes * 60) + diffSeconds;

                if (diffHours != 0 && diffMinutes != 0 && diffSeconds != 0) {
                    str_result = "Delay " + diffHours + "h " + diffMinutes + "m \n";
                } else if (diffHours != 0 && diffMinutes == 0 && diffSeconds != 0) {
                    str_result = "Delay " + diffHours + "h \n";
                } else if (diffHours != 0 && diffMinutes != 0 && diffSeconds == 0) {
                    str_result = "Delay " + diffHours + "h " + diffMinutes + "m\n";
                } else if (diffHours == 0 && diffMinutes != 0 && diffSeconds != 0) {
                    str_result = "Delay " + diffMinutes + "m \n";
                } else if (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0) {
                    str_result = "Delay " + diffMinutes + "m \n";
                } else {
                    str_result = "Delay " + diffSeconds + "s\n";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_result;
    }

    public static long cd_total(String str_time) throws ParseException {

        String dateStart = str_time.substring(0, str_time.indexOf(' '));
        String dateEnd = str_time.substring(str_time.indexOf(' ') + 1, str_time.length());

        String str_result = "";
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        long diff = 0;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateEnd);

            //in milliseconds
            diff = d2.getTime() - d1.getTime();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return diff;
    }

    public static void CreateExcelFile(Workbook wb, List<String> list) {
        //  Workbook wb = new XSSFWorkbook();
        List<String> list1 = new ArrayList<String>();
        list1.add("Time");

        for (int i = 0; i < list.size(); i++) {
            list1.add(list.get(i));
        }

        ////======== create sheet ====================
        Sheet sheet = wb.getSheet(str_current_date);
        sheet.setMargin(Sheet.TopMargin, 0.75);
        sheet.setColumnWidth(0, 500);

        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);

        //SET Time Cell
        Row time_row = null;
        Cell time_cell = null;
        int column_num = 2;

        CellStyle style = wb.createCellStyle();

        for (int i = 0; i < list1.size(); i++) {
            time_row = sheet.getRow(i);
            if (time_row == null) {
                time_row = sheet.createRow(i);
            }

            time_cell = time_row.createCell((short) column_num);
            if (i == 0 || i == list1.size() - 1) {
                style = wb.createCellStyle();
                style.setFont(font);
                style.setAlignment(CellStyle.ALIGN_CENTER);
                style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
                style.setBorderBottom(CellStyle.BORDER_THIN);
                style.setBorderLeft(CellStyle.BORDER_THIN);
            } else {
                style = wb.createCellStyle();
                style.setFont(font);
                style.setAlignment(CellStyle.ALIGN_CENTER);
                style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
                style.setBorderLeft(CellStyle.BORDER_THIN);
                style.setBorderRight(CellStyle.BORDER_THIN);
            }
            time_cell.setCellValue(list1.get(i));
            time_cell.setCellStyle(style);
        }

        //SET Color Cell
        sheet.setColumnWidth(0, 8900);
        Row row1 = sheet.getRow(1);
        Cell cell0 = row1.getCell((short) 0);
        if (cell0 == null) {
            cell0 = row1.createCell((short) 0);
        }
        style = wb.createCellStyle();
        style.setFont(font);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cell0.setCellValue("Color:");
        cell0.setCellStyle(style);

        short[] s = new short[13];
        String[] str_array = new String[13];

        str_array[0] = "Create Snapshot";
        s[0] = IndexedColors.LIGHT_YELLOW.getIndex();

        str_array[1] = "Prebatch";
        s[1] = IndexedColors.BROWN.getIndex();

        str_array[2] = "ETL";
        s[2] = IndexedColors.YELLOW.getIndex();

        str_array[3] = "CCRD ECA (PAYMENT & ABORT)";
        s[3] = IndexedColors.ORANGE.getIndex();

        str_array[4] = "PF ECA (PAYMENT & ABORT)";
        s[4] = IndexedColors.DARK_YELLOW.getIndex();

        str_array[5] = "Case Assignment - Collection";
        s[5] = IndexedColors.LIGHT_BLUE.getIndex();

        str_array[6] = "Case Assignment - Recovery";
        s[6] = IndexedColors.RED.getIndex();

        str_array[7] = "Auto-Dialler";
        s[7] = IndexedColors.GREEN.getIndex();

        str_array[8] = "ECA (ASSIGNMENT & REASSIGNMENT)";
        s[8] = IndexedColors.VIOLET.getIndex();

        str_array[9] = "Write Off";
        s[9] = IndexedColors.PINK.getIndex();

        str_array[10] = "Write Off Statement";
        s[10] = IndexedColors.TURQUOISE.getIndex();

        str_array[11] = "Others";
        s[11] = IndexedColors.GREY_40_PERCENT.getIndex();

        str_array[12] = "COAP_ETL";
        s[12] = IndexedColors.CORAL.getIndex();

        for (int i = 0; i < 13; i++) {
            Row row3 = sheet.getRow(i + 3);
            if (row3 == null) {
                row3 = sheet.createRow(i + 3);
            }
            cell0 = row3.getCell((short) 0);
            if (cell0 == null) {
                cell0 = row3.createCell((short) 0);
            }
            style = wb.createCellStyle();
            style.setFont(font);
            style.setAlignment(CellStyle.ALIGN_LEFT);
            style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setFillForegroundColor(s[i]);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
            cell0.setCellValue(str_array[i]);
            cell0.setCellStyle(style);
        }

        //SET TOP CELL
        style = wb.createCellStyle();
        style.setFont(font);
        style.setBorderBottom(CellStyle.BORDER_THIN);

        Row row_title = sheet.getRow(0);
        Cell cell13 = row_title.createCell((short) 13);
        cell13.setCellValue("BANK");
        style = wb.createCellStyle();

        Cell cell14 = row_title.createCell((short) 31);
        cell14.setCellValue("CC");
        cell14.setCellStyle(style);

        Cell cell15 = row_title.createCell((short) 44);
        cell15.setCellValue("DELTA");

        //SET top line alignment
        Cell cell00;
        style = wb.createCellStyle();
        for (int i = 3; i < delta_align; i++) {
            Row row3 = sheet.getRow(0);
            cell00 = row3.getCell((short) i);
            if (cell00 != null) {

            } else {
                cell00 = row3.createCell((short) i);
            }
            style.setBorderBottom(CellStyle.BORDER_THIN);

            cell00.setCellStyle(style);
        }

        //SET end line alignment
        Cell cell001;

        CellStyle style11 = wb.createCellStyle();

        style11 = wb.createCellStyle();
        for (int i = 3; i < delta_align; i++) {
            Row row4 = sheet.getRow(list1.size() - 1);
            cell001 = row4.getCell((short) i);
            if (cell001 != null) {

            } else {
                cell001 = row4.createCell((short) i);
            }
            style11.setBorderBottom(CellStyle.BORDER_THIN);

            cell001.setCellStyle(style);
        }

    }

    public static List createTimeStringArray(Workbook wb, String str_stream, int size) throws IOException, FileNotFoundException, InvalidFormatException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM,HH:mm:ss");
        Date DateStartTime = null;
        Date DateEndTime = null;
        String str_starttime = "";
        String str_endtime = "";

        if (str_stream.equals("WDBKD")) {
            //get start time

            str_starttime = getStarttime("WDBKD.log", LOG_FOLDER, 3, "R_WDBK_SNAP_SHOT", "WDBKD0701B", "WDBKD0701B");
            DateStartTime = sdf.parse(str_starttime);

            //get end time
            String str_time1 = "";
            File f1 = new File(LOG_FOLDER + "WDBKD.log");
            if (f1.exists()) {
                str_time1 = getEndTime(LOG_FOLDER + "WDBKD.log");
            } else {
                str_time1 = "not found";
            }

            String str_time2 = "";
            File f2 = new File(LOG_FOLDER + "WDCCD.log");
            if (f2.exists()) {
                str_time2 = getEndTime(LOG_FOLDER + "WDCCD.log");
            } else {
                str_time2 = "not found";
            }

            String str_time3 = "";
            File f3 = new File(LOG_FOLDER + "WDDLD.log");
            if (f3.exists()) {
                str_time3 = getEndTime(LOG_FOLDER + "WDDLD.log");
            } else {
                str_time3 = "not found";
            }

            Date matchDateTime1 = sdf.parse(str_time1);
            Date matchDateTime2 = sdf.parse(str_time2);
            Date matchDateTime3 = sdf.parse(str_time3);

            if (matchDateTime1.compareTo(matchDateTime2) == 1 && matchDateTime1.compareTo(matchDateTime3) == 1) {
                str_endtime = sdf.format(matchDateTime1);
                DateEndTime = matchDateTime1;

            } else if (matchDateTime2.compareTo(matchDateTime1) == 1 && matchDateTime2.compareTo(matchDateTime3) == 1) {
                str_endtime = sdf.format(matchDateTime2);
                DateEndTime = matchDateTime2;

            } else if (matchDateTime3.compareTo(matchDateTime1) == 1 && matchDateTime3.compareTo(matchDateTime2) == 1) {
                str_endtime = sdf.format(matchDateTime3);
                DateEndTime = matchDateTime3;
            }
        } else if (str_stream.equals("WDCOD")) {
            str_starttime = getStarttime("WDCOD.log", LOG_FOLDER, 3, "StartBatchNode", "WDCOD0001B", "WDCOD0001B");

            if (!str_starttime.equals("not found")) {
                DateStartTime = sdf.parse(str_starttime);
            } else {
                System.out.println("WDCOD start code " + str_starttime + "!");
            }
            //get end time
            str_endtime = "";
            File f1 = new File(LOG_FOLDER + "WDCOD.log");
            if (f1.exists()) {
                str_endtime = getEndTime(LOG_FOLDER + "WDCOD.log");
            } else {
                str_endtime = "not found";
            }

            DateEndTime = sdf.parse(str_endtime);
        }

        //create time list
        List<String> time_list_yyyyddmm_hhmmss = new ArrayList<String>();
        List<String> time_list_yyyyddmm_hmmss = new ArrayList<String>();
        List<String> time_list_hmmss = new ArrayList<String>();

        if (!str_starttime.equals("not found")) {

            //set date end time equal to matchDateTime add 30 minutes
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(DateEndTime);
            calendar.add(Calendar.MINUTE, 30);  //add 30 minutes
            DateEndTime = calendar.getTime();

            //create timeframe
            while (DateEndTime.compareTo(DateStartTime) == 1) {

                str_starttime = "" + str_starttime + " " + str_starttime.substring(str_starttime.indexOf(",") + 1, str_starttime.indexOf(":") - 2);
                time_list_yyyyddmm_hhmmss.add(str_starttime);
                calendar.setTime(DateStartTime);
                calendar.add(Calendar.MINUTE, 30); //add 30 minutes
                DateStartTime = calendar.getTime();
                str_starttime = sdf.format(DateStartTime);
            }

            String str_replace = "";

            for (int i = 0; i < time_list_yyyyddmm_hhmmss.size(); i++) {

                str_replace = time_list_yyyyddmm_hhmmss.get(i).replace("0 ", "0");
                str_replace = str_replace.replace(",0", ", ");

                time_list_yyyyddmm_hmmss.add(str_replace);

                time_list_hmmss.add(time_list_yyyyddmm_hhmmss.get(i).substring(time_list_yyyyddmm_hhmmss.get(i).indexOf(",") + 1, time_list_yyyyddmm_hhmmss.get(i).indexOf(":") + 3));
            }

            if (str_stream.equals("WDBKD")) {
                CreateExcelFile(wb, time_list_hmmss);
                Excel_drawLine(wb, time_list_hmmss);
            } else if (str_stream.equals("WDCOD")) {

                int startnum = size + 3;
                int endnum = size + time_list_hmmss.size() + 3;
                CreateExcelFile_WDCOD(wb, time_list_hmmss, startnum, endnum);
            }
        }
        return time_list_yyyyddmm_hmmss;
    }

    public static String getStarttime(String str_CC_D_B, String str_logfile, int columnnum, String str_job, String str_startcode, String str_endcode)
            throws FileNotFoundException, IOException, ParseException, InvalidFormatException {

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile + str_CC_D_B));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_starttime = "";
        String str_minutes = "";
        while ((line1 = bufferedReader1.readLine()) != null) {

            if (line1.contains(str_startcode)) {
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 7, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");
            }

        }

        if ("".equals(str_starttime)) {
            str_starttime = "not found";
        } else {
            str_minutes = str_starttime.substring(str_starttime.indexOf(":") + 1, str_starttime.indexOf(":") + 2);
            int num_minutes = Integer.parseInt(str_minutes);

            if (num_minutes < 3) {
                str_starttime = str_starttime.replace(str_starttime.substring(str_starttime.indexOf(":") + 1), "00:00");
            } else {
                str_starttime = str_starttime.replace(str_starttime.substring(str_starttime.indexOf(":") + 1), "30:00");
            }
        }
        return str_starttime;
    }

    public static String getEndTime(String str_logfile) throws FileNotFoundException, IOException {
        String lasttime = "";
        String sCurrentLine = "";
        BufferedReader br = new BufferedReader(new FileReader(str_logfile));

        while ((sCurrentLine = br.readLine()) != null && !" ".equals(br.readLine())) {
            lasttime = sCurrentLine;
        }

        int i = lasttime.indexOf("END:");
        lasttime = lasttime.substring(i + 5, lasttime.length());

        return lasttime;
    }

    public static void Excel_drawLine(Workbook wb, List<String> list) throws FileNotFoundException, IOException, InvalidFormatException {
        Sheet sheet = wb.getSheet(str_current_date);
        sheet.createFreezePane(3, 0);

        Cell cell_line1 = null;
        Cell cell_line2 = null;
        Cell cell_line3 = null;

        CellStyle style = wb.createCellStyle();
        for (int j = 0; j < list.size() + 1; j++) {
            Row row_line = sheet.getRow(j);
            cell_line1 = row_line.getCell(bank_align);
            cell_line2 = row_line.getCell(cc_align);
            cell_line3 = row_line.getCell(delta_align);
            if (cell_line1 == null) {
                cell_line1 = row_line.createCell(bank_align);
            }
            if (cell_line2 == null) {
                cell_line2 = row_line.createCell(cc_align);
            }
            if (cell_line3 == null) {
                cell_line3 = row_line.createCell(delta_align);
            }

            style.setBorderRight(CellStyle.BORDER_THIN);
            cell_line1.setCellStyle(style);
            cell_line2.setCellStyle(style);
            cell_line3.setCellStyle(style);
        }

        //SET top line allignment
        CellStyle style1 = wb.createCellStyle();

        Row row1_line = sheet.getRow(0);
        Cell cell11 = row1_line.getCell(bank_align);
        Cell cell22 = row1_line.getCell(cc_align);
        Cell cell33 = row1_line.getCell(delta_align);

        if (cell11 == null) {
            cell11 = row1_line.createCell(bank_align);
        }
        if (cell22 == null) {
            cell22 = row1_line.createCell(cc_align);
        }
        if (cell33 == null) {
            cell33 = row1_line.createCell(delta_align);
        }
        style1.setBorderBottom(CellStyle.BORDER_THIN);
        style1.setBorderRight(CellStyle.BORDER_THIN);

        cell11.setCellStyle(style1);
        cell22.setCellStyle(style1);
        cell33.setCellStyle(style1);

        //SET end line allignment
        CellStyle style2 = wb.createCellStyle();

        Row row2_line = sheet.getRow(list.size());
        Cell cell111 = row2_line.getCell(bank_align);
        Cell cell222 = row2_line.getCell(cc_align);
        Cell cell333 = row2_line.getCell(delta_align);
        if (cell111 == null) {
            cell111 = row1_line.createCell(bank_align);
        }
        if (cell222 == null) {
            cell222 = row1_line.createCell(cc_align);
        }
        if (cell333 == null) {
            cell333 = row1_line.createCell(delta_align);
        }

        style2.setBorderBottom(CellStyle.BORDER_THIN);
        style2.setBorderRight(CellStyle.BORDER_THIN);

        cell111.setCellStyle(style2);
        cell222.setCellStyle(style2);
        cell333.setCellStyle(style2);

    }

    public static void CreateExcelFile_WDCOD(Workbook wb, List<String> time_list_hmmss, int startnum, int endnum) throws FileNotFoundException, IOException {
        ////======== create sheet ====================
        Sheet sheet = wb.getSheet(str_current_date);
        sheet.setMargin(Sheet.TopMargin, 0.75);
        sheet.setColumnWidth(0, 500);

        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);

        Row row = null;

        CellStyle style = wb.createCellStyle();

        //SET TIME Cell
        Row time_row = sheet.getRow(startnum);
        if (time_row == null) {
            time_row = sheet.createRow(startnum);
        }

        Cell time_cell = time_row.createCell((short) 2);
        style = wb.createCellStyle();
        style.setFont(font);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        time_cell.setCellValue("Time");
        time_cell.setCellStyle(style);

        startnum = startnum + 1;
        int j = 0;
        for (int i = startnum; i < endnum + 1; i++) {

            row = sheet.getRow(i);
            if (row != null) {

            } else {
                row = sheet.createRow(i);
                Cell cell = row.createCell((short) 2);

                if (i == endnum) {
                    style = wb.createCellStyle();
                    style.setFont(font);
                    style.setAlignment(CellStyle.ALIGN_CENTER);
                    style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
                    style.setBorderLeft(CellStyle.BORDER_THIN);
                    style.setBorderRight(CellStyle.BORDER_THIN);
                    style.setBorderBottom(CellStyle.BORDER_THIN);

                } else {
                    style = wb.createCellStyle();
                    style.setFont(font);
                    style.setAlignment(CellStyle.ALIGN_CENTER);
                    style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
                    style.setBorderLeft(CellStyle.BORDER_THIN);
                    style.setBorderRight(CellStyle.BORDER_THIN);
                }
                cell.setCellValue(time_list_hmmss.get(j));
                cell.setCellStyle(style);
                j++;
            }
        }

        //SET Color Cell
        sheet.setColumnWidth(0, 8900);

        Row row1 = sheet.getRow(startnum);
        Cell cell0 = row1.createCell((short) 0);

        style = wb.createCellStyle();
        style.setFont(font);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cell0.setCellValue("Color:");
        cell0.setCellStyle(style);

        short[] s = new short[20];
        String[] str_array = new String[20];

        str_array[0] = "Housekeeping";
        s[0] = IndexedColors.TURQUOISE.getIndex();

        str_array[1] = "Others";
        s[1] = IndexedColors.GREY_40_PERCENT.getIndex();

        str_array[2] = "CSVFileGeneration";
        s[2] = IndexedColors.CORNFLOWER_BLUE.getIndex();

        str_array[3] = "FTPCSVFile";
        s[3] = IndexedColors.PLUM.getIndex();

        j = 0;
        for (int i = startnum; i < startnum + 4; i++) {
            Row row3 = sheet.getRow(i);
            if (row3 == null) {
                row3 = sheet.createRow(i);
            }
            cell0 = row3.getCell((short) 0);
            if (cell0 == null) {
                cell0 = row3.createCell((short) 0);
            }

            style = wb.createCellStyle();
            style.setFont(font);
            style.setAlignment(CellStyle.ALIGN_LEFT);
            style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setFillForegroundColor(s[j]);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
            cell0.setCellValue(str_array[j]);
            cell0.setCellStyle(style);
            j++;
        }

        style = wb.createCellStyle();
        style.setFont(font);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

        //SET TOP Line
        Row row_title = sheet.getRow(startnum - 1);
        if (row_title == null) {
            row_title = sheet.createRow(startnum - 1);
        }

        style = wb.createCellStyle();
        style.setFont(font);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBorderTop(CellStyle.BORDER_THIN);

        for (int i = 3; i < 18; i++) {
            Cell cell13 = row_title.createCell((short) i);
            if (i == 10) {
                cell13.setCellValue("WDCOD");
                cell13.setCellStyle(style);
            } else {
                cell13.setCellStyle(style);
            }
        }

        //SET BOTTOM Line
        Row last_row = sheet.getRow(endnum);
        if (last_row == null) {
            last_row = sheet.createRow(endnum);
        }

        style = wb.createCellStyle();
        style.setFont(font);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        for (int i = 3; i < 18; i++) {
            Cell cell14 = last_row.createCell((short) i);
            cell14.setCellStyle(style);
        }

//        //SET RIGHT BORDER
        Row row_line;
        Cell setright_cell = null;

        for (int i = startnum - 1; i < endnum + 1; i++) {
            row_line = sheet.getRow(i);
            setright_cell = row_line.getCell((short) 17);

            if (setright_cell == null) {
                setright_cell = row_line.createCell(17);
            }

            if (i == startnum - 1) {
                style = wb.createCellStyle();
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderTop(CellStyle.BORDER_THIN);
                style.setBorderBottom(CellStyle.BORDER_THIN);

            } else if (i == endnum) {
                style = wb.createCellStyle();
                style.setBorderRight(CellStyle.BORDER_THIN);
                style.setBorderBottom(CellStyle.BORDER_THIN);
            } else {
                style = wb.createCellStyle();
                style.setBorderRight(CellStyle.BORDER_THIN);
            }
            setright_cell.setCellStyle(style);
        }

    }

    public static void WDBKD(Workbook wb, String str_CC_D_B, String str_logfile, List<String> time_list_yyyyddmm_hmmss, int num) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
        String str_others = "";

        System.out.println("********* BANK ************");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 3, "R_WDBK_SNAP_SHOT", "WDBKD0701B", "WDBKD0701B", "LIGHT_YELLOW", 1, 2, "1h30m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 4, "R_PREBATCH", "WDBKD0376B", "WDBKD0381B", "BROWN", 1, 2, "7m - 45m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 5, "R_WDBK_ETL", "WDBKD0571B", "WDBKD0573B", "YELLOW", 1, 2, "2h30m - 4h30m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 6, "COAP_PF_ETL", "WDBKD0799B", "WDBKD0806B", "CORAL", 1, 2, "2h30m - 4h30m", 1);

        System.out.println("");
        String str_others1 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCRD_ECA_PAYMENT", "WDBKD0493B", "WDBKD0498B", "", 0, 0, "", 100);
        String str_others2 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCRD_ECA_ABORT", "WDBKD0316B", "WDBKD0319B", "", 0, 0, "", 100);
        str_others = str_others1 + str_others2;
        System.out.println("========================================================================");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 7, "CCRD_ECA_PAY_ABORT", "WDBKD0493B", "WDBKD0319B", "ORANGE", 4, 2, "8m - 20m", 1);
        System.out.println("========================================================================\n");

        str_others = "";
        String str_others33 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PF_ECA_PAYMENT", "WDBKD0764B", "WDBKD0770B", "", 0, 0, "", 100);
        String str_others44 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PF_ECA_ABORT", "WDBKD0771B", "WDBKD0775B", "", 0, 0, "", 100);
        str_others = str_others33 + str_others44;
        System.out.println("========================================================================");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 8, "PF_ECA_PAY_ABORT", "WDBKD0764B", "WDBKD0775B", "DARK_YELLOW", 4, 2, "8m - 20m", 1);
        System.out.println("========================================================================\n");

        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 9, "CCRD_CL", "WDBKD0001B", "WDBKD0018B", "LIGHT_BLUE", 1, 2, "1h30m - 3h30m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 10, "CCRD_RC", "WDBKD0180B", "WDBKD0198B", "RED", 1, 2, "1h30m - 2h30m", 1);     //CCRD CASE ASSIGNMENT (CCRD_RC_ASGN)  WDBKD0180B   WDBKD0198B 

        System.out.println("=============================================");
        System.out.println(cal_Total(str_logfile, "CCRD_CL_+_CCRD_RC_ASGN", "WDBKD0001B", "WDBKD0198B"));
        System.out.println("=============================================\n");

        //===========================CASE ASSIGNMENT======================================
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 11, "CRECC_CL", "WDBKD0276B", "WDBKD0293B", "LIGHT_BLUE", 1, 2, "30m - 1h", 1); //CRECC CASE ASSIGNMENT (CRECC_CL_ASGN)  WDBKD0276B WDBKD0293B 								   
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 12, "CRSME_RC", "WDBKD0199B", "WDBKD0217B", "RED", 1, 2, "15m - 20m", 1); //CRSME CASE ASSIGNMENT (CRSME_RC_ASGN) WDBKD0199B WDBKD0217B 
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 13, "CRSME_CGC", "WDBKD0301B", "WDBKD0309B", "RED", 1, 2, "4m - 8m", 1); //CRSME CGC CASE ASSIGNMENT (CRSME_CGC_ASGN) WDBKD0301B WDBKD0309B 										

        System.out.println("=============================================");
        System.out.println(cal_Total(str_logfile, "CRECC_CL_+_CRSME_RC_ASGN", "WDBKD0276B", "WDBKD0217B"));
        System.out.println(cal_Total(str_logfile, "CRECC_CL_+_CRSME_CGC_ASGN", "WDBKD0276B", "WDBKD0309B"));
        System.out.println("=============================================\n");

        //===========================CASE ASSIGNMENT======================================
        str_others1 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_PF_CL", "WDCCD0019B", "WDCCD0029B", "", 0, 0, "", 100);
        str_others33 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "P_COAP_UPDATE_CASE_PF", "WDCCD0402B", "WDCCD0402B", "", 0, 0, "", 100);
        str_others44 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_PF_CL", "WDCCD0030B", "WDCCD0036B", "", 0, 0, "", 100);
        str_others = str_others1 + str_others33 + str_others44;
        System.out.println("=============================================");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 14, "CCPF_PF_CL", "WDCCD0019B", "WDCCD0036B", "LIGHT_BLUE", 4, 3, "1h30m", 1); //CCPF-PF CASE ASSIGNMENT (CCPF_PF_CL_ASGN)   WDCCD0019B WDCCD0036B  
        System.out.println("=============================================\n");

        //R_WDCC_PF_CL   WDCCD0402B  WDCCD0036B
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 15, "R_WDBK_CCPF_AD", "WDBKD0269B", "WDBKD0273B", "GREEN", 1, 2, "40m - 1h30m", 1); //AutoDialer WDBKD0269B  WDBKD0273B
        System.out.println("=============================================");
        System.out.println(cal_Total(str_logfile, "CCPF_PF_CL+R_WDBK_CCPF_AD", "WDCCD0019B", "WDBKD0273B"));
        System.out.println("=============================================\n");

        //=================MAXIMUM =============================
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 16, "CCRD_CL_+_CCRD_RC_ASGN", "WDBKD0001B", "WDBKD0198B", "GREY_40_PERCENT", 4, 2, "3h 30m", 1);

        //==========================ECA ASSIGNMENT, ECA REASSIGNMENT============================== 
        System.out.println("");
        String str_others3 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCRD_ECA_ASGN", "WDBKD0320B", "WDBKD0329B", "", 0, 0, "", 100);
        String str_others4 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCRD_ECA_REASGN", "WDBKD0330B", "WDBKD0339B", "", 0, 0, "", 100);
        str_others = str_others3 + str_others4;
        System.out.println("========================================================================");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 17, "CCRD_ECA_ASGN_REASGN", "WDBKD0320B", "WDBKD0339B", "VIOLET", 4, 2, "2m - 3m", 1);
        System.out.println("========================================================================\n");

        //==========================PRERPT, POSTRPT===========================
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 22, "CRECC_CL_PRERPT", "WDBKD0434B", "WDBKD0437B", "GREY_40_PERCENT", 1, 2, "2h45m - 3h45m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 18, "CCRD_CL_PRERPT", "WDBKD0467B", "WDBKD0470B", "GREY_40_PERCENT", 1, 2, "2h45m - 3h45m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 19, "CCRD_RC_PRERPT", "WDBKD0425B", "WDBKD0428B", "GREY_40_PERCENT", 1, 2, "8m - 2h34m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 20, "CRSME_RC_PRERPT", "WDBKD0447B", "WDBKD0451B", "GREY_40_PERCENT", 1, 2, "6m - 20m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 21, "CRSME_CGC_PRERPT", "WDBKD0483B", "WDBKD0486B", "GREY_40_PERCENT", 1, 2, "47s - 6m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 22, "CCPF_PF_CL_PRERPT", "WDCCD0215B", "WDCCD0218B", "GREY_40_PERCENT", 1, 2, "30m - 2h", 1);

        System.out.println("");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 23, "CRECC_CL_POSTRPT", "WDBKD0475B", "WDBKD0478B", "GREY_40_PERCENT", 1, 2, "38s - 1m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 24, "CCRD_CL_POSTRPT", "WDBKD0471B", "WDBKD0474B", "GREY_40_PERCENT", 1, 2, "7m - 11m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 25, "CCRD_RC_POSTRPT", "WDBKD0457B", "WDBKD0461B", "GREY_40_PERCENT", 1, 2, "2m - 6m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 26, "CRSME_RC_POSTRPT", "WDBKD0479B", "WDBKD0482B", "GREY_40_PERCENT", 1, 2, "2m - 5m", 1);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 27, "CCPF_PF_CL_POSTRPT", "WDCCD0219B", "WDCCD0222B", "GREY_40_PERCENT", 1, 2, "1m", 1);
        //// readlog(str_others ,str_CC_D_B,str_logfile,24,"CCPF_PF_RC_POSTRPT", "WDCCD0237B", "WDCCD0241B","GREY_40_PERCENT",6,2);

//            //==========================OTHERS (e.g. PTP, Call Escalation==============================  
        System.out.println("\n--------------------------------- OTHERS ---------------------------------------------------");
        String str_others5 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CRECC-CL", "WDBKD0417B", "WDBKD0421B", "", 0, 0, "", 100);
        String str_others6 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CCRD-CL", "WDBKD0412B", "WDBKD0416B", "", 0, 0, "", 100);
        String str_others7 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CCRD-RC", "WDBKD0364B", "WDBKD0368B", "", 0, 0, "", 100);
        String str_others8 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CRSME-RC", "WDBKD0369B", "WDBKD0373B", "", 0, 0, "", 100);
        String str_others9 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CCPF-PF-CL", "WDCCD0191B", "WDCCD0195B", "", 0, 0, "", 100);
        String str_others10 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CCPF-PF-RC", "WDCCD0159B", "WDCCD0163B", "", 0, 0, "", 100);
        String str_others11 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CRECC-CL", "WDBKD0394B", "WDBKD0399B", "", 0, 0, "", 100);
        String str_others12 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CCRD-CL", "WDBKD0382B", "WDBKD0387B", "", 0, 0, "", 100);
        String str_others13 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CCRD-RC", "WDBKD0388B", "WDBKD0393B", "", 0, 0, "", 100);
        String str_others14 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CRSME-RC", "WDBKD0400B", "WDBKD0405B", "", 0, 0, "", 100);
        String str_others15 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CCPF-PF-CL", "WDCCD0285B", "WDCCD0290B", "", 0, 0, "", 100);
        String str_others16 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CCPF-PF-RC", "WDCCD0291B", "WDCCD0296B", "", 0, 0, "", 100);
        System.out.println("-------------------------------------------------------------------------------------------");
        str_others = str_others5 + str_others6 + str_others7 + str_others8 + str_others9 + str_others10 + "\n"
                + str_others11 + str_others12 + str_others13 + str_others14 + str_others15 + str_others16;
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 17, "OTHERS", "WDBKD0417B", "WDCCD0296B", "GREY_40_PERCENT", 4, 11, "12m - 52m", 1);

        System.out.println("");
        str_others = "";
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, LOG_FOLDER + "WDCCD.log", 20, "CCPF_PF_PERF_MON_002", "WDCCD0382B", "WDCCD0386B", "GREY_40_PERCENT", 1, 2, "30m - 2h", 1);

        str_others = "";
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 19, "CCRD_CL_PERF_MON_002", "WDBKD0776B", "WDBKD0780B", "GREY_40_PERCENT", 2, 3, "4m - 16m", 1);
        System.out.println("");

        System.out.println("");
        str_others = "";
        str_others1 = read_others(str_CC_D_B, str_logfile, "Generate_LOADS_Acct_No", "WDBKD0649B", "WDBKD0652B");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others1, str_CC_D_B, str_logfile, 3, "OTHERS", "WDBKD0649B", "WDBKD0652B", "GREY_40_PERCENT", 4, 2, "12m - 52m", 1);

        str_others2 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "GENERATE_REMINDER_BATCH", "WDBKD0640B", "WDBKD0647B", "", 0, 0, "", 100);
        str_others3 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Exception_Report", "WDBKD0258B", "WDBKD0275B", "", 0, 0, "", 100);
        str_others4 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "GenerateBrokenLinkageAcct&Cif", "WDBKD0696B", "WDBKD0700B", "", 0, 0, "", 100);
        str_others5 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "ETLUpdateAcctFromCustomerFile", "WDBKD0053B", "WDBKD0099B", "", 0, 0, "", 100);
        str_others = str_others2 + "\n" + str_others3 + "\n" + str_others4 + "\n" + str_others5;
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 4, "OTHERS", "WDBKD0640B", "WDBKD0099B", "GREY_40_PERCENT", 4, 7, "12m - 52m", 1);

    }

    public static void WDCCD(Workbook wb, String str_CC_D_B, String str_logfile, List<String> time_list_yyyyddmm_hmmss, int num) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
        System.out.println("\n********* CC ************");
        String str_others = "";
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 28, "R_WDCC_ETL", "WDCCD0138B", "WDCCD0140B", "YELLOW", 2, 2, "30m - 3h", 1);

        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 29, "COAP_CC_ETL", "WDCCD0393B", "WDCCD0400B", "CORAL", 2, 2, "30m - 3h", 1);

        //R_WDBK_CC_PF_WO (8 jobs)
        System.out.println("");

        String str_others01 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Start_Batch_Node", "WDCCD0373B", "WDCCD0373B", "", 0, 0, "", 100);
        String str_others02 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Set_Date_Control", "WDCCD0374B", "WDCCD0374B", "", 0, 0, "", 100);
        String str_others03 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Set_Batch_Recovery_Date", "WDCCD0375B", "WDCCD0375B", "", 0, 0, "", 100);
        String str_others04 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Write_Off", "WDBKD0294B", "WDBKD0294B", "", 0, 0, "", 100);
        String str_others05 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Write_Off_Statement", "WDBKD0295B", "WDBKD0295B", "", 0, 0, "", 100);
        String str_others06 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Write_Off_Islamic", "WDBKD0297B", "WDBKD0297B", "", 0, 0, "", 100);
        String str_others07 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Write_Off_Islamic_Statement", "WDBKD0298B", "WDBKD0298B", "", 0, 0, "", 100);
        String str_others08 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "End_Batch_Node", "WDCCD0376B", "WDCCD0376B", "", 0, 0, "", 100);
        str_others = str_others01 + str_others02 + str_others03 + "\n" + str_others04 + str_others05 + str_others06 + str_others07 + "\n" + str_others08;
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 30, "R_WDCC_CCPF_WO", "WDCCD0373B", "WDCCD0376B", "PINK", 3, 2, "15m - 2h15m", 1);
        System.out.println("");

        String str_others14 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_ECA_PAYMENT", "WDCCD0248B", "WDCCD0253B", "", 0, 0, "", 100);
        String str_others15 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_ECA_ABORT", "WDCCD0106B", "WDCCD0109B", "", 0, 0, "", 100);
        str_others = str_others14 + str_others15;
        System.out.println("========================================================================");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 31, "R_WDCC_CCPF_ECA_PAY_ABORT", "WDCCD0248B", "WDCCD0109B", "ORANGE", 3, 2, "8m - 15m", 1);
        System.out.println("========================================================================\n");

        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 32, "R_WDCCD_PRERPT_CC_CL", "WDCCD0201B", "WDCCD0204B", "GREY_40_PERCENT", 2, 2, "4m - 16m", 1);

        //// ==============CCPF_CC_RC_PRERPT=============
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 33, "R_WDCCD_PRERPT_CC_RC", "WDCCD0227B", "WDCCD0231B", "GREY_40_PERCENT", 2, 2, "40m - 50m", 1);

        System.out.println("");

        //// ============== PTP, Call Escalation ===========
        System.out.println("------------------------------------------------------------------------------");
        String str_others1 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_CL_PTP", "WDCCD0196B", "WDCCD0200B", "", 0, 0, "", 100);
        String str_others2 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_RC_PTP", "WDCCD0154B", "WDCCD0158B", "", 0, 0, "", 100);
        String str_others3 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_CL_CE", "WDCCD0179B", "WDCCD0184B", "", 0, 0, "", 100);
        String str_others4 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_RC_CE", "WDCCD0185B", "WDCCD0190B", "", 0, 0, "", 100);
        str_others = str_others1 + str_others2 + "\n" + str_others3 + str_others4;
        System.out.println("------------------------------------------------------------------------------");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 38, "Others", "WDCCD0196B", "WDCCD0190B", "GREY_40_PERCENT", 4, 5, "4m - 16m", 1);
        System.out.println("");

        ////===========================CCPF_CC_PERF_MON_002======================================
        str_others = "";
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 34, "CCPF_CC_PERF_MON_002", "WDCCD0377B", "WDCCD0381B", "GREY_40_PERCENT", 2, 3, "4m - 16m", 1);
        System.out.println("");

        ////===========================CASE ASSIGNMENT======================================
        str_others1 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_CL", "WDCCD0001B", "WDCCD0011B", "", 0, 0, "", 100);
        String str_others33 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "P_COAP_UPDATE_CASE_CC", "WDCCD0401B", "WDCCD0401B", "", 0, 0, "", 100);
        String str_others44 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_CL", "WDCCD0012B", "WDCCD0018B", "", 0, 0, "", 100);
        str_others = str_others1 + str_others33 + str_others44;
        System.out.println("=============================================");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 35, "R_WDCC_CC_CL", "WDCCD0001B", "WDCCD0018B", "LIGHT_BLUE", 4, 3, "1h53m - 2h53m", 1);
        System.out.println("=============================================\n");

        System.out.println("");

        ////=============Auto Dialer ============
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 36, "R_WDCC_CC_AD", "WDCCD0323B", "WDCCD0327B", "GREEN", 2, 2, "20m - 40m", 1);
        System.out.println("");

        ////=============ECA ASGN, ECA REASGN============
        String str_others12 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_ECA_ASGN", "WDCCD0110B", "WDCCD0119B", "", 0, 0, "", 100);
        String str_others13 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_ECA_REASGN", "WDCCD0120B", "WDCCD0129B", "", 0, 0, "", 100);
        str_others = str_others12 + str_others13;
        System.out.println("=======================================================================");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 37, "R_WDCC_CCPF_ECA_ASSIGN_REASSIGN", "WDCCD0110B", "WDCCD0129B", "VIOLET", 4, 2, "3m - 4m", 1);
        System.out.println("=======================================================================\n");

        ////============== Others ====================
        System.out.println("------------------------------------------------------------------------------");
        String str_others5 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Update_MCIF_Number", "WDCCD0333B", "WDCCD0333B", "", 0, 0, "", 100);
        String str_others6 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "InsertPFActiveAccount", "WDCCD0334B", "WDCCD0334B", "", 0, 0, "", 100);
        str_others = str_others5 + str_others6;
        System.out.println("------------------------------------------------------------------------------");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 38, "Others", "WDCCD0333B", "WDCCD0334B", "GREY_40_PERCENT", 4, 2, "4m - 10m", 1);

        System.out.println("");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 39, "R_WDCC_CC_RC", "WDCCD0044B", "WDCCD0062B", "RED", 2, 2, "1h45m - 2h30m", 1);
        System.out.println("");

        /////============= CCPF_CC_CL_POSTRPT==================
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 40, "CCPF_CC_CL_POSTRPT", "WDCCD0205B", "WDCCD0208B", "GREY_40_PERCENT", 2, 2, "40m - 1h", 1);

        System.out.println("\n------------------------------------------------------------------------------");
        String str_others8 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_CC_LGL_CIVIL", "WDCCD0082B", "WDCCD0089B", "", 0, 0, "", 100);
        String str_others9 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_CC_SOLICITOR", "WDCCD0165B", "WDCCD0176B", "", 0, 0, "", 100);
        str_others = str_others8 + str_others9;
        System.out.println("------------------------------------------------------------------------------");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 41, "Others", "WDCCD0082B", "WDCCD0176B", "GREY_40_PERCENT", 4, 2, "25m - 35m", 1);
        System.out.println("");

        ////==============WRITE OFF=============
        str_others01 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Start_Batch_Node", "WDCCD0328B", "WDCCD0328B", "", 0, 0, "", 100);
        str_others02 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Set_Date_Control", "WDCCD0329B", "WDCCD0329B", "", 0, 0, "", 100);
        str_others03 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Set_Batch_Recovery_Date", "WDCCD0365B", "WDCCD0365B", "", 0, 0, "", 100);
        str_others04 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Write_Off_CSVS", "WDBKD0296B", "WDBKD0296B", "", 0, 0, "", 100);
        str_others05 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "End_Batch_Node", "WDCCD0330B", "WDCCD0330B", "", 0, 0, "", 100);
        str_others = str_others01 + str_others02 + str_others03 + str_others04 + str_others05;
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 42, "R_WDCC_CCPF_WO", "WDCCD0328B", "WDCCD0330B", "TURQUOISE", 4, 5, "15m - 2h15m", 1);

        System.out.println("");
        System.out.println("------------------------------------------------------------------------------");
        String str_others10 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_LEGAL_CSV", "WDBKD0562B", "WDBKD0564B", "", 0, 0, "", 100);
        String str_others11 = readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_CC_RC_POSTRPT", "WDCCD0232B", "WDCCD0236B", "", 0, 0, "", 100);
        str_others = str_others10 + str_others11;
        System.out.println("------------------------------------------------------------------------------");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 43, "Others", "WDBKD0562B", "WDCCD0236B", "GREY_40_PERCENT", 4, 2, "30m - 1h", 1);
    }

    public static void WDDLD(Workbook wb, String str_CC_D_B, String str_logfile, List<String> time_list_yyyyddmm_hmmss, int num) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
        System.out.println("\n********* DELTA ************");
        String str_others = "";
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 44, "R_WDDL_ETL", "WDDLD0090B", "WDDLD0092B", "YELLOW", 2, 2, "1h - 4h", 1);

        ///===========================ECA PAY_ABORT======================================
        System.out.println("");
        String str_others14 = read_others(str_CC_D_B, str_logfile, "AFABF_ECA_PAYMENT", "WDDLD0283B", "WDDLD0288B");
        String str_others15 = read_others(str_CC_D_B, str_logfile, "AFABF_ECA_ABORT", "WDDLD0175B", "WDDLD0178B");
        str_others = str_others14 + str_others15;
        System.out.println("=============================================================================");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 45, "AFABF_ECA_PAY_ABORT", "WDDLD0283B", "WDDLD0178B", "ORANGE", 4, 2, "7m - 15m", 1);
        System.out.println("=============================================================================\n");
        ////==========================AFABF_CL_PRERPT============================== 
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 46, "AFABF_CL_PRERPT", "WDDLD0275B", "WDDLD0278B", "GREY_40_PERCENT", 2, 2, "30m - 40m", 1);

        ////==========================Others==============================
        System.out.println("\n----------------------------------------------------------------------");
        String str_others1 = read_others(str_CC_D_B, str_logfile, "PTP-UPDATE AFABF-CL", "WDDLD0239B", "WDDLD0243B");
        String str_others2 = read_others(str_CC_D_B, str_logfile, "PTP-UPDATE AFABF-RC", "WDDLD0220B", "WDDLD0224B");
        String str_others3 = read_others(str_CC_D_B, str_logfile, "CallEscalationAFABF-CL", "WDDLD0227B", "WDDLD0232B");
        String str_others4 = read_others(str_CC_D_B, str_logfile, "CallEscalationAFABF-RC", "WDDLD0233B", "WDDLD0238B");
        str_others = str_others1 + str_others2 + "\n" + str_others3 + str_others4;
        System.out.println("----------------------------------------------------------------------");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 47, "Others", "WDDLD0239B", "WDDLD0238B", "GREY_40_PERCENT", 4, 5, "3m - 1h30m", 1);

        System.out.println("");
        ////===========================CASE ASSIGNMENT======================================
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 48, "R_WDDL_AFABF_CL", "WDDLD0015B", "WDDLD0032B", "LIGHT_BLUE", 2, 2, "48m - 1h", 1);
        System.out.println("");
        ////===========================Auto Dialer======================================
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 49, "R_WDDL_AFABF_AD", "WDDLD0142B", "WDDLD0147B", "GREEN", 2, 2, "45m - 56m", 1);
        System.out.println("");
        ////===========================CASE ASSIGNMENT======================================
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 50, "R_WDDL_AFABF_RC", "WDDLD0148B", "WDDLD0166B", "RED", 2, 2, "48m - 1h5m", 1);

        ////==========================Others============================== 
        System.out.println("\n------------------------------------------------------------------------");
        String str_others5 = read_others(str_CC_D_B, str_logfile, "AFABF_LGL_CIVIL", "WDDLD0167B", "WDDLD0174B");
        String str_others6 = read_others(str_CC_D_B, str_logfile, "AFABF_LGL_REPO", "WDDLD0200B", "WDDLD0207B");
        String str_others7 = read_others(str_CC_D_B, str_logfile, "AFABF-AKPK", "WDDLD0289B", "WDDLD0296B");
        System.out.println("------------------------------------------------------------------------");
        str_others = str_others5 + str_others6 + str_others7;
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 51, "Others", "WDDLD0167B", "WDDLD0296B", "GREY_40_PERCENT", 4, 4, "50m - 1h", 1);

        ////===========================ECA ASSIGN REASSIGN======================================
        System.out.println("");
        String str_others12 = read_others(str_CC_D_B, str_logfile, "AFABF_ECA_ASGN", "WDDLD0179B", "WDDLD0188B");
        String str_others13 = read_others(str_CC_D_B, str_logfile, "AFABF_ECA_REASGN", "WDDLD0189B", "WDDLD0198B");
        str_others = str_others12 + str_others13;
        System.out.println("==============================================================================================");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 52, "ECA_ASSIGN_REASSIGN", "WDDLD0179B", "WDDLD0198B", "VIOLET", 4, 2, "3m", 1);
        System.out.println("==============================================================================================");

        ////==========================Others============================== 
        System.out.println("\n------------------------------------------------------------------------");
        String str_others8 = read_others(str_CC_D_B, str_logfile, "AFABF_SOLICITOR", "WDDLD0208B", "WDDLD0219B");
        String str_others9 = read_others(str_CC_D_B, str_logfile, "AFABFOtherRecoveryBatch", "WDDLD0433B", "WDDLD0437B");
        String str_others10 = read_others(str_CC_D_B, str_logfile, "AFABF_CL_POSTRPT", "WDDLD0279B", "WDDLD0282B");
        String str_others11 = read_others(str_CC_D_B, str_logfile, "AFABF_RC_POSTRPT", "WDDLD0270B", "WDDLD0274B");
        System.out.println("------------------------------------------------------------------------");
        str_others = str_others8 + str_others9 + str_others10 + str_others11;
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 53, "Others", "WDDLD0208B", "WDDLD0274B", "GREY_40_PERCENT", 4, 4, "50m - 1h", 1);

    }

    public static void WDCOD(Workbook wb, String str_CC_D_B, String str_logfile, List<String> time_list_yyyyddmm_hmmss, int num) throws IOException, FileNotFoundException, ParseException, InvalidFormatException {
        System.out.println("\n********* WDCOD ************");
        String str_others = "";
        System.out.println("\n------------------------------------------------------------------------");
        String str_others1 = read_others(str_CC_D_B, str_logfile, "WDCOD0001B_StartBatchNode", "WDCOD0001B", "WDCOD0001B");
        String str_others2 = read_others(str_CC_D_B, str_logfile, "WDCOD0002B_SetDateControl", "WDCOD0002B", "WDCOD0002B");
        String str_others3 = read_others(str_CC_D_B, str_logfile, "WDCOD0014B_SetBatchRecoveryDate", "WDCOD0014B", "WDCOD0014B");
        String str_others4 = read_others(str_CC_D_B, str_logfile, "WDCOD0003B_Housekeeping", "WDCOD0003B", "WDCOD0003B");
        String str_others5 = read_others(str_CC_D_B, str_logfile, "WDCOD0018B_HouseKeepingGeneratedFiles", "WDCOD0018B", "WDCOD0018B");
        String str_others6 = read_others(str_CC_D_B, str_logfile, "WDCOD0004B_EndBatchNode", "WDCOD0004B", "WDCOD0004B");
        str_others = str_others1 + str_others2 + str_others3 + str_others4 + str_others5 + str_others6;
        System.out.println("------------------------------------------------------------------------");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 3, "Housekeeping", "WDCOD0001B", "WDCOD0004B", "TURQUOISE", 6, 6, "2m", num);

        System.out.println("\n------------------------------------------------------------------------");
        str_others1 = read_others(str_CC_D_B, str_logfile, "WDCOD0005B_StartBatchNode", "WDCOD0005B", "WDCOD0005B");
        str_others2 = read_others(str_CC_D_B, str_logfile, "WDCOD0006B_SetDateControl", "WDCOD0006B", "WDCOD0006B");
        str_others3 = read_others(str_CC_D_B, str_logfile, "WDCOD0015B_SetBatchRecoveryDate", "WDCOD0015B", "WDCOD0015B");
        str_others4 = read_others(str_CC_D_B, str_logfile, "WDCOD0020B_CompressFolder", "WDCOD0020B", "WDCOD0020B");
        str_others = str_others1 + str_others2 + str_others3 + str_others4;
        System.out.println("------------------------------------------------------------------------");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 4, "Others", "WDCOD0005B", "WDCOD0020B", "GREY_40_PERCENT", 6, 4, "2m", num);

        System.out.println("");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 5, "WDCOD0007B", "WDCOD0007B", "WDCOD0007B", "CORNFLOWER_BLUE", 1, 2, "2m", num);

        str_others = "";
        System.out.println("\n----------------10 CSVFileGeneration Concurrent-----------------------------------");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 6, "WDCOD0008B", "WDCOD0008B", "WDCOD0008B", "CORNFLOWER_BLUE", 1, 2, "2m", num);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 7, "WDCOD0009B", "WDCOD0009B", "WDCOD0009B", "CORNFLOWER_BLUE", 1, 2, "2m", num);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 8, "WDCOD0010B", "WDCOD0010B", "WDCOD0010B", "CORNFLOWER_BLUE", 1, 2, "2m", num);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 9, "WDCOD0011B", "WDCOD0011B", "WDCOD0011B", "CORNFLOWER_BLUE", 1, 2, "2m", num);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 10, "WDCOD0012B", "WDCOD0012B", "WDCOD0012B", "CORNFLOWER_BLUE", 1, 2, "2m", num);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 11, "WDCOD0021B", "WDCOD0021B", "WDCOD0021B", "CORNFLOWER_BLUE", 1, 2, "2m", num);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 12, "WDCOD0022B", "WDCOD0022B", "WDCOD0022B", "CORNFLOWER_BLUE", 1, 2, "2m", num);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 13, "WDCOD0023B", "WDCOD0023B", "WDCOD0023B", "CORNFLOWER_BLUE", 1, 2, "2m", num);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 14, "WDCOD0024B", "WDCOD0024B", "WDCOD0024B", "CORNFLOWER_BLUE", 1, 2, "2m", num);
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 15, "WDCOD0025B", "WDCOD0025B", "WDCOD0025B", "CORNFLOWER_BLUE", 1, 2, "2m", num);
        System.out.println("------------------------------------------------------------------------");

        System.out.println("");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 16, "FTPCsvFile", "WDCOD0019B", "WDCOD0019B", "PLUM", 2, 2, "2m", num);

        System.out.println("\n----------------------------------------------------------------------");
        str_others1 = read_others(str_CC_D_B, str_logfile, "WDCOD0016B_RemBatchRecoveryDate", "WDCOD0016B", "WDCOD0016B");
        str_others2 = read_others(str_CC_D_B, str_logfile, "WDCOD0013B_EndBatchNode", "WDCOD0013B", "WDCOD0013B");
        str_others = str_others1 + str_others2;
        System.out.println("------------------------------------------------------------------------");
        readlog(wb, time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 17, "Others", "WDCOD0016B", "WDCOD0013B", "GREY_40_PERCENT", 6, 2, "2m", num);
        System.out.println("\n");

    }

    public static String readlog(Workbook wb, List<String> time_list_yyyyddmm_hmmss, String str_others, String str_CC_D_B, String str_logfile, int columnnum, String str_job, String str_startcode, String str_endcode, String str_color, int col2, int row2, String str_estimatetime, int num) throws FileNotFoundException, IOException, ParseException, InvalidFormatException {
        String str_output = "";
        int diff = 0;

        if (str_job.length() < str_job_maxlength) {
            diff = str_job_maxlength - str_job.length();

            for (int z = 0; z < diff; z++) {
                str_job += " ";
            }
        }

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_duration = "";
        String str_starttime = "";
        String str_endtime = "";
        String str_starttime11 = "";
        String str_endtime11 = "";

        int startnum = 0;
        int endnum = 0;

        while ((line1 = bufferedReader1.readLine()) != null) {

            if (line1.contains(str_startcode)) {
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 6, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");

                if (" ".equals(str_starttime.substring(str_starttime.indexOf(",") + 1, str_starttime.indexOf(",") + 2))) {
                    str_starttime = str_starttime.replace(", ", ",x");
                    str_starttime = str_starttime.replaceAll(" ", "");
                    str_starttime = str_starttime.replaceAll("x", " ");
                } else {
                    str_starttime = str_starttime.replaceAll(" ", "");
                }

                str_starttime11 = str_starttime;
                str_starttime = str_starttime.replaceAll(",", " ");
            }
            if (line1.contains(str_endcode)) {
                i = line1.indexOf("END:");
                str_endtime = line1.substring(i + 4, line1.length());

                if (" ".equals(str_endtime.substring(str_endtime.indexOf(",") + 1, str_endtime.indexOf(",") + 2))) {
                    str_endtime = str_endtime.replace(", ", ",x");
                    str_endtime = str_endtime.replaceAll(" ", "");
                    str_endtime = str_endtime.replaceAll("x", " ");
                } else {
                    str_endtime = str_endtime.replaceAll(" ", "");
                }
                str_endtime11 = str_endtime;

                str_endtime = str_endtime.replaceAll(",", " ");
            }
        }

        if (!"".equals(str_endtime) && !"".equals(str_starttime)) {

            String str_start_minutes = str_starttime11.substring(str_starttime11.indexOf(":") + 1, str_starttime11.indexOf(":") + 2);
            int num_start_minutes = Integer.parseInt(str_start_minutes);

            String str_end_minutes = str_endtime11.substring(str_endtime11.indexOf(":") + 1, str_endtime11.indexOf(":") + 2);
            int num_end_minutes = Integer.parseInt(str_end_minutes);

            if (num_start_minutes < 3) {  //start time minute less than 30

                str_starttime11 = str_starttime11.substring(0, 14) + "00:00";

            } else {
                str_starttime11 = str_starttime11.substring(0, 14) + "30:00";
            }

            if (num_end_minutes < 3) {   //end time minute less than 30
                str_endtime11 = str_endtime11.substring(0, 14) + "00:00";
            } else {
                str_endtime11 = str_endtime11.substring(0, 14) + "30:00";
            }

            if ("WDCOD".equals(str_CC_D_B)) {
                startnum = num + 3; //draw start line at row number of list length plus 3 lines space
                endnum = num + 3;   //draw end line at row number of list length plus 3 lines space
            } else {
                startnum = 0;
                endnum = 0;
            }

            if (!"".equals(str_endtime11) && !"".equals(str_starttime11)) {

                for (int n = 0; n < time_list_yyyyddmm_hmmss.size(); n++) {

                    if ((str_starttime11).equals(time_list_yyyyddmm_hmmss.get(n))) {
                        startnum = startnum + n + 1;
                    }

                    if ((str_endtime11).equals(time_list_yyyyddmm_hmmss.get(n))) {
                        endnum = endnum + n + 2;
                    }
                }
            }

            SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

            Date d1 = null;
            Date d3 = null;
            d1 = format.parse(str_starttime);

            String str_yyyy_dd_MM = new SimpleDateFormat("yyyy-dd-MM").format(d1);

            d1 = format.parse(str_starttime);
            String str_start_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d1);

            d3 = format.parse(str_endtime);
            String str_end_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d3);

            str_duration = calculateDiff(str_starttime, str_endtime);
            int diff1 = 0;

            if (str_duration.length() < str_time_maxlength) {
                diff1 = str_time_maxlength - str_duration.length();

                for (int z = 0; z < diff1; z++) {
                    str_duration += " ";
                }
            }

            str_output = str_job.replace(" ", "") + " (" + str_duration.replace(" ", "") + ") From " + str_start_HH_mm_ss + " to " + str_end_HH_mm_ss + "\n";
            System.out.println(str_job + " (" + str_startcode + " " + str_endcode + ") " + str_start_HH_mm_ss + " - " + str_end_HH_mm_ss + " (" + str_duration + ")");

            str_job = str_job.replace(" ", "");
            str_duration = str_duration.replace(" ", "");

            if ("CCRD_CL_+_CCRD_RC_ASGN".equals(str_job) && num != 100) {
                drawing_right_brace(wb, str_duration, startnum, columnnum, endnum);
            } else if (num != 100) {
                WriteExcelFile(wb, str_others, str_job, str_start_HH_mm_ss, str_end_HH_mm_ss, str_duration, startnum, columnnum, endnum, str_color, col2, row2);
            }

        } else if ((!"".equals(str_starttime)) && ("".equals(str_endtime))) {
            str_output = str_job + " (" + str_startcode + " " + str_endcode + ")" + str_starttime.substring(str_starttime.indexOf(" "), str_starttime.length()) + ", NOT FOUND <<ESTIMATE finish within " + str_estimatetime + ">>";
            System.out.println(str_job + " (" + str_startcode + " " + str_endcode + ")" + str_starttime.substring(str_starttime.indexOf(" "), str_starttime.length()) + ", NOT FOUND <<ESTIMATE finish within " + str_estimatetime + ">>");
        } else if ("".equals(str_starttime)) {
            str_output = str_job + " (" + str_startcode + " " + str_endcode + ") NOT FOUND, NOT FOUND  <<ESTIMATE finish within " + str_estimatetime + ">>";
            System.out.println(str_job + " (" + str_startcode + " " + str_endcode + ") NOT FOUND, NOT FOUND  <<ESTIMATE finish within " + str_estimatetime + ">>");
        }
        return str_output;
    }

    public static String read_others(String str_CC_D_B, String str_logfile, String str_job, String str_startcode, String str_endcode) throws FileNotFoundException, IOException, ParseException, InvalidFormatException {
        int diff = 0;

        if (str_job.length() < str_job_maxlength) {
            diff = str_job_maxlength - str_job.length();

            for (int z = 0; z < diff; z++) {
                str_job += " ";
            }
        }
        String line = null;

        int count = 0;

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_log = "";
        String str_log1 = "";
        String str_duration = "";
        String str_starttime = "";
        String str_endtime = "";
        while ((line1 = bufferedReader1.readLine()) != null) {

            if (line1.contains(str_startcode)) {
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 6, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");
                str_starttime = str_starttime.replaceAll(" ", "");
                str_starttime = str_starttime.replaceAll(",", " ");
            }
            if (line1.contains(str_endcode)) {
                i = line1.indexOf("END:");
                str_endtime = line1.substring(i + 4, line1.length());
                str_endtime = str_endtime.replaceAll(" ", "");
                str_endtime = str_endtime.replaceAll(",", " ");
            }
        }

        if (!"".equals(str_endtime) && !"".equals(str_starttime)) {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

            Date d1 = null;
            Date d2 = null;
            Date d3 = null;
            d1 = format.parse(str_starttime);

            String str_yyyy_dd_MM = new SimpleDateFormat("yyyy-dd-MM").format(d1);

            String str_time = str_yyyy_dd_MM + " 0:00:00";
            d2 = format.parse(str_time);
            d1 = format.parse(str_starttime);
            String str_start_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d1);

            d3 = format.parse(str_endtime);
            String str_end_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d3);

            str_duration = calculateDiff(str_starttime, str_endtime);
            int diff1 = 0;

            if (str_duration.length() < str_time_maxlength) {
                diff1 = str_time_maxlength - str_duration.length();

                for (int z = 0; z < diff1; z++) {
                    str_duration += " ";
                }
            }

            str_log1 = str_job + " (" + str_startcode + " " + str_endcode + ") " + str_start_HH_mm_ss + " - " + str_end_HH_mm_ss + " (" + str_duration + ")";

            System.out.println(str_log1);

            str_job = str_job.replace(" ", "");
            str_duration = str_duration.replace(" ", "");
            str_log = str_job + " (" + str_duration + ") From " + str_start_HH_mm_ss + " to " + str_end_HH_mm_ss + "\n";
        } else if ((!"".equals(str_starttime)) && ("".equals(str_endtime))) {
            System.out.println(str_job + " (" + str_startcode + " " + str_endcode + ")" + str_starttime.substring(str_starttime.indexOf(" "), str_starttime.length()) + ", NOT FOUND");
        } else if ("".equals(str_starttime)) {
            System.out.println(str_job + " (" + str_startcode + " " + str_endcode + ") NOT FOUND, NOT FOUND");
        }

        return str_log;
    }

    public static String cal_Total(String str_logfile, String str_job, String str_startcode, String str_endcode) throws FileNotFoundException, IOException, ParseException {

        String line = null;

        int count = 0;

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_log = "";
        String str_duration = "";
        String str_starttime = "";
        String str_endtime = "";

        while ((line1 = bufferedReader1.readLine()) != null) {

            if (line1.contains(str_startcode)) {
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 6, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");
                str_starttime = str_starttime.replaceAll(" ", "");
                str_starttime = str_starttime.replaceAll(",", " ");
            }
            if (line1.contains(str_endcode)) {
                i = line1.indexOf("END:");
                str_endtime = line1.substring(i + 4, line1.length());
                str_endtime = str_endtime.replaceAll(" ", "");
                str_endtime = str_endtime.replaceAll(",", " ");
            }
        }

        String str_total = "";

        if ((!"".equals(str_starttime)) && (!"".equals(str_endtime))) {

            str_duration = calculateDiff(str_starttime, str_endtime);

            str_total = "(" + str_job + ") Total: " + str_duration;
        } else if ((!"".equals(str_starttime)) && ("".equals(str_endtime))) {
            str_total = str_job + ": NULL NOT YET FINISHED";
        }

        return str_total;
    }

    public static String calculateDiff(String str_starttime, String str_endtime) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        String str_duration = "";

        try {
            d1 = format.parse(str_starttime);
            d2 = format.parse(str_endtime);
//            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffDays = diff / (1000 * 60 * 60 * 24);

            long diffHours = 0;

            if (diffDays < 1) {
                diffHours = diff / (60 * 60 * 1000) % 24;
            } else {
                diffHours = (diffDays * 24) + (diff / (60 * 60 * 1000) % 24);
            }

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;

            long lg_seconds = (diffHours * 3600) + (diffMinutes * 60) + diffSeconds;

            if (diffHours != 0 && diffMinutes != 0 && diffMinutes < 10 && diffSeconds != 0) {
                str_duration = diffHours + "h  " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes < 10 && diffSeconds == 0) {
                str_duration = diffHours + "h  " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes >= 10 && diffSeconds != 0) {
                str_duration = diffHours + "h " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes >= 10 && diffSeconds == 0) {
                str_duration = diffHours + "h " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes == 0 && diffSeconds != 0) {
                str_duration = diffHours + "h ";
            } else if ((diffHours == 0 && diffMinutes != 0 && diffSeconds != 0 && diffMinutes < 10) || (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0 && diffMinutes < 10)) {
                str_duration = "    " + diffMinutes + "m";
            } else if ((diffHours == 0 && diffMinutes != 0 && diffSeconds != 0 && diffMinutes >= 10) || (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0 && diffMinutes >= 10)) {
                str_duration = "   " + diffMinutes + "m";
            } else {
                str_duration = "   " + diffSeconds + "s";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_duration;
    }

    private static void drawing_right_brace(Workbook wb, String str_duration, int startnum, int columnnum, int endnum) throws FileNotFoundException, IOException, ParseException, InvalidFormatException {
        int halfnum = (startnum + endnum) / 2;

        str_duration = "MAX " + str_duration;

        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);

        Sheet sheet = wb.getSheet(str_current_date);

        Row row1 = sheet.getRow(halfnum);
        Cell cell0 = row1.createCell((short) columnnum);

        CellStyle style = wb.createCellStyle();
        style = wb.createCellStyle();
        style.setFont(font);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setVerticalAlignment(CellStyle.VERTICAL_BOTTOM);
        style.setBorderLeft(CellStyle.BORDER_THIN);
        cell0.setCellValue(str_duration);
        cell0.setCellStyle(style);

        CreationHelper helper = wb.getCreationHelper();
        Drawing drawing = sheet.createDrawingPatriarch();

        ClientAnchor anchor = helper.createClientAnchor();

        ////start at columnnum=13
        anchor.setCol1(columnnum);
        anchor.setRow1(startnum);

        ////end at olumnnum=15
        anchor.setCol2(columnnum);
        anchor.setRow2(endnum);

        XSSFSimpleShape shape = ((XSSFDrawing) drawing).createSimpleShape((XSSFClientAnchor) anchor);
        shape.setShapeType(ShapeTypes.LEFT_BRACE);
        //shape.setFillColor(255, 255, 255);
        shape.setNoFill(true);
        shape.setLineWidth(2);
        shape.setLineStyle(0);
        shape.setLineStyleColor(0, 0, 0);

    }

    public static void WriteExcelFile(Workbook wb, String str_others, String str_job, String str_starttime, String str_endtime, String str_duration, int startnum, int columnnum, int endnum, String str_color, int col2, int row2) throws FileNotFoundException, IOException, InvalidFormatException {
        Sheet sheet = wb.getSheet(str_current_date);

        int length = str_job.length() * 100 + 2000;

        int timelength = str_duration.length() * 100 + 1200;

        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);

        CellStyle style = wb.createCellStyle();
        style.setWrapText(true);

        String str = "";

        Row row1 = sheet.getRow(startnum);

        if (row1 == null) {
            row1 = sheet.createRow(startnum);
        }
        String str_comment = "";

        Cell cell1 = row1.getCell((short) columnnum);
        if (cell1 == null) {
            cell1 = row1.createCell((short) columnnum);
        }
        
        str = str_job + "\n" + str_duration;
        if ("YELLOW".equals(str_color)) {      //ETL
            style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            str_comment = str_job + " (" + str_duration + ")\nFrom " + str_starttime + " to " + str_endtime;
            addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);
            
        } else if ("LIGHT_YELLOW".equals(str_color)) {      //Create Snapshot
            style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
            str = str_duration;
            str_comment = str_job + " (" + str_duration + ")\nFrom " + str_starttime + " to " + str_endtime;
            addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);

        } else if ("BROWN".equals(str_color)) {      //Prebatch
            style.setFillForegroundColor(IndexedColors.BROWN.getIndex());
            str = str_duration;
            str_comment = str_job + " (" + str_duration + ")\nFrom " + str_starttime + " to " + str_endtime;
            addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);

        } else if ("DARK_YELLOW".equals(str_color)) {  //PF ECA_PYMNT ECA_ABORT
            style.setFillForegroundColor(IndexedColors.DARK_YELLOW.getIndex());
            str = str_duration;
            addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);

        } else if ("ORANGE".equals(str_color)) {  //ECA_PYMNT ECA_ABORT
            style.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            str = str_duration;
            addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);

        } else if ("GREEN".equals(str_color)) { //AUTODIAL
            style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
            str = str_duration;
            str_comment = str_job + " (" + str_duration + ")\n From " + str_starttime + " to " + str_endtime;
            addCellComment( sheet,  wb,  col2,  row2, str_comment, str_others,cell1);
            
        } else if ("VIOLET".equals(str_color)) {  //ECA_ASGN ECA_REASGN
            style.setFillForegroundColor(IndexedColors.VIOLET.getIndex());
            str = str_duration;
            addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);

        } else if ("GREY_40_PERCENT".equals(str_color)) {       //OTHER
            if ("CRECC_CL_PRERPT".equals(str_job) || "CCRD_CL_PRERPT".equals(str_job) || "CCRD_RC_PRERPT".equals(str_job)
                    || "CRSME_RC_PRERPT".equals(str_job)
                    || "CRSME_CGC_PRERPT".equals(str_job) || "CCPF_PF_CL_PRERPT".equals(str_job) || "CRECC_CL_POSTRPT".equals(str_job)
                    || "CCRD_CL_POSTRPT".equals(str_job) || "CCRD_RC_POSTRPT".equals(str_job) || "CRSME_RC_POSTRPT".equals(str_job)
                    || "CCPF_PF_CL_POSTRPT".equals(str_job) || "CCPF_CC_RC_PRERPT".equals(str_job)
                    || "CCPF_CC_CL_POSTRPT".equals(str_job) || "AFABF_CL_PRERPT".equals(str_job)
                    || "R_WDCCD_PRERPT_CC_CL".equals(str_job) || "R_WDCCD_PRERPT_CC_RC".equals(str_job)
                    || "CCPF_CC_PERF_MON_002".equals(str_job) || "CCRD_CL_PERF_MON_002".equals(str_job) || "CCPF_PF_PERF_MON_002".equals(str_job)) {
                style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
                sheet.setColumnWidth(columnnum, length);
                str_comment = str_job + " (" + str_duration + ")\nFrom " + str_starttime + " to " + str_endtime;
                addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);

            } else {
                style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
                str = str_duration;
                addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);
            }

        } else if ("CORAL".equals(str_color)) {
            style.setFillForegroundColor(IndexedColors.CORAL.getIndex());
            str_comment = str_job + " (" + str_duration + ")\n From " + str_starttime + " to " + str_endtime;
            addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);
            
        } else if ("LIGHT_BLUE".equals(str_color)) {      //CL_ASGN
            if ("CCRD_CL".equals(str_job) || "CRECC_CL".equals(str_job)) {
                style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
                sheet.setColumnWidth(columnnum, length);
                str_comment = str_job + " (" + str_duration + ")\nFrom " + str_starttime + " to " + str_endtime;
                addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);

            } else if ("CCPF_PF_CL".equals(str_job) || "R_WDCC_CC_CL".equals(str_job)) {
                style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
                sheet.setColumnWidth(columnnum, length);
                addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);
                
            } else {
                style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
                str_comment = str_job + " (" + str_duration + ")\nFrom " + str_starttime + " to " + str_endtime;
                addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);
                
            }
        } else if ("RED".equals(str_color)) {
            if ("CCRD_RC".equals(str_job) || "CRSME_RC".equals(str_job) || "CRSME_CGC".equals(str_job)) {
                style.setFillForegroundColor(IndexedColors.RED.getIndex());
                sheet.setColumnWidth(columnnum, length);
                str_comment = str_job + " (" + str_duration + ")\nFrom " + str_starttime + " to " + str_endtime;
                addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);
                
            } else {
                style.setFillForegroundColor(IndexedColors.RED.getIndex());
                str_comment = str_job + " (" + str_duration + ")\nFrom " + str_starttime + " to " + str_endtime;
                addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);
                
            }
        } else if ("PINK".equals(str_color) && "R_WDCC_CCPF_WO".equals(str_job)) {      //18 WO
            style.setFillForegroundColor(IndexedColors.PINK.getIndex());
            str_comment = str_job + " (" + str_duration + ") From " + str_starttime + " to " + str_endtime;
            addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);

        } else if ("TURQUOISE".equals(str_color)) {
            style.setFillForegroundColor(IndexedColors.TURQUOISE.getIndex());
            addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);
            
        } else if ("CORNFLOWER_BLUE".equals(str_color)) {
            style.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
            str_comment = str_job + " (" + str_duration + ")\n From " + str_starttime + " to " + str_endtime;
            addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);
            
        } else if ("PLUM".equals(str_color)) {
            style.setFillForegroundColor(IndexedColors.PLUM.getIndex());
            str_comment = str_job + " (" + str_duration + ")\n From " + str_starttime + " to " + str_endtime;
            addCellComment( sheet,  wb,  col2,  row2, str_comment,str_others, cell1);
            
        }
        
        sheet.setColumnWidth(columnnum, timelength);

        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setBorderRight(CellStyle.BORDER_THIN);

        cell1.setCellValue(str);
        Row row = null;
        Cell cell = null;

        for (int i = startnum; i < endnum; i++) {
            row = sheet.getRow(i);
            if (row == null) {
                row = sheet.createRow((short) i);
            }
            cell = row.getCell((short) columnnum);
            if (cell == null) {
                cell = row.createCell((short) columnnum);
            }
            cell.setCellStyle(style);
        }

        for (int colNum = 0; colNum < row.getLastCellNum(); colNum++) {
            wb.getSheet(str_current_date).autoSizeColumn(colNum);
        }

    }

       public static void addCellComment(Sheet sheet, Workbook wb, int col2, int row2,String str_comment, String str_others, Cell cell1) {
                ResourceBundle.clearCache();
	        Drawing drawing = sheet.createDrawingPatriarch();
		CreationHelper factory = wb.getCreationHelper();

                ////XSSFClientAnchor(int dx1, int dy1, int dx2, int dy2, int col1, int row1, int col2, int row2)
		//final ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, 0, col2, row2);
 		
                    final ClientAnchor anchor = factory.createClientAnchor();
                    anchor.setDx1( 0 );
                    anchor.setDy1( 0 );
                    anchor.setDx2( 0 );
                    anchor.setDy2( 0 );
                    anchor.setCol1( 0 );
                    anchor.setRow1( 0 );
                    anchor.setCol2( col2 );
                    anchor.setRow2( row2 );
                    anchor.setAnchorType( ClientAnchor.MOVE_DONT_RESIZE );
                    
		Comment comment = drawing.createCellComment(anchor);
                
                
                if(!"".equals(str_comment)){
                    RichTextString str = factory.createRichTextString(str_comment);   
                    comment.setString(str);
                }else{
                    RichTextString str = factory.createRichTextString(str_others);
                    comment.setString(str);
                }
                
		cell1.setCellComment(comment);
                  ResourceBundle.clearCache();
	}
       
    public static void getSummaryStartEndTime(Workbook wb) throws IOException, FileNotFoundException, ParseException, InvalidFormatException, WriteException {
        ////Get Start & End time
        String str_start_end1 = readlog("WDBKD", LOG_FOLDER + "WDBKD.log", "WDBKD0571B", getlastline("WDBKD.log", "WDBKD0741B"));
        String str_starttime1 = str_start_end1.substring(0, str_start_end1.indexOf("|"));
        String str_endtime1 = str_start_end1.substring(str_start_end1.indexOf("|") + 1, str_start_end1.length());
        if (Integer.parseInt(str_starttime1.substring(str_starttime1.indexOf(",") + 1, str_starttime1.indexOf(":")).replace(" ", "")) >= 0 && Integer.parseInt(str_starttime1.substring(str_starttime1.indexOf(",") + 1, str_starttime1.indexOf(":")).replace(" ", "")) <= 9) {
            str_starttime1 = str_starttime1.replace(",", ", ");
        }
        if (Integer.parseInt(str_endtime1.substring(str_endtime1.indexOf(",") + 1, str_endtime1.indexOf(":")).replace(" ", "")) >= 0 && Integer.parseInt(str_endtime1.substring(str_endtime1.indexOf(",") + 1, str_endtime1.indexOf(":")).replace(" ", "")) <= 9) {
            str_endtime1 = str_endtime1.replace(",", ", ");
        }

        String str_start_end2 = readlog("WDCCD", LOG_FOLDER + "WDCCD.log", "WDCCD0138B", getlastline("WDCCD.log", "WDCCD0366B"));
        String str_starttime2 = str_start_end2.substring(0, str_start_end2.indexOf("|"));
        String str_endtime2 = str_start_end2.substring(str_start_end2.indexOf("|") + 1, str_start_end2.length());
        if (Integer.parseInt(str_starttime2.substring(str_starttime2.indexOf(",") + 1, str_starttime2.indexOf(":")).replace(" ", "")) >= 0 && Integer.parseInt(str_starttime2.substring(str_starttime2.indexOf(",") + 1, str_starttime2.indexOf(":")).replace(" ", "")) <= 9) {
            str_starttime2 = str_starttime2.replace(",", ", ");
        }
        if (Integer.parseInt(str_endtime2.substring(str_endtime2.indexOf(",") + 1, str_endtime2.indexOf(":")).replace(" ", "")) >= 0 && Integer.parseInt(str_endtime2.substring(str_endtime2.indexOf(",") + 1, str_endtime2.indexOf(":")).replace(" ", "")) <= 9) {
            str_endtime2 = str_endtime2.replace(",", ", ");
        }
        String str_start_end3 = readlog("WDDLD", LOG_FOLDER + "WDDLD.log", "WDDLD0090B", getlastline("WDDLD.log", "WDDLD0432B"));
        String str_starttime3 = str_start_end3.substring(0, str_start_end3.indexOf("|"));
        String str_endtime3 = str_start_end3.substring(str_start_end3.indexOf("|") + 1, str_start_end3.length());
        if (Integer.parseInt(str_starttime3.substring(str_starttime3.indexOf(",") + 1, str_starttime3.indexOf(":")).replace(" ", "")) >= 0 && Integer.parseInt(str_starttime3.substring(str_starttime3.indexOf(",") + 1, str_starttime3.indexOf(":")).replace(" ", "")) <= 9) {
            str_starttime3 = str_starttime3.replace(",", ", ");
        }
        if (Integer.parseInt(str_endtime3.substring(str_endtime3.indexOf(",") + 1, str_endtime3.indexOf(":")).replace(" ", "")) >= 0 && Integer.parseInt(str_endtime3.substring(str_endtime3.indexOf(",") + 1, str_endtime3.indexOf(":")).replace(" ", "")) <= 9) {
            str_endtime3 = str_endtime3.replace(",", ", ");
        }
        String str_start_end4 = readlog("WDCOD", LOG_FOLDER + "WDCOD.log", "WDCOD0001B", "WDCOD0013B");
        String str_starttime4 = str_start_end4.substring(0, str_start_end4.indexOf("|"));
        String str_endtime4 = str_start_end4.substring(str_start_end4.indexOf("|") + 1, str_start_end4.length());
        if (Integer.parseInt(str_starttime4.substring(str_starttime4.indexOf(",") + 1, str_starttime4.indexOf(":")).replace(" ", "")) >= 0 && Integer.parseInt(str_starttime4.substring(str_starttime4.indexOf(",") + 1, str_starttime4.indexOf(":")).replace(" ", "")) <= 9) {
            str_starttime4 = str_starttime4.replace(",", ", ");
        } else if (Integer.parseInt(str_starttime4.substring(str_starttime4.indexOf(",") + 1, str_starttime4.indexOf(":")).replace(" ", "")) >= 0 && Integer.parseInt(str_starttime4.substring(str_starttime4.indexOf(",") + 1, str_starttime4.indexOf(":")).replace(" ", "")) <= 17) {
            str_starttime4 = str_starttime4.replace(",", ", ");
        }

        if (Integer.parseInt(str_endtime4.substring(str_endtime4.indexOf(",") + 1, str_endtime4.indexOf(":")).replace(" ", "")) >= 0 && Integer.parseInt(str_endtime4.substring(str_endtime4.indexOf(",") + 1, str_endtime4.indexOf(":")).replace(" ", "")) <= 9) {
            str_endtime4 = str_endtime4.replace(",", ", ");
        }

        String str_start_end5 = readlog("WDAPD", LOG_FOLDER + "WDAPD.log", "WDAPD0001B", "WDAPD0025B");
        String str_starttime5 = str_start_end5.substring(0, str_start_end5.indexOf("|"));
        String str_endtime5 = str_start_end5.substring(str_start_end5.indexOf("|") + 1, str_start_end5.length());
        if (Integer.parseInt(str_starttime5.substring(str_starttime5.indexOf(",") + 1, str_starttime5.indexOf(":")).replace(" ", "")) >= 0 && Integer.parseInt(str_starttime5.substring(str_starttime5.indexOf(",") + 1, str_starttime5.indexOf(":")).replace(" ", "")) <= 9) {
            str_starttime5 = str_starttime5.replace(",", ", ");
        }
        if (Integer.parseInt(str_endtime5.substring(str_endtime5.indexOf(",") + 1, str_endtime5.indexOf(":")).replace(" ", "")) >= 0 && Integer.parseInt(str_endtime5.substring(str_endtime5.indexOf(",") + 1, str_endtime5.indexOf(":")).replace(" ", "")) <= 9) {
            str_endtime5 = str_endtime5.replace(",", ", ");
        }
        String str_getdate = "";
        String str_date = "";
        str_getdate = str_start_end1.substring(0, str_start_end1.indexOf(","));
        str_date = dateFormat(str_getdate).substring(0, dateFormat(str_getdate).indexOf("|"));

        String str_date1 = "";
        str_date1 = str_start_end1.substring(0, str_start_end1.indexOf(","));
        String str_date2 = dateFormat(str_date1).substring(dateFormat(str_date1).indexOf("|") + 1, dateFormat(str_date1).length());

        String[] lines1 = cal_delay(LOG_FOLDER + "WDBKD.log", 1).split("\r\n|\r|\n");

        String[] lines2 = cal_delay(LOG_FOLDER + "WDCCD.log", 1).split("\r\n|\r|\n");

        String[] lines3 = cal_delay(LOG_FOLDER + "WDDLD.log", 1).split("\r\n|\r|\n");

        int rownum = 8;
        CreateSummary(wb, rownum, str_date, str_starttime1, str_endtime1, calduration(str_starttime1, str_endtime1), cal_delay(LOG_FOLDER + "WDBKD.log", 0), cal_delay(LOG_FOLDER + "WDBKD.log", 1), lines1.length - 1);
        CreateSummary(wb, rownum += 6, str_date, str_starttime2, str_endtime2, calduration(str_starttime2, str_endtime2), cal_delay(LOG_FOLDER + "WDCCD.log", 0), cal_delay(LOG_FOLDER + "WDCCD.log", 1), lines2.length + 2);
        CreateSummary(wb, rownum += 6, str_date, str_starttime3, str_endtime3, calduration(str_starttime3, str_endtime3), cal_delay(LOG_FOLDER + "WDDLD.log", 0), cal_delay(LOG_FOLDER + "WDDLD.log", 1), lines3.length + 2);
        CreateSummary(wb, rownum += 6, str_date2, str_starttime4, str_endtime4, calduration(str_starttime4, str_endtime4), "", "", 0);
        CreateSummary(wb, rownum += 5, str_date2, str_starttime5, str_endtime5, calduration(str_starttime5, str_endtime5), "", "", 0);

        ResourceBundle.clearCache();
    }

    private static void getRainbowDraw(Workbook wb, int index_graph) throws IOException, FileNotFoundException, InvalidFormatException, ParseException {
        ////======== create sheet ====================
        Sheet sheet_date = wb.getSheet(str_current_date);
        if (sheet_date == null) {
            sheet_date = wb.createSheet(str_current_date);
            wb.setSheetOrder(str_current_date, index_graph);
        }

        ////START Draw rainbow chat
        List<String> time_list_yyyyddmm_hmmss = createTimeStringArray(wb, "WDBKD", 0);
        WDBKD(wb, "WDBKD", LOG_FOLDER + "WDBKD.log", time_list_yyyyddmm_hmmss, 1);
        WDCCD(wb, "WDCCD", LOG_FOLDER + "WDCCD.log", time_list_yyyyddmm_hmmss, 1);
        WDDLD(wb, "WDDLD", LOG_FOLDER + "WDDLD.log", time_list_yyyyddmm_hmmss, 1);
        List<String> time_list_yyyyddmm_hmmss_WDCO = createTimeStringArray(wb, "WDCOD", time_list_yyyyddmm_hmmss.size());
        WDCOD(wb, "WDCOD", LOG_FOLDER + "WDCOD.log", time_list_yyyyddmm_hmmss_WDCO, time_list_yyyyddmm_hmmss.size());
        ////END Draw rainbow chat  

        ResourceBundle.clearCache();
    }

    private static void getTemplateWrite(Workbook wb, int index_graph) throws FileNotFoundException, IOException, ParseException {
        Sheet sheet_template = wb.getSheet("Template");
        if (sheet_template == null) {
            sheet_template = wb.createSheet("Template");
            wb.setSheetOrder("Template", index_graph);
        }

        Row row1 = null;
        Cell cell1 = null;
        Cell cell2 = null;
        Cell cell3 = null;
        Cell cell4 = null;
        Cell cell5 = null;
        Cell cell6 = null;
        Cell cell7 = null;
        Cell cell8 = null;
        Cell cell0 = null;

        String[] inFile = new String[5];
        inFile[0] = LOG_FOLDER + "WDCOD.log";
        inFile[1] = LOG_FOLDER + "WDCCD.log";
        inFile[2] = LOG_FOLDER + "WDBKD.log";
        inFile[3] = LOG_FOLDER + "WDDLD.log";
        inFile[4] = LOG_FOLDER + "WDAPD.log";
        BufferedWriter bw = null;
        FileWriter fw = null;
        String line = "";
        String output = "";
        String content = "";
        int count = 0;
        //WDDLD0432B
        //WDCOD0017B
        //WDCCD0366B
        //WDBKD0741B

        int j = 1;
        String result = "";
        String detailText = "";
        String detClassName = "";
        String detBatchCode = "";
        String detCategory = "";
        int startPos = 0;
        int startLen = 0;
        int endPos = 0;
        int endLen = 0;
        int batchPos = 0;
        int batchLen = 0;
        String[] startArray = null;
        String[] endArray = null;
        String[] detailArray = null;

        String startDate = null;
        String startTime = null;
        String endDate = null;
        String endTime = null;
        DateFormat date_formatter = new SimpleDateFormat(WRONG_DATE_FORMAT);
        String startDateFinal = null;
        String endDateFinal = null;
        String status = null;
        String d1 = null;
        String d2 = null;
        DateFormat time_formatter = new SimpleDateFormat(TIME_FORMAT);
        Date t1 = null;
        Date t2 = null;
        long duration = 0;

        for (int i = 0; i < inFile.length; i++) {
            BufferedReader br = new BufferedReader(new FileReader(inFile[i]));
            while ((line = br.readLine()) != null) {
                // process record line-by-line
                line = line.trim(); // remove leading and trailing whitespace
                if (!line.equals("") // don't write out blank lines
                        && !line.substring(0, 10).equals("WDDLD0432B") // don't write out summary job
                        && !line.substring(0, 10).equals("WDCOD0017B")
                        && !line.substring(0, 10).equals("WDCCD0366B")
                        && !line.substring(0, 10).equals("WDBKD0741B")) {

                    startPos = line.indexOf(KEYWORD_START);
                    startLen = KEYWORD_START.length();
                    endPos = line.indexOf(KEYWORD_END);
                    endLen = KEYWORD_END.length();
                    batchPos = line.indexOf(KEYWORD_BATCH);
                    batchLen = KEYWORD_BATCH.length();
                    startArray = line.substring((startPos + startLen + 1), (batchPos)).split(",");
                    endArray = line.substring(endPos + endLen + 1).split(",");
                    detailArray = line.substring(0, startPos).trim().split(" ");

                    for (int z = 0; z < detailArray.length; z++) {
                        detailText = detailArray[z].replaceAll("\"", "").trim();
                        if (detailText.length() > 2) {
                            if (detailText.length() >= 4 && detailText.substring(0, 4).equals("com.")) {
                                String[] detailClassArray = detailText.split("\\.");
                                detClassName = detailClassArray[detailClassArray.length - 1];
                            }
                            if (detailText.length() >= 4 && detailText.substring(0, 2).equals("WD")) {
                                detBatchCode = detailText;
                            }
                            if (detailText.length() >= 2 && detailText.substring(0, 2).equals("R_")) {
                                detCategory = detailText;
                            }
                        }
                    }

                    startDate = startArray[0].trim();
                    startTime = startArray[1].trim();
                    endDate = endArray[0].trim();
                    endTime = endArray[1].trim();
                    date_formatter = new SimpleDateFormat(WRONG_DATE_FORMAT);
                    startDateFinal = new SimpleDateFormat(CHANGE_DATE_FORMAT).format(date_formatter.parse(startDate));
                    endDateFinal = new SimpleDateFormat(CHANGE_DATE_FORMAT).format(date_formatter.parse(endDate));
                    status = line.substring((batchPos + batchLen), (endPos)).trim();
                    d1 = new SimpleDateFormat(CALCULATE_DATE_FORMAT).format(date_formatter.parse(startDate));
                    d2 = new SimpleDateFormat(CALCULATE_DATE_FORMAT).format(date_formatter.parse(endDate));
                    time_formatter = new SimpleDateFormat(TIME_FORMAT);
                    t1 = time_formatter.parse(d1 + " " + startTime);
                    t2 = time_formatter.parse(d2 + " " + endTime);
                    duration = Math.abs(t2.getTime() - t1.getTime());
                    result = endDateFinal + "," + endTime + "," + detClassName + "," + detBatchCode + "," + detCategory + ",,," + duration + ',' + status;

                    // System.out.println(j + " " + result);
                    row1 = sheet_template.getRow(j);
                    if (row1 == null) {
                        row1 = sheet_template.createRow(j);
                    }

                    cell0 = row1.getCell((short) 0);
                    if (cell0 == null) {
                        cell0 = row1.createCell((short) 0);
                    }
                    cell0.setCellValue(endDateFinal);

                    cell1 = row1.getCell((short) 1);
                    if (cell1 == null) {
                        cell1 = row1.createCell((short) 1);
                    }
                    cell1.setCellValue(endTime);

                    cell2 = row1.getCell((short) 2);
                    if (cell2 == null) {
                        cell2 = row1.createCell((short) 2);
                    }
                    cell2.setCellValue(detClassName);

                    cell3 = row1.getCell((short) 3);
                    if (cell3 == null) {
                        cell3 = row1.createCell((short) 3);
                    }
                    cell3.setCellValue(detBatchCode);

                    cell4 = row1.getCell((short) 4);
                    if (cell4 == null) {
                        cell4 = row1.createCell((short) 4);
                    }
                    cell4.setCellValue(detCategory);

                    cell5 = row1.getCell((short) 5);
                    if (cell5 == null) {
                        cell5 = row1.createCell((short) 5);
                    }
                    cell5.setCellValue("");

                    cell6 = row1.getCell((short) 6);
                    if (cell6 == null) {
                        cell6 = row1.createCell((short) 6);
                    }
                    cell6.setCellValue("");

                    cell7 = row1.getCell((short) 7);
                    if (cell7 == null) {
                        cell7 = row1.createCell((short) 7);
                    }
                    cell7.setCellValue(duration);

                    cell8 = row1.getCell((short) 8);
                    if (cell8 == null) {
                        cell8 = row1.createCell((short) 8);
                    }
                    cell8.setCellValue(status);

                }
                j++;
            }
        }
        System.out.println("\nTotal lines of template: " + j);
        ResourceBundle.clearCache();
    }

    private static void modifiedLogFile(String FILENAME) throws FileNotFoundException, IOException {
        File inputFile = new File(FILENAME);
        File tempFile = new File(LOG_FOLDER + "WDCOD1.log");

        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

        String currentLine;
        int count = 0;
        while ((currentLine = reader.readLine()) != null) {

            // trim newline when comparing with lineToRemove
            //   System.out.println(count + " " + currentLine + " ");
            if (count < 25) {
                writer.write(currentLine + System.getProperty("line.separator"));
            }
            count++;
        }
        writer.close();
        reader.close();

        inputFile.delete();
        tempFile.renameTo(inputFile);

    }

    //https://www.programcreek.com/java-api-examples/index.php?source_dir=OOXML-master/src/org/apache/poi/xssf/usermodel/XSSFVMLDrawing.java
    //https://www.programcreek.com/java-api-examples/index.php?api=org.apache.poi.xssf.model.CommentsTable
    //https://bz.apache.org/bugzilla/attachment.cgi?id=31440&action=edit
    
}
