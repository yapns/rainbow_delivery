
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException; 
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class runTESTBatch {
 
    public static long exceed_time_in_minutes = 5;
    public static int str_job_maxlength = 32;
    public static int str_time_maxlength = 7;

    public static int bank_align = 27;
    public static int cc_align = 43;
    public static int delta_align = 53;
 
    public static void main(String[] args) throws FileNotFoundException, IOException, Exception {
        
        //String LOG_FOLDER = "D:\\DailyCatchUp\\20170102\\";
        //String outputFile = "check.txt";
        
        String LOG_FOLDER = args[0];
        String outputFile = args[1];
        
        BufferedWriter bw = null;
        FileWriter fw = null;

        File file = new File(outputFile);

        if (!file.exists()) {
            file.createNewFile();
        } else {
            file.delete();
            file.createNewFile();
        }

        fw = new FileWriter(file.getAbsoluteFile(), true);
        bw = new BufferedWriter(fw);

        
         
        //START Check input file and Output file
        String str_date = LOG_FOLDER.substring(LOG_FOLDER.length() - 9, LOG_FOLDER.length() - 1);
   
   
        String str_proceed1 = checklog("WDBKD.log", "WDBKD0741B", LOG_FOLDER, bw);
        String str_proceed2 = checklog("WDCCD.log", "WDCCD0366B", LOG_FOLDER, bw);
        String str_proceed3 = checklog("WDDLD.log", "WDDLD0432B", LOG_FOLDER, bw);
        // String str_proceed4 = checklog("WDCOD.log", "WDCOD0017B");
        // String str_proceed5 = checklog("WDAPD.log", "WDAPD0025B");

        bw.write("\n");
 
        getRainbowDraw(LOG_FOLDER, bw);

        try {

            if (bw != null) {
                bw.close();
            }

            if (fw != null) {
                fw.close();
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static String readlog(String str_CC_D_B, String str_logfile, String str_startcode, String str_endcode) throws FileNotFoundException, IOException, ParseException {
        int diff = 0;

        String str_start_end = "";

        String line = null;

        int count = 0;

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_duration = "";
        String str_starttime = "";
        String str_endtime = "";

        while ((line1 = bufferedReader1.readLine()) != null) {

            if (line1.contains(str_startcode)) {
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 6, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");
                str_starttime = str_starttime.replaceAll(" 201", "201");
            }
            if (line1.contains(str_endcode)) {
                i = line1.indexOf("END:");
                str_endtime = line1.substring(i + 4, line1.length());
                str_endtime = str_endtime.replaceAll(" 201", "201");
            }
        }

        return str_start_end = str_starttime + "|" + str_endtime;
    }

    public static String calduration(String str_starttime, String str_endtime) {

        str_starttime = str_starttime.replace(",", " ");
        str_endtime = str_endtime.replace(",", " ");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        String str_duration = "";

        String str_diffHours = "";

        String str_diffMinutes = "";

        String str_diffSeconds = "";

        String str_output = "";
        try {
            d1 = format.parse(str_starttime);
            d2 = format.parse(str_endtime);

            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long lg_seconds = (diffHours * 3600) + (diffMinutes * 60) + diffSeconds;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            if (diffDays > 0) {
                diffHours = diffHours + (diffDays * 24);
            }
            if (diffHours < 10) {
                str_diffHours = "0" + diffHours;
            } else {
                str_diffHours = "" + diffHours;
            }

            if (diffMinutes < 10) {
                str_diffMinutes = "0" + diffMinutes;
            } else {
                str_diffMinutes = "" + diffMinutes;
            }

            if (diffSeconds < 10) {
                str_diffSeconds = "0" + diffSeconds;
            } else {
                str_diffSeconds = "" + diffSeconds;
            }
            str_output = str_diffHours + ":" + str_diffMinutes + ":" + str_diffSeconds;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_output;
    }
 
    public static String checklog(String str_logfile, String str_summary_job, String LOG_FOLDER, BufferedWriter bw) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(LOG_FOLDER + str_logfile));
        String[] lines = new String[2];
        String str_proceed = null;
        int count = 0;
        String line = null;
        String str_error_line = "";
        int summary_count = 0;

        while ((line = br.readLine()) != null) {
            lines[count % lines.length] = line;
            count++;
            if (line.contains(str_summary_job)) {
                if (line.contains("Error")) {
                    str_error_line += "\n ERROR: line " + count + " >" + line;
                }
                summary_count = count;
            } else if (line.contains("Error")) {
                str_error_line += "\n ERROR: line " + count + " >" + line;
            }
        }

        //No error & Got Summary
        if (str_error_line.equals("") && summary_count != 0) {
            str_error_line = "No Error";
            bw.write("\n\n\n");
            bw.write(str_logfile + "> Total lines: " + count + ", Summary Log at " + summary_count + " (" + str_error_line + ")");
        } //Got error & No Summary
        else if (!str_error_line.equals("") && summary_count == 0) {
            bw.write("\n\n\n");
            bw.write(str_logfile + "> Total lines: " + count + ", No Summary Log " + str_error_line);
        } //No error & No Summary
        else if (str_error_line.equals("") && summary_count == 0) {
            bw.write("\n\n\n");
            str_error_line = "No Error";
            bw.write(str_logfile + "> Total lines: " + count + ", No Summary Log" + " (" + str_error_line + ")");
        } //Got error & Got Summary
        else if (!str_error_line.equals("") && summary_count != 0) {
            bw.write("\n\n\n");
            bw.write(str_logfile + "> Total lines: " + count + ", Summary Log at " + summary_count + str_error_line);
        }

        return str_proceed;
    }

    public static String cal_delay(String str_logfile, int num) throws FileNotFoundException, IOException, ParseException {

        String str_result = "";
        String str_ltime = "";
        String str_time = "";
        String str_stime = "";
        String lasttime = "";
        String starttime = "";
        String str_startjob = "";
        String str_endjob = "";
        String str_job = "";
        String str_result2 = "";
        String str_result1 = "";
        String sCurrentLine = "";
        BufferedReader br = new BufferedReader(new FileReader(str_logfile));

        ArrayList<String> list = new ArrayList<String>();
        ArrayList<String> result = new ArrayList<String>();
        int count = 0;
        while ((sCurrentLine = br.readLine()) != null) {

            if ((sCurrentLine.contains("R_WDCC_ETL") && !sCurrentLine.contains("WDCCD0333B")) && (sCurrentLine.contains("R_WDCC_ETL") && !sCurrentLine.contains("WDCCD0334B"))
                    || (sCurrentLine.contains("R_WDBK_ETL") && !sCurrentLine.contains("P_ETL_CIS_"))
                    || sCurrentLine.contains("R_WDDL_ETL")) {

                list.add(sCurrentLine);
            }
        }

        long total = 0;
        String str_ljob = "";
        String str_sjob = "";

        String str_f_output = "";
        for (int i = 0; i < list.size(); i++) {
            if (i != list.size() - 1) {

                lasttime = list.get(i);
                int lll = lasttime.indexOf("WD");
                str_ljob = lasttime.substring(lll, lll + 10);

                int ll = lasttime.indexOf("END:");
                lasttime = lasttime.substring(ll + 4, lasttime.length());

                int jj = lasttime.indexOf(",");
                lasttime = lasttime.substring(jj + 1, lasttime.length());
                lasttime = lasttime.replaceAll(" ", "");
                str_ltime = lasttime;

                starttime = list.get(i + 1);
                int sss = starttime.indexOf("WD");
                str_sjob = starttime.substring(sss, sss + 10);

                int ss = starttime.indexOf("START:");
                starttime = starttime.substring(ss + 5, starttime.length());

                int qq = starttime.indexOf(",");
                starttime = starttime.substring(qq + 1, starttime.indexOf("Batch"));
                starttime = starttime.replaceAll(" ", "");
                str_stime = starttime;

                if (!"".equals(cd_delay(str_ltime + " " + str_stime))) {
                    total = total + cd_total(str_ltime + " " + str_stime);
                }

                if (!"".equals(cd_delay(str_ltime + " " + str_stime))) {
                    if (!"WDBKD".equals(str_sjob.substring(0, 5))) {
                        str_result = "ETL " + cd_delay(str_ltime + " " + str_stime);
                    }

                    result.add(cd_delay(str_ltime + " " + str_stime)
                            + str_ljob + " " + list.get(i).substring(list.get(i).indexOf("START:"), list.get(i).length()) + "\n"
                            + str_sjob + " " + list.get(i + 1).substring(list.get(i + 1).indexOf("START:"), list.get(i + 1).length()) + "\n");

                }
            }
        }

        if (!result.isEmpty()) {
            if ("WDBKD".equals(result.get(0).substring(result.get(0).indexOf("WD"), result.get(0).indexOf("WD") + 5).replace("\n", ""))) {
                long diffSeconds = total / 1000 % 60;

                long diffMinutes = total / (60 * 1000) % 60;
                long diffHours = total / (60 * 60 * 1000) % 24;
                long lg_seconds = (diffHours * 3600) + (diffMinutes * 60) + diffSeconds;

                if (diffHours != 0 && diffMinutes != 0 && diffSeconds != 0) {
                    str_result = "ETL Delay " + diffHours + "h " + diffMinutes + "m";
                } else if (diffHours != 0 && diffMinutes == 0 && diffSeconds != 0) {
                    str_result = "ETL Delay " + diffHours + "h";
                } else if (diffHours != 0 && diffMinutes != 0 && diffSeconds == 0) {
                    str_result = "ETL Delay " + diffHours + "h " + diffMinutes + "m";
                } else if (diffHours == 0 && diffMinutes != 0 && diffSeconds != 0) {
                    str_result = "ETL Delay " + diffMinutes + "m";
                } else if (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0) {
                    str_result = "ETL Delay " + diffMinutes + "m";
                } else {
                    str_result = "ETL Delay " + diffSeconds + "s";
                }
                str_f_output += str_result + "\n";

            } else {

            }

            for (int i = 0; i < result.size(); i++) {
                str_f_output += result.get(i) + "\n";

            }
        }
        if (num == 0) {
            return str_result;
        } else {
            return str_f_output;
        }

    }

    public static String cd_delay(String str_time) throws ParseException {

        String dateStart = str_time.substring(0, str_time.indexOf(' '));
        String dateEnd = str_time.substring(str_time.indexOf(' ') + 1, str_time.length());

        String str_result = "";
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        long total = 0;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateEnd);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long exceedtime = exceed_time_in_minutes;
            //if time more than 5 mins, then show delay
            if (diff > (exceedtime * 60000)) {

                long diffSeconds = diff / 1000 % 60;
                total = diff;

                long diffMinutes = diff / (60 * 1000) % 60;
                long diffHours = diff / (60 * 60 * 1000) % 24;
                long lg_seconds = (diffHours * 3600) + (diffMinutes * 60) + diffSeconds;

                if (diffHours != 0 && diffMinutes != 0 && diffSeconds != 0) {
                    str_result = "Delay " + diffHours + "h " + diffMinutes + "m \n";
                } else if (diffHours != 0 && diffMinutes == 0 && diffSeconds != 0) {
                    str_result = "Delay " + diffHours + "h \n";
                } else if (diffHours != 0 && diffMinutes != 0 && diffSeconds == 0) {
                    str_result = "Delay " + diffHours + "h " + diffMinutes + "m\n";
                } else if (diffHours == 0 && diffMinutes != 0 && diffSeconds != 0) {
                    str_result = "Delay " + diffMinutes + "m \n";
                } else if (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0) {
                    str_result = "Delay " + diffMinutes + "m \n";
                } else {
                    str_result = "Delay " + diffSeconds + "s\n";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_result;
    }

    public static long cd_total(String str_time) throws ParseException {

        String dateStart = str_time.substring(0, str_time.indexOf(' '));
        String dateEnd = str_time.substring(str_time.indexOf(' ') + 1, str_time.length());

        String str_result = "";
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        long diff = 0;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateEnd);

            //in milliseconds
            diff = d2.getTime() - d1.getTime();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return diff;
    }

    public static List createTimeStringArray(String str_stream, int size, String LOG_FOLDER, BufferedWriter bw) throws IOException, FileNotFoundException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM,HH:mm:ss");
        Date DateStartTime = null;
        Date DateEndTime = null;
        String str_starttime = "";
        String str_endtime = "";

        if (str_stream.equals("WDBKD")) {
            //get start time

            str_starttime = getStarttime("WDBKD.log", LOG_FOLDER, 3, "R_WDBK_SNAP_SHOT", "WDBKD0701B", "WDBKD0701B");
            DateStartTime = sdf.parse(str_starttime);

            //get end time
            String str_time1 = "";
            File f1 = new File(LOG_FOLDER + "WDBKD.log");
            if (f1.exists()) {
                str_time1 = getEndTime(LOG_FOLDER + "WDBKD.log");
            } else {
                str_time1 = "not found";
            }

            String str_time2 = "";
            File f2 = new File(LOG_FOLDER + "WDCCD.log");
            if (f2.exists()) {
                str_time2 = getEndTime(LOG_FOLDER + "WDCCD.log");
            } else {
                str_time2 = "not found";
            }

            String str_time3 = "";
            File f3 = new File(LOG_FOLDER + "WDDLD.log");
            if (f3.exists()) {
                str_time3 = getEndTime(LOG_FOLDER + "WDDLD.log");
            } else {
                str_time3 = "not found";
            }

            Date matchDateTime1 = sdf.parse(str_time1);
            Date matchDateTime2 = sdf.parse(str_time2);
            Date matchDateTime3 = sdf.parse(str_time3);

            if (matchDateTime1.compareTo(matchDateTime2) == 1 && matchDateTime1.compareTo(matchDateTime3) == 1) {
                str_endtime = sdf.format(matchDateTime1);
                DateEndTime = matchDateTime1;

            } else if (matchDateTime2.compareTo(matchDateTime1) == 1 && matchDateTime2.compareTo(matchDateTime3) == 1) {
                str_endtime = sdf.format(matchDateTime2);
                DateEndTime = matchDateTime2;

            } else if (matchDateTime3.compareTo(matchDateTime1) == 1 && matchDateTime3.compareTo(matchDateTime2) == 1) {
                str_endtime = sdf.format(matchDateTime3);
                DateEndTime = matchDateTime3;
            }
        } else if (str_stream.equals("WDCOD")) {
            str_starttime = getStarttime("WDCOD.log", "", 3, "StartBatchNode", "WDCOD0001B", "WDCOD0001B");

            if (!str_starttime.equals("not found")) {
                DateStartTime = sdf.parse(str_starttime);
            } else {
                bw.write("WDCOD start code " + str_starttime + "!");
            }
            //get end time
            str_endtime = "";
            File f1 = new File("" + "WDCOD.log");
            if (f1.exists()) {
                str_endtime = getEndTime("" + "WDCOD.log");
            } else {
                str_endtime = "not found";
            }

            DateEndTime = sdf.parse(str_endtime);
        }

        //create time list
        List<String> time_list_yyyyddmm_hhmmss = new ArrayList<String>();
        List<String> time_list_yyyyddmm_hmmss = new ArrayList<String>();
        List<String> time_list_hmmss = new ArrayList<String>();

        if (!str_starttime.equals("not found")) {

            //set date end time equal to matchDateTime add 30 minutes
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(DateEndTime);
            calendar.add(Calendar.MINUTE, 30);  //add 30 minutes
            DateEndTime = calendar.getTime();

            //create timeframe
            while (DateEndTime.compareTo(DateStartTime) == 1) {

                str_starttime = "" + str_starttime + " " + str_starttime.substring(str_starttime.indexOf(",") + 1, str_starttime.indexOf(":") - 2);
                time_list_yyyyddmm_hhmmss.add(str_starttime);
                calendar.setTime(DateStartTime);
                calendar.add(Calendar.MINUTE, 30); //add 30 minutes
                DateStartTime = calendar.getTime();
                str_starttime = sdf.format(DateStartTime);
            }

            String str_replace = "";

            for (int i = 0; i < time_list_yyyyddmm_hhmmss.size(); i++) {

                str_replace = time_list_yyyyddmm_hhmmss.get(i).replace("0 ", "0");
                str_replace = str_replace.replace(",0", ", ");

                time_list_yyyyddmm_hmmss.add(str_replace);

                time_list_hmmss.add(time_list_yyyyddmm_hhmmss.get(i).substring(time_list_yyyyddmm_hhmmss.get(i).indexOf(",") + 1, time_list_yyyyddmm_hhmmss.get(i).indexOf(":") + 3));
            }

        }
        return time_list_yyyyddmm_hmmss;
    }

    public static String getStarttime(String str_CC_D_B, String str_logfile, int columnnum, String str_job, String str_startcode, String str_endcode)
            throws FileNotFoundException, IOException, ParseException {

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile + str_CC_D_B));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_starttime = "";
        String str_minutes = "";
        while ((line1 = bufferedReader1.readLine()) != null) {

            if (line1.contains(str_startcode)) {
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 7, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");
            }

        }

        if ("".equals(str_starttime)) {
            str_starttime = "not found";
        } else {
            str_minutes = str_starttime.substring(str_starttime.indexOf(":") + 1, str_starttime.indexOf(":") + 2);
            int num_minutes = Integer.parseInt(str_minutes);

            if (num_minutes < 3) {
                str_starttime = str_starttime.replace(str_starttime.substring(str_starttime.indexOf(":") + 1), "00:00");
            } else {
                str_starttime = str_starttime.replace(str_starttime.substring(str_starttime.indexOf(":") + 1), "30:00");
            }
        }
        return str_starttime;
    }

    public static String getEndTime(String str_logfile) throws FileNotFoundException, IOException {
        String lasttime = "";
        String sCurrentLine = "";
        BufferedReader br = new BufferedReader(new FileReader(str_logfile));

        while ((sCurrentLine = br.readLine()) != null && !" ".equals(br.readLine())) {
            lasttime = sCurrentLine;
        }

        int i = lasttime.indexOf("END:");
        lasttime = lasttime.substring(i + 5, lasttime.length());

        return lasttime;
    }

    public static void WDBKD(String str_CC_D_B, String str_logfile, List<String> time_list_yyyyddmm_hmmss, int num, String LOG_FOLDER, BufferedWriter bw) throws IOException, FileNotFoundException, ParseException {
        String str_others = "";
        bw.write("\n\n\n********* BANK ************\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 3, "R_WDBK_SNAP_SHOT", "WDBKD0701B", "WDBKD0701B", "LIGHT_YELLOW", 1, 2, "1h30m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 4, "R_PREBATCH", "WDBKD0376B", "WDBKD0381B", "BROWN", 1, 2, "7m - 45m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 5, "R_WDBK_ETL", "WDBKD0571B", "WDBKD0573B", "YELLOW", 1, 2, "2h30m - 4h30m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 6, "COAP_PF_ETL", "WDBKD0799B", "WDBKD0806B", "CORAL", 1, 2, "2h30m - 4h30m", 1, bw);

        bw.write("\n");
        String str_others1 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCRD_ECA_PAYMENT", "WDBKD0493B", "WDBKD0498B", "", 0, 0, "", 100, bw);
        String str_others2 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCRD_ECA_ABORT", "WDBKD0316B", "WDBKD0319B", "", 0, 0, "", 100, bw);
        str_others = str_others1 + str_others2;
        bw.write("========================================================================\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 7, "CCRD_ECA_PAY_ABORT", "WDBKD0493B", "WDBKD0319B", "ORANGE", 4, 2, "8m - 20m", 1, bw);
        bw.write("========================================================================\n\n");

        str_others = "";
        String str_others33 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PF_ECA_PAYMENT", "WDBKD0764B", "WDBKD0770B", "", 0, 0, "", 100, bw);
        String str_others44 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PF_ECA_ABORT", "WDBKD0771B", "WDBKD0775B", "", 0, 0, "", 100, bw);
        str_others = str_others33 + str_others44;
        bw.write("========================================================================\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 8, "PF_ECA_PAY_ABORT", "WDBKD0764B", "WDBKD0775B", "DARK_YELLOW", 4, 2, "8m - 20m", 1, bw);
        bw.write("========================================================================\n\n");

        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 9, "CCRD_CL", "WDBKD0001B", "WDBKD0018B", "LIGHT_BLUE", 1, 2, "1h30m - 3h30m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 10, "CCRD_RC", "WDBKD0180B", "WDBKD0198B", "RED", 1, 2, "1h30m - 2h30m", 1, bw);     //CCRD CASE ASSIGNMENT (CCRD_RC_ASGN)  WDBKD0180B   WDBKD0198B 

        bw.write("=============================================\n");
        bw.write(cal_Total(str_logfile, "CCRD_CL_+_CCRD_RC_ASGN", "WDBKD0001B", "WDBKD0198B"));
        bw.write("\n=============================================\n\n");

        //===========================CASE ASSIGNMENT======================================
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 11, "CRECC_CL", "WDBKD0276B", "WDBKD0293B", "LIGHT_BLUE", 1, 2, "30m - 1h", 1, bw); //CRECC CASE ASSIGNMENT (CRECC_CL_ASGN)  WDBKD0276B WDBKD0293B 								   
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 12, "CRSME_RC", "WDBKD0199B", "WDBKD0217B", "RED", 1, 2, "15m - 20m", 1, bw); //CRSME CASE ASSIGNMENT (CRSME_RC_ASGN) WDBKD0199B WDBKD0217B 
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 13, "CRSME_CGC", "WDBKD0301B", "WDBKD0309B", "RED", 1, 2, "4m - 8m", 1, bw); //CRSME CGC CASE ASSIGNMENT (CRSME_CGC_ASGN) WDBKD0301B WDBKD0309B 										

        bw.write("=============================================\n");
        bw.write(cal_Total(str_logfile, "CRECC_CL_+_CRSME_RC_ASGN", "WDBKD0276B", "WDBKD0217B"));
        bw.write(cal_Total(str_logfile, "CRECC_CL_+_CRSME_CGC_ASGN", "WDBKD0276B", "WDBKD0309B"));
        bw.write("\n=============================================\n\n");

        //===========================CASE ASSIGNMENT======================================
        str_others1 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_PF_CL", "WDCCD0019B", "WDCCD0029B", "", 0, 0, "", 100, bw);
        str_others33 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "P_COAP_UPDATE_CASE_PF", "WDCCD0402B", "WDCCD0402B", "", 0, 0, "", 100, bw);
        str_others44 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_PF_CL", "WDCCD0030B", "WDCCD0036B", "", 0, 0, "", 100, bw);
        str_others = str_others1 + str_others33 + str_others44;
        bw.write("=============================================\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 14, "CCPF_PF_CL", "WDCCD0019B", "WDCCD0036B", "LIGHT_BLUE", 4, 3, "1h30m", 1, bw); //CCPF-PF CASE ASSIGNMENT (CCPF_PF_CL_ASGN)   WDCCD0019B WDCCD0036B  
        bw.write("=============================================\n\n");

        //R_WDCC_PF_CL   WDCCD0402B  WDCCD0036B
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 15, "R_WDBK_CCPF_AD", "WDBKD0269B", "WDBKD0273B", "GREEN", 1, 2, "40m - 1h30m", 1, bw); //AutoDialer WDBKD0269B  WDBKD0273B
        bw.write("=============================================\n");
        bw.write(cal_Total(str_logfile, "CCPF_PF_CL+R_WDBK_CCPF_AD", "WDCCD0019B", "WDBKD0273B"));
        bw.write("\n=============================================\n\n");

        //=================MAXIMUM =============================
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 16, "CCRD_CL_+_CCRD_RC_ASGN", "WDBKD0001B", "WDBKD0198B", "GREY_40_PERCENT", 4, 2, "3h 30m", 1, bw);

        //==========================ECA ASSIGNMENT, ECA REASSIGNMENT============================== 
        bw.write("\n");
        String str_others3 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCRD_ECA_ASGN", "WDBKD0320B", "WDBKD0329B", "", 0, 0, "", 100, bw);
        String str_others4 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCRD_ECA_REASGN", "WDBKD0330B", "WDBKD0339B", "", 0, 0, "", 100, bw);
        str_others = str_others3 + str_others4;
        bw.write("========================================================================\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 17, "CCRD_ECA_ASGN_REASGN", "WDBKD0320B", "WDBKD0339B", "VIOLET", 4, 2, "2m - 3m", 1, bw);
        bw.write("========================================================================\n\n");

        //==========================PRERPT, POSTRPT===========================
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 22, "CRECC_CL_PRERPT", "WDBKD0434B", "WDBKD0437B", "GREY_40_PERCENT", 1, 2, "2h45m - 3h45m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 18, "CCRD_CL_PRERPT", "WDBKD0467B", "WDBKD0470B", "GREY_40_PERCENT", 1, 2, "2h45m - 3h45m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 19, "CCRD_RC_PRERPT", "WDBKD0425B", "WDBKD0428B", "GREY_40_PERCENT", 1, 2, "8m - 2h34m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 20, "CRSME_RC_PRERPT", "WDBKD0447B", "WDBKD0451B", "GREY_40_PERCENT", 1, 2, "6m - 20m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 21, "CRSME_CGC_PRERPT", "WDBKD0483B", "WDBKD0486B", "GREY_40_PERCENT", 1, 2, "47s - 6m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 22, "CCPF_PF_CL_PRERPT", "WDCCD0215B", "WDCCD0218B", "GREY_40_PERCENT", 1, 2, "30m - 2h", 1, bw);

        bw.write("\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 23, "CRECC_CL_POSTRPT", "WDBKD0475B", "WDBKD0478B", "GREY_40_PERCENT", 1, 2, "38s - 1m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 24, "CCRD_CL_POSTRPT", "WDBKD0471B", "WDBKD0474B", "GREY_40_PERCENT", 1, 2, "7m - 11m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 25, "CCRD_RC_POSTRPT", "WDBKD0457B", "WDBKD0461B", "GREY_40_PERCENT", 1, 2, "2m - 6m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 26, "CRSME_RC_POSTRPT", "WDBKD0479B", "WDBKD0482B", "GREY_40_PERCENT", 1, 2, "2m - 5m", 1, bw);
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 27, "CCPF_PF_CL_POSTRPT", "WDCCD0219B", "WDCCD0222B", "GREY_40_PERCENT", 1, 2, "1m", 1, bw);
        //// readlog(str_others ,str_CC_D_B,str_logfile,24,"CCPF_PF_RC_POSTRPT", "WDCCD0237B", "WDCCD0241B","GREY_40_PERCENT",6,2);

//            //==========================OTHERS (e.g. PTP, Call Escalation==============================  
        bw.write("\n--------------------------------- OTHERS ---------------------------------------------------\n");
        String str_others5 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CRECC-CL", "WDBKD0417B", "WDBKD0421B", "", 0, 0, "", 100, bw);
        String str_others6 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CCRD-CL", "WDBKD0412B", "WDBKD0416B", "", 0, 0, "", 100, bw);
        String str_others7 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CCRD-RC", "WDBKD0364B", "WDBKD0368B", "", 0, 0, "", 100, bw);
        String str_others8 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CRSME-RC", "WDBKD0369B", "WDBKD0373B", "", 0, 0, "", 100, bw);
        String str_others9 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CCPF-PF-CL", "WDCCD0191B", "WDCCD0195B", "", 0, 0, "", 100, bw);
        String str_others10 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "PTP-UPDATE_CCPF-PF-RC", "WDCCD0159B", "WDCCD0163B", "", 0, 0, "", 100, bw);
        String str_others11 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CRECC-CL", "WDBKD0394B", "WDBKD0399B", "", 0, 0, "", 100, bw);
        String str_others12 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CCRD-CL", "WDBKD0382B", "WDBKD0387B", "", 0, 0, "", 100, bw);
        String str_others13 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CCRD-RC", "WDBKD0388B", "WDBKD0393B", "", 0, 0, "", 100, bw);
        String str_others14 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CRSME-RC", "WDBKD0400B", "WDBKD0405B", "", 0, 0, "", 100, bw);
        String str_others15 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CCPF-PF-CL", "WDCCD0285B", "WDCCD0290B", "", 0, 0, "", 100, bw);
        String str_others16 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CallEscalation_CCPF-PF-RC", "WDCCD0291B", "WDCCD0296B", "", 0, 0, "", 100, bw);
        bw.write("-------------------------------------------------------------------------------------------\n");
        str_others = str_others5 + str_others6 + str_others7 + str_others8 + str_others9 + str_others10 + "\n"
                + str_others11 + str_others12 + str_others13 + str_others14 + str_others15 + str_others16;
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 17, "OTHERS", "WDBKD0417B", "WDCCD0296B", "GREY_40_PERCENT", 4, 11, "12m - 52m", 1, bw);

        bw.write("\n");
        str_others = "";
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, LOG_FOLDER + "WDCCD.log", 20, "CCPF_PF_PERF_MON_002", "WDCCD0382B", "WDCCD0386B", "GREY_40_PERCENT", 1, 2, "30m - 2h", 1, bw);

        str_others = "";
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 19, "CCRD_CL_PERF_MON_002", "WDBKD0776B", "WDBKD0780B", "GREY_40_PERCENT", 2, 3, "4m - 16m", 1, bw);
        bw.write("\n");

        bw.write("\n");
        str_others = "";
        str_others1 = read_others(str_CC_D_B, str_logfile, "Generate_LOADS_Acct_No", "WDBKD0649B", "WDBKD0652B", bw);
        readlog(time_list_yyyyddmm_hmmss, str_others1, str_CC_D_B, str_logfile, 3, "OTHERS", "WDBKD0649B", "WDBKD0652B", "GREY_40_PERCENT", 4, 2, "12m - 52m", 1, bw);

        str_others2 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "GENERATE_REMINDER_BATCH", "WDBKD0640B", "WDBKD0647B", "", 0, 0, "", 100, bw);
        str_others3 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Exception_Report", "WDBKD0258B", "WDBKD0275B", "", 0, 0, "", 100, bw);
        str_others4 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "GenerateBrokenLinkageAcct&Cif", "WDBKD0696B", "WDBKD0700B", "", 0, 0, "", 100, bw);
        str_others5 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "ETLUpdateAcctFromCustomerFile", "WDBKD0053B", "WDBKD0099B", "", 0, 0, "", 100, bw);
        str_others = str_others2 + "\n" + str_others3 + "\n" + str_others4 + "\n" + str_others5;
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 4, "OTHERS", "WDBKD0640B", "WDBKD0099B", "GREY_40_PERCENT", 4, 7, "12m - 52m", 1, bw);

    }

    public static void WDCCD(String str_CC_D_B, String str_logfile, List<String> time_list_yyyyddmm_hmmss, int num, BufferedWriter bw) throws IOException, FileNotFoundException, ParseException {
        bw.write("\n");
        bw.write("\n\n********* CC ************\n");
        String str_others = "";
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 28, "R_WDCC_ETL", "WDCCD0138B", "WDCCD0140B", "YELLOW", 2, 2, "30m - 3h", 1, bw);

        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 29, "COAP_CC_ETL", "WDCCD0393B", "WDCCD0400B", "CORAL", 2, 2, "30m - 3h", 1, bw);
 
        bw.write("\n");

        String str_others01 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Start_Batch_Node", "WDCCD0373B", "WDCCD0373B", "", 0, 0, "", 100, bw);
        String str_others02 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Set_Date_Control", "WDCCD0374B", "WDCCD0374B", "", 0, 0, "", 100, bw);
        String str_others03 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Set_Batch_Recovery_Date", "WDCCD0375B", "WDCCD0375B", "", 0, 0, "", 100, bw);
        String str_others04 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Write_Off", "WDBKD0294B", "WDBKD0294B", "", 0, 0, "", 100, bw);
        String str_others05 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Write_Off_Statement", "WDBKD0295B", "WDBKD0295B", "", 0, 0, "", 100, bw);
        String str_others06 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Write_Off_Islamic", "WDBKD0297B", "WDBKD0297B", "", 0, 0, "", 100, bw);
        String str_others07 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Write_Off_Islamic_Statement", "WDBKD0298B", "WDBKD0298B", "", 0, 0, "", 100, bw);
        String str_others08 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "End_Batch_Node", "WDCCD0376B", "WDCCD0376B", "", 0, 0, "", 100, bw);
        str_others = str_others01 + str_others02 + str_others03 + "\n" + str_others04 + str_others05 + str_others06 + str_others07 + "\n" + str_others08;
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 30, "R_WDCC_CCPF_WO", "WDCCD0373B", "WDCCD0376B", "PINK", 3, 2, "15m - 2h15m", 1, bw);
        bw.write("\n");

        String str_others14 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_ECA_PAYMENT", "WDCCD0248B", "WDCCD0253B", "", 0, 0, "", 100, bw);
        String str_others15 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_ECA_ABORT", "WDCCD0106B", "WDCCD0109B", "", 0, 0, "", 100, bw);
        str_others = str_others14 + str_others15;
        bw.write("========================================================================\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 31, "R_WDCC_CCPF_ECA_PAY_ABORT", "WDCCD0248B", "WDCCD0109B", "ORANGE", 3, 2, "8m - 15m", 1, bw);
        bw.write("========================================================================\n\n");

        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 32, "R_WDCCD_PRERPT_CC_CL", "WDCCD0201B", "WDCCD0204B", "GREY_40_PERCENT", 2, 2, "4m - 16m", 1, bw);

        //// ==============CCPF_CC_RC_PRERPT=============
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 33, "R_WDCCD_PRERPT_CC_RC", "WDCCD0227B", "WDCCD0231B", "GREY_40_PERCENT", 2, 2, "40m - 50m", 1, bw);

        bw.write("\n");

        //// ============== PTP, Call Escalation ===========
        bw.write("------------------------------------------------------------------------------\n");
        String str_others1 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_CL_PTP", "WDCCD0196B", "WDCCD0200B", "", 0, 0, "", 100, bw);
        String str_others2 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_RC_PTP", "WDCCD0154B", "WDCCD0158B", "", 0, 0, "", 100, bw);
        String str_others3 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_CL_CE", "WDCCD0179B", "WDCCD0184B", "", 0, 0, "", 100, bw);
        String str_others4 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_RC_CE", "WDCCD0185B", "WDCCD0190B", "", 0, 0, "", 100, bw);
        str_others = str_others1 + str_others2 + "\n" + str_others3 + str_others4;
        bw.write("------------------------------------------------------------------------------\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 38, "Others", "WDCCD0196B", "WDCCD0190B", "GREY_40_PERCENT", 4, 5, "4m - 16m", 1, bw);
        bw.write("\n");

        ////===========================CCPF_CC_PERF_MON_002======================================
        str_others = "";
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 34, "CCPF_CC_PERF_MON_002", "WDCCD0377B", "WDCCD0381B", "GREY_40_PERCENT", 2, 3, "4m - 16m", 1, bw);
        bw.write("\n");

        ////===========================CASE ASSIGNMENT======================================
        str_others1 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_CL", "WDCCD0001B", "WDCCD0011B", "", 0, 0, "", 100, bw);
        String str_others33 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "P_COAP_UPDATE_CASE_CC", "WDCCD0401B", "WDCCD0401B", "", 0, 0, "", 100, bw);
        String str_others44 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "R_WDCC_CC_CL", "WDCCD0012B", "WDCCD0018B", "", 0, 0, "", 100, bw);
        str_others = str_others1 + str_others33 + str_others44;
        bw.write("=============================================\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 35, "R_WDCC_CC_CL", "WDCCD0001B", "WDCCD0018B", "LIGHT_BLUE", 4, 3, "1h53m - 2h53m", 1, bw);
        bw.write("=============================================\n\n");

        bw.write("");

        ////=============Auto Dialer ============
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 36, "R_WDCC_CC_AD", "WDCCD0323B", "WDCCD0327B", "GREEN", 2, 2, "20m - 40m", 1, bw);
        bw.write("\n");

        ////=============ECA ASGN, ECA REASGN============
        String str_others12 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_ECA_ASGN", "WDCCD0110B", "WDCCD0119B", "", 0, 0, "", 100, bw);
        String str_others13 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_ECA_REASGN", "WDCCD0120B", "WDCCD0129B", "", 0, 0, "", 100, bw);
        str_others = str_others12 + str_others13;
        bw.write("=======================================================================\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 37, "R_WDCC_CCPF_ECA_ASSIGN_REASSIGN", "WDCCD0110B", "WDCCD0129B", "VIOLET", 4, 2, "3m - 4m", 1, bw);
        bw.write("=======================================================================\n\n");

        ////============== Others ====================
        bw.write("------------------------------------------------------------------------------\n");
        String str_others5 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Update_MCIF_Number", "WDCCD0333B", "WDCCD0333B", "", 0, 0, "", 100, bw);
        String str_others6 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "InsertPFActiveAccount", "WDCCD0334B", "WDCCD0334B", "", 0, 0, "", 100, bw);
        str_others = str_others5 + str_others6;
        bw.write("------------------------------------------------------------------------------\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 38, "Others", "WDCCD0333B", "WDCCD0334B", "GREY_40_PERCENT", 4, 2, "4m - 10m", 1, bw);

        bw.write("\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 39, "R_WDCC_CC_RC", "WDCCD0044B", "WDCCD0062B", "RED", 2, 2, "1h45m - 2h30m", 1, bw);
        bw.write("\n");

        /////============= CCPF_CC_CL_POSTRPT==================
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 40, "CCPF_CC_CL_POSTRPT", "WDCCD0205B", "WDCCD0208B", "GREY_40_PERCENT", 2, 2, "40m - 1h", 1, bw);

        bw.write("\n------------------------------------------------------------------------------\n");
        String str_others8 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_CC_LGL_CIVIL", "WDCCD0082B", "WDCCD0089B", "", 0, 0, "", 100, bw);
        String str_others9 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_CC_SOLICITOR", "WDCCD0165B", "WDCCD0176B", "", 0, 0, "", 100, bw);
        str_others = str_others8 + str_others9;
        bw.write("------------------------------------------------------------------------------\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 41, "Others", "WDCCD0082B", "WDCCD0176B", "GREY_40_PERCENT", 4, 2, "25m - 35m", 1, bw);
        bw.write("\n");

        ////==============WRITE OFF=============
        str_others01 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Start_Batch_Node", "WDCCD0328B", "WDCCD0328B", "", 0, 0, "", 100, bw);
        str_others02 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Set_Date_Control", "WDCCD0329B", "WDCCD0329B", "", 0, 0, "", 100, bw);
        str_others03 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Set_Batch_Recovery_Date", "WDCCD0365B", "WDCCD0365B", "", 0, 0, "", 100, bw);
        str_others04 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "Write_Off_CSVS", "WDBKD0296B", "WDBKD0296B", "", 0, 0, "", 100, bw);
        str_others05 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "End_Batch_Node", "WDCCD0330B", "WDCCD0330B", "", 0, 0, "", 100, bw);
        str_others = str_others01 + str_others02 + str_others03 + str_others04 + str_others05;
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 42, "R_WDCC_CCPF_WO", "WDCCD0328B", "WDCCD0330B", "TURQUOISE", 4, 5, "15m - 2h15m", 1, bw);

        bw.write("\n");
        bw.write("------------------------------------------------------------------------------\n");
        String str_others10 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_LEGAL_CSV", "WDBKD0562B", "WDBKD0564B", "", 0, 0, "", 100, bw);
        String str_others11 = readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 0, "CCPF_CC_RC_POSTRPT", "WDCCD0232B", "WDCCD0236B", "", 0, 0, "", 100, bw);
        str_others = str_others10 + str_others11;
        bw.write("------------------------------------------------------------------------------\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 43, "Others", "WDBKD0562B", "WDCCD0236B", "GREY_40_PERCENT", 4, 2, "30m - 1h", 1, bw);
    }

    public static void WDDLD(String str_CC_D_B, String str_logfile, List<String> time_list_yyyyddmm_hmmss, int num, BufferedWriter bw) throws IOException,
            FileNotFoundException, ParseException {
        bw.write("\n");
        bw.write("\n\n********* DELTA ************\n");
        String str_others = "";
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 44, "R_WDDL_ETL", "WDDLD0090B", "WDDLD0092B", "YELLOW", 2, 2, "1h - 4h", 1, bw);

        ///===========================ECA PAY_ABORT======================================
        bw.write("\n");
        String str_others14 = read_others(str_CC_D_B, str_logfile, "AFABF_ECA_PAYMENT", "WDDLD0283B", "WDDLD0288B", bw);
        String str_others15 = read_others(str_CC_D_B, str_logfile, "AFABF_ECA_ABORT", "WDDLD0175B", "WDDLD0178B", bw);
        str_others = str_others14 + str_others15;
        bw.write("=============================================================================\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 45, "AFABF_ECA_PAY_ABORT", "WDDLD0283B", "WDDLD0178B", "ORANGE", 4, 2, "7m - 15m", 1, bw);
        bw.write("=============================================================================\n\n");
        ////==========================AFABF_CL_PRERPT============================== 
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 46, "AFABF_CL_PRERPT", "WDDLD0275B", "WDDLD0278B", "GREY_40_PERCENT", 2, 2, "30m - 40m", 1, bw);

        ////==========================Others==============================
        bw.write("\n----------------------------------------------------------------------\n");
        String str_others1 = read_others(str_CC_D_B, str_logfile, "PTP-UPDATE AFABF-CL", "WDDLD0239B", "WDDLD0243B", bw);
        String str_others2 = read_others(str_CC_D_B, str_logfile, "PTP-UPDATE AFABF-RC", "WDDLD0220B", "WDDLD0224B", bw);
        String str_others3 = read_others(str_CC_D_B, str_logfile, "CallEscalationAFABF-CL", "WDDLD0227B", "WDDLD0232B", bw);
        String str_others4 = read_others(str_CC_D_B, str_logfile, "CallEscalationAFABF-RC", "WDDLD0233B", "WDDLD0238B", bw);
        str_others = str_others1 + str_others2 + "\n" + str_others3 + str_others4;
        bw.write("----------------------------------------------------------------------\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 47, "Others", "WDDLD0239B", "WDDLD0238B", "GREY_40_PERCENT", 4, 5, "3m - 1h30m", 1, bw);

        bw.write("\n");
        ////===========================CASE ASSIGNMENT======================================
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 48, "R_WDDL_AFABF_CL", "WDDLD0015B", "WDDLD0032B", "LIGHT_BLUE", 2, 2, "48m - 1h", 1, bw);
        bw.write("\n");
        ////===========================Auto Dialer======================================
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 49, "R_WDDL_AFABF_AD", "WDDLD0142B", "WDDLD0147B", "GREEN", 2, 2, "45m - 56m", 1, bw);
        bw.write("\n");
        ////===========================CASE ASSIGNMENT======================================
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 50, "R_WDDL_AFABF_RC", "WDDLD0148B", "WDDLD0166B", "RED", 2, 2, "48m - 1h5m", 1, bw);

        ////==========================Others============================== 
        bw.write("\n------------------------------------------------------------------------\n");
        String str_others5 = read_others(str_CC_D_B, str_logfile, "AFABF_LGL_CIVIL", "WDDLD0167B", "WDDLD0174B", bw);
        String str_others6 = read_others(str_CC_D_B, str_logfile, "AFABF_LGL_REPO", "WDDLD0200B", "WDDLD0207B", bw);
        String str_others7 = read_others(str_CC_D_B, str_logfile, "AFABF-AKPK", "WDDLD0289B", "WDDLD0296B", bw);
        bw.write("------------------------------------------------------------------------\n");
        str_others = str_others5 + str_others6 + str_others7;
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 51, "Others", "WDDLD0167B", "WDDLD0296B", "GREY_40_PERCENT", 4, 4, "50m - 1h", 1, bw);

        ////===========================ECA ASSIGN REASSIGN======================================
        bw.write("\n");
        String str_others12 = read_others(str_CC_D_B, str_logfile, "AFABF_ECA_ASGN", "WDDLD0179B", "WDDLD0188B", bw);
        String str_others13 = read_others(str_CC_D_B, str_logfile, "AFABF_ECA_REASGN", "WDDLD0189B", "WDDLD0198B", bw);
        str_others = str_others12 + str_others13;
        bw.write("==============================================================================================\n");
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 52, "ECA_ASSIGN_REASSIGN", "WDDLD0179B", "WDDLD0198B", "VIOLET", 4, 2, "3m", 1, bw);
        bw.write("==============================================================================================\n");

        ////==========================Others============================== 
        bw.write("\n------------------------------------------------------------------------\n");
        String str_others8 = read_others(str_CC_D_B, str_logfile, "AFABF_SOLICITOR", "WDDLD0208B", "WDDLD0219B", bw);
        String str_others9 = read_others(str_CC_D_B, str_logfile, "AFABFOtherRecoveryBatch", "WDDLD0433B", "WDDLD0437B", bw);
        String str_others10 = read_others(str_CC_D_B, str_logfile, "AFABF_CL_POSTRPT", "WDDLD0279B", "WDDLD0282B", bw);
        String str_others11 = read_others(str_CC_D_B, str_logfile, "AFABF_RC_POSTRPT", "WDDLD0270B", "WDDLD0274B", bw);
        bw.write("------------------------------------------------------------------------\n");
        str_others = str_others8 + str_others9 + str_others10 + str_others11;
        readlog(time_list_yyyyddmm_hmmss, str_others, str_CC_D_B, str_logfile, 53, "Others", "WDDLD0208B", "WDDLD0274B", "GREY_40_PERCENT", 4, 4, "50m - 1h", 1, bw);

    }

    public static String readlog(List<String> time_list_yyyyddmm_hmmss, String str_others, String str_CC_D_B, String str_logfile, int columnnum,
            String str_job, String str_startcode, String str_endcode, String str_color, int col2, int row2, String str_estimatetime, int num, BufferedWriter bw) throws FileNotFoundException, IOException, ParseException {
        String str_output = "";
        int diff = 0;

        if (str_job.length() < str_job_maxlength) {
            diff = str_job_maxlength - str_job.length();

            for (int z = 0; z < diff; z++) {
                str_job += " ";
            }
        }

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_duration = "";
        String str_starttime = "";
        String str_endtime = "";
        String str_starttime11 = "";
        String str_endtime11 = "";

        int startnum = 0;
        int endnum = 0;

        while ((line1 = bufferedReader1.readLine()) != null) {

            if (line1.contains(str_startcode)) {
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 6, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");

                if (" ".equals(str_starttime.substring(str_starttime.indexOf(",") + 1, str_starttime.indexOf(",") + 2))) {
                    str_starttime = str_starttime.replace(", ", ",x");
                    str_starttime = str_starttime.replaceAll(" ", "");
                    str_starttime = str_starttime.replaceAll("x", " ");
                } else {
                    str_starttime = str_starttime.replaceAll(" ", "");
                }

                str_starttime11 = str_starttime;
                str_starttime = str_starttime.replaceAll(",", " ");
            }
            if (line1.contains(str_endcode)) {
                i = line1.indexOf("END:");
                str_endtime = line1.substring(i + 4, line1.length());

                if (" ".equals(str_endtime.substring(str_endtime.indexOf(",") + 1, str_endtime.indexOf(",") + 2))) {
                    str_endtime = str_endtime.replace(", ", ",x");
                    str_endtime = str_endtime.replaceAll(" ", "");
                    str_endtime = str_endtime.replaceAll("x", " ");
                } else {
                    str_endtime = str_endtime.replaceAll(" ", "");
                }
                str_endtime11 = str_endtime;

                str_endtime = str_endtime.replaceAll(",", " ");
            }
        }

        if (!"".equals(str_endtime) && !"".equals(str_starttime)) {
 
            SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

            Date d1 = null;
            Date d3 = null;  
            
            d1 = format.parse(str_starttime);
            String str_start_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d1);

            d3 = format.parse(str_endtime);
            String str_end_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d3);

            str_duration = calculateDiff(str_starttime, str_endtime);
            int diff1 = 0;

            if (str_duration.length() < str_time_maxlength) {
                diff1 = str_time_maxlength - str_duration.length();

                for (int z = 0; z < diff1; z++) {
                    str_duration += " ";
                }
            }

            str_output = str_job.replace(" ", "") + " (" + str_duration.replace(" ", "") + ") From " + str_start_HH_mm_ss + " to " + str_end_HH_mm_ss + "\n";
            bw.write(str_job + " (" + str_startcode + " " + str_endcode + ") " + str_start_HH_mm_ss + " - " + str_end_HH_mm_ss + " (" + str_duration + ")\n");

            str_job = str_job.replace(" ", "");
            str_duration = str_duration.replace(" ", "");

        } else if ((!"".equals(str_starttime)) && ("".equals(str_endtime))) {
            str_output = str_job + " (" + str_startcode + " " + str_endcode + ")" + str_starttime.substring(str_starttime.indexOf(" "), str_starttime.length()) + ", NOT FOUND <<ESTIMATE finish within " + str_estimatetime + ">>";
            bw.write(str_job + " (" + str_startcode + " " + str_endcode + ")" + str_starttime.substring(str_starttime.indexOf(" "), str_starttime.length()) + ", NOT FOUND <<ESTIMATE finish within " + str_estimatetime + ">>\n");
        } else if ("".equals(str_starttime)) {
            str_output = str_job + " (" + str_startcode + " " + str_endcode + ") NOT FOUND, NOT FOUND  <<ESTIMATE finish within " + str_estimatetime + ">>";
            bw.write(str_job + " (" + str_startcode + " " + str_endcode + ") NOT FOUND, NOT FOUND  <<ESTIMATE finish within " + str_estimatetime + ">>\n");
        }
        return str_output;
    }

    public static String read_others(String str_CC_D_B, String str_logfile, String str_job, String str_startcode, String str_endcode, BufferedWriter bw) throws FileNotFoundException, IOException, ParseException {
        int diff = 0;

        if (str_job.length() < str_job_maxlength) {
            diff = str_job_maxlength - str_job.length();

            for (int z = 0; z < diff; z++) {
                str_job += " ";
            }
        }
        String line = null;

        int count = 0;

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_log = "";
        String str_log1 = "";
        String str_duration = "";
        String str_starttime = "";
        String str_endtime = "";
        while ((line1 = bufferedReader1.readLine()) != null) {

            if (line1.contains(str_startcode)) {
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 6, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");
                str_starttime = str_starttime.replaceAll(" ", "");
                str_starttime = str_starttime.replaceAll(",", " ");
            }
            if (line1.contains(str_endcode)) {
                i = line1.indexOf("END:");
                str_endtime = line1.substring(i + 4, line1.length());
                str_endtime = str_endtime.replaceAll(" ", "");
                str_endtime = str_endtime.replaceAll(",", " ");
            }
        }

        if (!"".equals(str_endtime) && !"".equals(str_starttime)) {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

            Date d1 = null;
            Date d2 = null;
            Date d3 = null;
            d1 = format.parse(str_starttime);

            String str_yyyy_dd_MM = new SimpleDateFormat("yyyy-dd-MM").format(d1);

            String str_time = str_yyyy_dd_MM + " 0:00:00";
            d2 = format.parse(str_time);
            d1 = format.parse(str_starttime);
            String str_start_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d1);

            d3 = format.parse(str_endtime);
            String str_end_HH_mm_ss = new SimpleDateFormat("HH:mm:ss").format(d3);

            str_duration = calculateDiff(str_starttime, str_endtime);
            int diff1 = 0;

            if (str_duration.length() < str_time_maxlength) {
                diff1 = str_time_maxlength - str_duration.length();

                for (int z = 0; z < diff1; z++) {
                    str_duration += " ";
                }
            }

            str_log1 = str_job + " (" + str_startcode + " " + str_endcode + ") " + str_start_HH_mm_ss + " - " + str_end_HH_mm_ss + " (" + str_duration + ")\n";

            bw.write(str_log1);

            str_job = str_job.replace(" ", "");
            str_duration = str_duration.replace(" ", "");
            str_log = str_job + " (" + str_duration + ") From " + str_start_HH_mm_ss + " to " + str_end_HH_mm_ss + "\n";
        } else if ((!"".equals(str_starttime)) && ("".equals(str_endtime))) {
            bw.write(str_job + " (" + str_startcode + " " + str_endcode + ")" + str_starttime.substring(str_starttime.indexOf(" "), str_starttime.length()) + ", NOT FOUND\n");
        } else if ("".equals(str_starttime)) {
            bw.write(str_job + " (" + str_startcode + " " + str_endcode + ") NOT FOUND, NOT FOUND\n");
        }

        return str_log;
    }

    public static String cal_Total(String str_logfile, String str_job, String str_startcode, String str_endcode) throws FileNotFoundException, IOException, ParseException {

        String line = null;

        int count = 0;

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(str_logfile));

        int ii = 0;
        int jj = 0;
        int i = 0;
        int j = 0;
        String line1 = null;
        String str_log = "";
        String str_duration = "";
        String str_starttime = "";
        String str_endtime = "";

        while ((line1 = bufferedReader1.readLine()) != null) {

            if (line1.contains(str_startcode)) {
                ii = line1.indexOf("START:");
                jj = line1.indexOf("END:");
                str_starttime = line1.substring(ii + 6, jj);

                str_starttime = str_starttime.replaceAll("Batch", "");
                str_starttime = str_starttime.replaceAll("Error", "");
                str_starttime = str_starttime.replaceAll("Success", "");
                str_starttime = str_starttime.replaceAll(" ", "");
                str_starttime = str_starttime.replaceAll(",", " ");
            }
            if (line1.contains(str_endcode)) {
                i = line1.indexOf("END:");
                str_endtime = line1.substring(i + 4, line1.length());
                str_endtime = str_endtime.replaceAll(" ", "");
                str_endtime = str_endtime.replaceAll(",", " ");
            }
        }

        String str_total = "";

        if ((!"".equals(str_starttime)) && (!"".equals(str_endtime))) {

            str_duration = calculateDiff(str_starttime, str_endtime);

            str_total = "(" + str_job + ") Total: " + str_duration;
        } else if ((!"".equals(str_starttime)) && ("".equals(str_endtime))) {
            str_total = str_job + ": NULL NOT YET FINISHED";
        }

        return str_total;
    }

    public static String calculateDiff(String str_starttime, String str_endtime) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        String str_duration = "";

        try {
            d1 = format.parse(str_starttime);
            d2 = format.parse(str_endtime);
//            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffDays = diff / (1000 * 60 * 60 * 24);

            long diffHours = 0;

            if (diffDays < 1) {
                diffHours = diff / (60 * 60 * 1000) % 24;
            } else {
                diffHours = (diffDays * 24) + (diff / (60 * 60 * 1000) % 24);
            }

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;

            long lg_seconds = (diffHours * 3600) + (diffMinutes * 60) + diffSeconds;

            if (diffHours != 0 && diffMinutes != 0 && diffMinutes < 10 && diffSeconds != 0) {
                str_duration = diffHours + "h  " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes < 10 && diffSeconds == 0) {
                str_duration = diffHours + "h  " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes >= 10 && diffSeconds != 0) {
                str_duration = diffHours + "h " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes != 0 && diffMinutes >= 10 && diffSeconds == 0) {
                str_duration = diffHours + "h " + diffMinutes + "m";
            } else if (diffHours != 0 && diffMinutes == 0 && diffSeconds != 0) {
                str_duration = diffHours + "h ";
            } else if ((diffHours == 0 && diffMinutes != 0 && diffSeconds != 0 && diffMinutes < 10) || (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0 && diffMinutes < 10)) {
                str_duration = "    " + diffMinutes + "m";
            } else if ((diffHours == 0 && diffMinutes != 0 && diffSeconds != 0 && diffMinutes >= 10) || (diffHours == 0 && diffMinutes != 0 && diffSeconds == 0 && diffMinutes >= 10)) {
                str_duration = "   " + diffMinutes + "m";
            } else {
                str_duration = "   " + diffSeconds + "s";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_duration;
    }

    private static void getRainbowDraw(String LOG_FOLDER, BufferedWriter bw) throws IOException, FileNotFoundException, ParseException {
 
        List<String> time_list_yyyyddmm_hmmss = createTimeStringArray("WDBKD", 0, LOG_FOLDER, bw);
        WDBKD("WDBKD", LOG_FOLDER + "WDBKD.log", time_list_yyyyddmm_hmmss, 1, LOG_FOLDER, bw);
        WDCCD("WDCCD", LOG_FOLDER + "WDCCD.log", time_list_yyyyddmm_hmmss, 1, bw);
        WDDLD("WDDLD", LOG_FOLDER + "WDDLD.log", time_list_yyyyddmm_hmmss, 1, bw);
       
    }

   
}
