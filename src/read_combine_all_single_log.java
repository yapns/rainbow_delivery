
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nsyap
 */
public class read_combine_all_single_log {

    private static final String str_allfolderBANK = "D:\\DailyCatchUp\\20170711\\allWDBKD";
    private static final String str_allfolderCC = "D:\\DailyCatchUp\\20170711\\allWDCCD";

    private static final String str_folderBANK = "D:\\DailyCatchUp\\20170711\\WDBKD";
    private static final String str_BANK_jobs = "D:\\DailyCatchUp\\20170711\\707WDBKD.txt";

    private static final String FILENAME = "D:\\DailyCatchUp\\20170711\\WDBKD.log";

    public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {

        // findLogs(str_allfolderBANK, str_folderBANK, str_BANK_jobs);
        
        String str_before_text_start;
        String str_log = "";

        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

            String sCurrentLine;
            //from wdbkd.log
            File folder = new File(str_folderBANK);
            
            while ((sCurrentLine = br.readLine()) != null) {

                int count = 0;
                for (File fileEntry : folder.listFiles()) {

                    //from wdbkd.log, if contain context equal to job of individual log, write str_log
                    if (sCurrentLine.contains(fileEntry.getName().replace(".log", ""))) {

                        str_log = startendLog(fileEntry.getName());
                        
                        //from wdbkd.log, get text before START statement
                        str_before_text_start = sCurrentLine.substring(0, sCurrentLine.indexOf("START:"));
                        
                        System.out.println(str_before_text_start + str_log);
                    }

                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String startendLog(String str_log) throws FileNotFoundException, IOException, ParseException {

        BufferedReader br1 = new BufferedReader(new FileReader(str_folderBANK + "\\" + str_log));
        String sCurrentLine;
        String str_line = null;
        String str_date_start_yyyyMMdd =null;
        String str_date_start_yyyyddMM = null;
        String str_date_end_yyyyMMdd = null;
        String str_date_end_yyyyddMM = null;
        while ((sCurrentLine = br1.readLine()) != null) {
            str_line += sCurrentLine;
        }

        str_line = str_line.replace("===", "");
        str_line = str_line.replace("Start Date         : ", "START: ");
        str_line = str_line.replace(str_line.substring(0, str_line.indexOf("START")), "");
        str_line = str_line.replace(str_line.substring(str_line.indexOf("."), str_line.indexOf("Batch")), " ");
        str_line = str_line.replace(" Start Time         : ", ",");
        str_line = str_line.replace("Batch Status       : ", "");
        str_line = str_line.replace("End Date           : ", "==");
        str_line = str_line.replace("==", "END: ");

        str_line = str_line.replace(" End Time           : ", ",");
        //   str_line = str_line.replace(str_line.substring(str_line.indexOf("Duration") - 10, str_line.length()), "");

        str_line = str_line.replace(str_line.substring(str_line.indexOf("END:") + 24, str_line.length()), "");
        str_line = str_line + " ";

        str_date_start_yyyyMMdd = str_line.substring(str_line.indexOf("START") + 7, str_line.indexOf(","));
        str_date_end_yyyyMMdd = str_line.substring(str_line.indexOf("END") + 5, str_line.length() - 10);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Date date = formatter.parse(str_date_start_yyyyMMdd);
        Date date1 = formatter.parse(str_date_end_yyyyMMdd);

        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-dd-MM");
        str_date_start_yyyyddMM = formatter2.format(date);
        str_date_end_yyyyddMM = formatter2.format(date1);

        str_line = str_line.replace(str_line.substring(str_line.indexOf("START") + 7, str_line.indexOf(",")), str_date_start_yyyyddMM);
        str_line = str_line.replace(str_line.substring(str_line.indexOf("END") + 5, str_line.length() - 10), str_date_end_yyyyddMM);
        System.out.println("" + str_line);
        return str_line;
    }

    public static void findLogs(String str_allfolder, String str_outfolder, String str_jobs) {
        //find 707 BANK jobs from all BANK logs and CC logs 
        int count = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(str_jobs))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                if (str_jobs.equals(str_BANK_jobs)) {
                    count += copyLogs(sCurrentLine, str_allfolderCC, str_outfolder);
                }
                count += copyLogs(sCurrentLine, str_allfolder, str_outfolder);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Total files copied: " + count);
    }

    public static int copyLogs(String str_find, String str_fromFolder, String str_inFolder) {
        File folder = new File(str_fromFolder);
        int count = 0;
        for (File fileEntry : folder.listFiles()) {

            if (str_find.equals(fileEntry.getName().replace(".log", ""))) {
                File afile = new File(str_fromFolder + "\\" + fileEntry.getName());

                if (afile.renameTo(new File(str_inFolder + "\\" + fileEntry.getName()))) {
                    count++;
                }
            }

        }
        return count;
    }
}
